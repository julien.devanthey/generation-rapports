﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GenerationRapports
{
    static class Program
    {
        /// <summary>
        /// Point d'entrée principal de l'application.
        /// </summary>
        [STAThread]
#pragma warning disable CS0017 // Plusieurs points d'entrée sont définis dans le programme. Compilez avec l'option /main pour spécifier le type qui contient le point d'entrée.
        static void Main()
#pragma warning restore CS0017 // Plusieurs points d'entrée sont définis dans le programme. Compilez avec l'option /main pour spécifier le type qui contient le point d'entrée.
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new frmMain());
        }
    }
}
