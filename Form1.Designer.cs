﻿namespace GenerationRapports
{
    partial class frmMain
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.lblGUID = new System.Windows.Forms.Label();
            this.progressbarProjet = new System.Windows.Forms.ProgressBar();
            this.tbxUsers = new System.Windows.Forms.TextBox();
            this.btnGenerer = new System.Windows.Forms.Button();
            this.menu2 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.btnAjouterConfig = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.cbxHeureParJour = new System.Windows.Forms.ComboBox();
            this.cbxProjets = new System.Windows.Forms.ComboBox();
            this.cbxEmploye = new System.Windows.Forms.ComboBox();
            this.btnSupprimerConfig = new System.Windows.Forms.Button();
            this.lbxConfig = new System.Windows.Forms.ListBox();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.lbxMois = new System.Windows.Forms.ComboBox();
            this.btnSupprimer = new System.Windows.Forms.Button();
            this.btnOuvrir = new System.Windows.Forms.Button();
            this.btnEnvoyerRapports = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnQuitter = new System.Windows.Forms.Button();
            this.lblVersion = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.btnMenu1 = new System.Windows.Forms.Button();
            this.btnMenu2 = new System.Windows.Forms.Button();
            this.btnMenu3 = new System.Windows.Forms.Button();
            this.btnMenu4 = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnMenu7 = new System.Windows.Forms.Button();
            this.btnMenu6 = new System.Windows.Forms.Button();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.btnMenu5 = new System.Windows.Forms.Button();
            this.menu1 = new System.Windows.Forms.GroupBox();
            this.lblNbrSelectionRapportsGenere = new System.Windows.Forms.Label();
            this.lblNbrSelectionRapports = new System.Windows.Forms.Label();
            this.btnUnselectALLrapports = new System.Windows.Forms.Button();
            this.btnSelectALLrapports = new System.Windows.Forms.Button();
            this.btnUnselectAllGeneratedRapports = new System.Windows.Forms.Button();
            this.btnSelectAllGeneratedRapports = new System.Windows.Forms.Button();
            this.lblprogressbar = new System.Windows.Forms.Label();
            this.lbxDocumentsGeneres = new System.Windows.Forms.ListView();
            this.lbxRapports = new System.Windows.Forms.ListView();
            this.label503 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.menu3 = new System.Windows.Forms.GroupBox();
            this.btnEnregistrerParametreEnvoi = new System.Windows.Forms.Button();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.tbxOptionCC = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.tbxOptionMessage = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tbxOptionTitre = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.menu4 = new System.Windows.Forms.GroupBox();
            this.rdbNon = new System.Windows.Forms.RadioButton();
            this.rdbOui = new System.Windows.Forms.RadioButton();
            this.label12 = new System.Windows.Forms.Label();
            this.tbxOptionGeneratedFilePath = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.menu5 = new System.Windows.Forms.GroupBox();
            this.btnClearLogs = new System.Windows.Forms.Button();
            this.lbxLogs = new System.Windows.Forms.ListView();
            this.label24 = new System.Windows.Forms.Label();
            this.cbxEmploye1 = new System.Windows.Forms.ComboBox();
            this.cbxEmploye2 = new System.Windows.Forms.ComboBox();
            this.cbxEmploye3 = new System.Windows.Forms.ComboBox();
            this.cbxEmploye4 = new System.Windows.Forms.ComboBox();
            this.cbxEmploye5 = new System.Windows.Forms.ComboBox();
            this.cbxEmploye6 = new System.Windows.Forms.ComboBox();
            this.cbxEmploye7 = new System.Windows.Forms.ComboBox();
            this.cbxEmploye8 = new System.Windows.Forms.ComboBox();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.tbxPrixJour1 = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.tbxPrixJour2 = new System.Windows.Forms.TextBox();
            this.tbxPrixJour3 = new System.Windows.Forms.TextBox();
            this.tbxPrixJour4 = new System.Windows.Forms.TextBox();
            this.tbxPrixJour5 = new System.Windows.Forms.TextBox();
            this.tbxPrixJour6 = new System.Windows.Forms.TextBox();
            this.tbxPrixJour7 = new System.Windows.Forms.TextBox();
            this.tbxPrixJour8 = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.tbxNbrJours1 = new System.Windows.Forms.TextBox();
            this.tbxNbrJours2 = new System.Windows.Forms.TextBox();
            this.tbxNbrJours4 = new System.Windows.Forms.TextBox();
            this.tbxNbrJours3 = new System.Windows.Forms.TextBox();
            this.tbxNbrJours5 = new System.Windows.Forms.TextBox();
            this.tbxNbrJours6 = new System.Windows.Forms.TextBox();
            this.tbxNbrJours7 = new System.Windows.Forms.TextBox();
            this.tbxNbrJours8 = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.tbxRevenue3 = new System.Windows.Forms.TextBox();
            this.tbxRevenue2 = new System.Windows.Forms.TextBox();
            this.tbxRevenue4 = new System.Windows.Forms.TextBox();
            this.tbxRevenue5 = new System.Windows.Forms.TextBox();
            this.tbxRevenue6 = new System.Windows.Forms.TextBox();
            this.tbxRevenue7 = new System.Windows.Forms.TextBox();
            this.tbxRevenue8 = new System.Windows.Forms.TextBox();
            this.lblTotal = new System.Windows.Forms.Label();
            this.lbxMois2 = new System.Windows.Forms.ComboBox();
            this.btnGenererAtlanse = new System.Windows.Forms.Button();
            this.menu6 = new System.Windows.Forms.GroupBox();
            this.tbxRevenue1 = new System.Windows.Forms.TextBox();
            this.lblNomPrenomATL = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.lbxATLEmployeInformation = new System.Windows.Forms.ListView();
            this.colDate = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colProjet = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colHeure = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colTypeHeure = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colCommentaire = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btnATLpreview8 = new System.Windows.Forms.Button();
            this.btnATLpreview7 = new System.Windows.Forms.Button();
            this.btnATLpreview6 = new System.Windows.Forms.Button();
            this.btnATLpreview5 = new System.Windows.Forms.Button();
            this.btnATLpreview4 = new System.Windows.Forms.Button();
            this.btnATLpreview3 = new System.Windows.Forms.Button();
            this.btnATLpreview2 = new System.Windows.Forms.Button();
            this.btnATLpreview1 = new System.Windows.Forms.Button();
            this.menu7 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label38 = new System.Windows.Forms.Label();
            this.tbxAdminMAIL_MailDenvoi = new System.Windows.Forms.TextBox();
            this.tbxAdminMAIL_Port = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.btnSaveMAIL = new System.Windows.Forms.Button();
            this.tbxAdminMAIL_SMTP = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.btnVoirMDP = new System.Windows.Forms.Button();
            this.label39 = new System.Windows.Forms.Label();
            this.tbxAdminAPI_user = new System.Windows.Forms.TextBox();
            this.btnSaveAPI = new System.Windows.Forms.Button();
            this.tbxAdminAPI_password = new System.Windows.Forms.TextBox();
            this.label41 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnSaveDIVERS = new System.Windows.Forms.Button();
            this.tbxAdminDIVERS_backupname = new System.Windows.Forms.TextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnSaveHTML = new System.Windows.Forms.Button();
            this.tbxAdminHTML_logoWidth = new System.Windows.Forms.TextBox();
            this.tbxAdminHTML_colonne5largeur = new System.Windows.Forms.TextBox();
            this.tbxAdminHTML_colonne5nom = new System.Windows.Forms.TextBox();
            this.tbxAdminHTML_colonne4largeur = new System.Windows.Forms.TextBox();
            this.tbxAdminHTML_colonne4nom = new System.Windows.Forms.TextBox();
            this.tbxAdminHTML_colonne3largeur = new System.Windows.Forms.TextBox();
            this.tbxAdminHTML_colonne3nom = new System.Windows.Forms.TextBox();
            this.tbxAdminHTML_colonne2largeur = new System.Windows.Forms.TextBox();
            this.tbxAdminHTML_colonne2nom = new System.Windows.Forms.TextBox();
            this.tbxAdminHTML_colonne1largeur = new System.Windows.Forms.TextBox();
            this.tbxAdminHTML_colonne1nom = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.tbxAdminHTML_CSS = new System.Windows.Forms.TextBox();
            this.tbxAdminHTML_logo = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.menu2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.menu1.SuspendLayout();
            this.menu3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.menu4.SuspendLayout();
            this.menu5.SuspendLayout();
            this.menu6.SuspendLayout();
            this.menu7.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblGUID
            // 
            this.lblGUID.AutoSize = true;
            this.lblGUID.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.lblGUID.Location = new System.Drawing.Point(1414, -18);
            this.lblGUID.Name = "lblGUID";
            this.lblGUID.Size = new System.Drawing.Size(34, 13);
            this.lblGUID.TabIndex = 0;
            this.lblGUID.Text = "GUID";
            // 
            // progressbarProjet
            // 
            this.progressbarProjet.Location = new System.Drawing.Point(541, 230);
            this.progressbarProjet.Name = "progressbarProjet";
            this.progressbarProjet.Size = new System.Drawing.Size(413, 10);
            this.progressbarProjet.TabIndex = 9;
            // 
            // tbxUsers
            // 
            this.tbxUsers.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.tbxUsers.Location = new System.Drawing.Point(1348, -23);
            this.tbxUsers.Name = "tbxUsers";
            this.tbxUsers.Size = new System.Drawing.Size(100, 22);
            this.tbxUsers.TabIndex = 11;
            this.tbxUsers.Visible = false;
            // 
            // btnGenerer
            // 
            this.btnGenerer.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.btnGenerer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGenerer.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGenerer.Location = new System.Drawing.Point(758, 499);
            this.btnGenerer.Name = "btnGenerer";
            this.btnGenerer.Size = new System.Drawing.Size(223, 40);
            this.btnGenerer.TabIndex = 12;
            this.btnGenerer.Text = "Générer";
            this.btnGenerer.UseVisualStyleBackColor = false;
            this.btnGenerer.Click += new System.EventHandler(this.btnGenerer_Click);
            // 
            // menu2
            // 
            this.menu2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.menu2.Controls.Add(this.label3);
            this.menu2.Controls.Add(this.label14);
            this.menu2.Controls.Add(this.label1);
            this.menu2.Controls.Add(this.label13);
            this.menu2.Controls.Add(this.btnAjouterConfig);
            this.menu2.Controls.Add(this.label8);
            this.menu2.Controls.Add(this.cbxHeureParJour);
            this.menu2.Controls.Add(this.cbxProjets);
            this.menu2.Controls.Add(this.progressbarProjet);
            this.menu2.Controls.Add(this.cbxEmploye);
            this.menu2.Controls.Add(this.btnSupprimerConfig);
            this.menu2.Controls.Add(this.lbxConfig);
            this.menu2.ForeColor = System.Drawing.Color.Black;
            this.menu2.Location = new System.Drawing.Point(1259, 33);
            this.menu2.Name = "menu2";
            this.menu2.Size = new System.Drawing.Size(1020, 1042);
            this.menu2.TabIndex = 16;
            this.menu2.TabStop = false;
            this.menu2.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.Control;
            this.label3.Location = new System.Drawing.Point(19, 75);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(176, 30);
            this.label3.TabIndex = 36;
            this.label3.Text = "Rapports désirés";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.SystemColors.Control;
            this.label14.Location = new System.Drawing.Point(539, 252);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(101, 20);
            this.label14.TabIndex = 41;
            this.label14.Text = "Heures / jour";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.Control;
            this.label1.Location = new System.Drawing.Point(537, 175);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 20);
            this.label1.TabIndex = 40;
            this.label1.Text = "Projets";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.SystemColors.Control;
            this.label13.Location = new System.Drawing.Point(537, 107);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(69, 20);
            this.label13.TabIndex = 39;
            this.label13.Text = "Employé";
            // 
            // btnAjouterConfig
            // 
            this.btnAjouterConfig.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.btnAjouterConfig.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAjouterConfig.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAjouterConfig.Location = new System.Drawing.Point(541, 313);
            this.btnAjouterConfig.Name = "btnAjouterConfig";
            this.btnAjouterConfig.Size = new System.Drawing.Size(413, 43);
            this.btnAjouterConfig.TabIndex = 23;
            this.btnAjouterConfig.Text = "Ajouter";
            this.btnAjouterConfig.UseVisualStyleBackColor = false;
            this.btnAjouterConfig.Click += new System.EventHandler(this.btnAjouterConfig_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Segoe UI", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.SystemColors.Control;
            this.label8.Location = new System.Drawing.Point(12, 17);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(428, 45);
            this.label8.TabIndex = 35;
            this.label8.Text = "Configuration des rapports";
            // 
            // cbxHeureParJour
            // 
            this.cbxHeureParJour.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbxHeureParJour.FormattingEnabled = true;
            this.cbxHeureParJour.Location = new System.Drawing.Point(541, 275);
            this.cbxHeureParJour.Name = "cbxHeureParJour";
            this.cbxHeureParJour.Size = new System.Drawing.Size(209, 28);
            this.cbxHeureParJour.Sorted = true;
            this.cbxHeureParJour.TabIndex = 22;
            // 
            // cbxProjets
            // 
            this.cbxProjets.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbxProjets.FormattingEnabled = true;
            this.cbxProjets.Location = new System.Drawing.Point(541, 203);
            this.cbxProjets.Name = "cbxProjets";
            this.cbxProjets.Size = new System.Drawing.Size(413, 28);
            this.cbxProjets.Sorted = true;
            this.cbxProjets.TabIndex = 20;
            // 
            // cbxEmploye
            // 
            this.cbxEmploye.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbxEmploye.FormattingEnabled = true;
            this.cbxEmploye.Location = new System.Drawing.Point(541, 130);
            this.cbxEmploye.Name = "cbxEmploye";
            this.cbxEmploye.Size = new System.Drawing.Size(413, 28);
            this.cbxEmploye.Sorted = true;
            this.cbxEmploye.TabIndex = 17;
            this.cbxEmploye.SelectedIndexChanged += new System.EventHandler(this.cbxEmploye_SelectedIndexChanged);
            // 
            // btnSupprimerConfig
            // 
            this.btnSupprimerConfig.BackColor = System.Drawing.Color.LightGray;
            this.btnSupprimerConfig.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSupprimerConfig.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.btnSupprimerConfig.Location = new System.Drawing.Point(24, 516);
            this.btnSupprimerConfig.Name = "btnSupprimerConfig";
            this.btnSupprimerConfig.Size = new System.Drawing.Size(161, 23);
            this.btnSupprimerConfig.TabIndex = 17;
            this.btnSupprimerConfig.Text = "Supprimer";
            this.btnSupprimerConfig.UseVisualStyleBackColor = false;
            this.btnSupprimerConfig.Click += new System.EventHandler(this.btnSupprimerConfig_Click);
            // 
            // lbxConfig
            // 
            this.lbxConfig.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.lbxConfig.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.lbxConfig.FormattingEnabled = true;
            this.lbxConfig.ItemHeight = 21;
            this.lbxConfig.Location = new System.Drawing.Point(24, 107);
            this.lbxConfig.Name = "lbxConfig";
            this.lbxConfig.Size = new System.Drawing.Size(511, 403);
            this.lbxConfig.Sorted = true;
            this.lbxConfig.TabIndex = 17;
            // 
            // lbxMois
            // 
            this.lbxMois.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.lbxMois.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbxMois.FormattingEnabled = true;
            this.lbxMois.Location = new System.Drawing.Point(787, 53);
            this.lbxMois.Name = "lbxMois";
            this.lbxMois.Size = new System.Drawing.Size(194, 28);
            this.lbxMois.TabIndex = 24;
            this.lbxMois.SelectionChangeCommitted += new System.EventHandler(this.lbxMois_SelectionChangeCommitted);
            // 
            // btnSupprimer
            // 
            this.btnSupprimer.BackColor = System.Drawing.Color.LightGray;
            this.btnSupprimer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSupprimer.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.btnSupprimer.Location = new System.Drawing.Point(268, 961);
            this.btnSupprimer.Name = "btnSupprimer";
            this.btnSupprimer.Size = new System.Drawing.Size(127, 23);
            this.btnSupprimer.TabIndex = 28;
            this.btnSupprimer.Text = "Supprimer";
            this.btnSupprimer.UseVisualStyleBackColor = false;
            this.btnSupprimer.Click += new System.EventHandler(this.btnSupprimer_Click);
            // 
            // btnOuvrir
            // 
            this.btnOuvrir.BackColor = System.Drawing.Color.LightGray;
            this.btnOuvrir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOuvrir.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.btnOuvrir.Location = new System.Drawing.Point(401, 961);
            this.btnOuvrir.Name = "btnOuvrir";
            this.btnOuvrir.Size = new System.Drawing.Size(127, 23);
            this.btnOuvrir.TabIndex = 29;
            this.btnOuvrir.Text = "Ouvrir";
            this.btnOuvrir.UseVisualStyleBackColor = false;
            this.btnOuvrir.Click += new System.EventHandler(this.btnOuvrir_Click);
            // 
            // btnEnvoyerRapports
            // 
            this.btnEnvoyerRapports.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.btnEnvoyerRapports.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEnvoyerRapports.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEnvoyerRapports.Location = new System.Drawing.Point(758, 961);
            this.btnEnvoyerRapports.Name = "btnEnvoyerRapports";
            this.btnEnvoyerRapports.Size = new System.Drawing.Size(223, 40);
            this.btnEnvoyerRapports.TabIndex = 30;
            this.btnEnvoyerRapports.Text = "Envoyer rapports";
            this.btnEnvoyerRapports.UseVisualStyleBackColor = false;
            this.btnEnvoyerRapports.Click += new System.EventHandler(this.btnEnvoyerRapports_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.Control;
            this.label4.Location = new System.Drawing.Point(15, 57);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(103, 30);
            this.label4.TabIndex = 32;
            this.label4.Text = "Rapports";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(104)))), ((int)(((byte)(104)))));
            this.panel1.Controls.Add(this.btnQuitter);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1325, 33);
            this.panel1.TabIndex = 36;
            this.panel1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseDown);
            this.panel1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseMove);
            this.panel1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseUp);
            // 
            // btnQuitter
            // 
            this.btnQuitter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(104)))), ((int)(((byte)(104)))));
            this.btnQuitter.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnQuitter.Font = new System.Drawing.Font("Verdana", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnQuitter.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(104)))), ((int)(((byte)(104)))));
            this.btnQuitter.Image = ((System.Drawing.Image)(resources.GetObject("btnQuitter.Image")));
            this.btnQuitter.Location = new System.Drawing.Point(1218, -3);
            this.btnQuitter.Margin = new System.Windows.Forms.Padding(0);
            this.btnQuitter.Name = "btnQuitter";
            this.btnQuitter.Size = new System.Drawing.Size(35, 35);
            this.btnQuitter.TabIndex = 37;
            this.btnQuitter.UseVisualStyleBackColor = false;
            this.btnQuitter.Click += new System.EventHandler(this.btnQuitter_Click);
            // 
            // lblVersion
            // 
            this.lblVersion.AutoSize = true;
            this.lblVersion.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVersion.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.lblVersion.Location = new System.Drawing.Point(19, 103);
            this.lblVersion.Name = "lblVersion";
            this.lblVersion.Size = new System.Drawing.Size(72, 15);
            this.lblVersion.TabIndex = 38;
            this.lblVersion.Text = "Version 1.2.8";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label6.Location = new System.Drawing.Point(18, 80);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(172, 20);
            this.label6.TabIndex = 37;
            this.label6.Text = "Génération de rapports";
            // 
            // btnMenu1
            // 
            this.btnMenu1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(63)))), ((int)(((byte)(110)))));
            this.btnMenu1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMenu1.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMenu1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnMenu1.Location = new System.Drawing.Point(-1, 166);
            this.btnMenu1.Name = "btnMenu1";
            this.btnMenu1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.btnMenu1.Size = new System.Drawing.Size(235, 40);
            this.btnMenu1.TabIndex = 37;
            this.btnMenu1.Text = "Génération et envois";
            this.btnMenu1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnMenu1.UseVisualStyleBackColor = false;
            this.btnMenu1.Click += new System.EventHandler(this.btnMenu1_Click);
            // 
            // btnMenu2
            // 
            this.btnMenu2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(63)))), ((int)(((byte)(70)))));
            this.btnMenu2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMenu2.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMenu2.ForeColor = System.Drawing.SystemColors.Control;
            this.btnMenu2.Location = new System.Drawing.Point(-1, 244);
            this.btnMenu2.Name = "btnMenu2";
            this.btnMenu2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.btnMenu2.Size = new System.Drawing.Size(235, 40);
            this.btnMenu2.TabIndex = 38;
            this.btnMenu2.Text = "Configuration des rapports";
            this.btnMenu2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnMenu2.UseVisualStyleBackColor = false;
            this.btnMenu2.Click += new System.EventHandler(this.btnMenu2_Click);
            // 
            // btnMenu3
            // 
            this.btnMenu3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(63)))), ((int)(((byte)(70)))));
            this.btnMenu3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMenu3.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMenu3.ForeColor = System.Drawing.SystemColors.Control;
            this.btnMenu3.Location = new System.Drawing.Point(-1, 283);
            this.btnMenu3.Name = "btnMenu3";
            this.btnMenu3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.btnMenu3.Size = new System.Drawing.Size(235, 40);
            this.btnMenu3.TabIndex = 39;
            this.btnMenu3.Text = "Configuration des envois";
            this.btnMenu3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnMenu3.UseVisualStyleBackColor = false;
            this.btnMenu3.Click += new System.EventHandler(this.btnMenu3_Click);
            // 
            // btnMenu4
            // 
            this.btnMenu4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(63)))), ((int)(((byte)(70)))));
            this.btnMenu4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMenu4.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMenu4.ForeColor = System.Drawing.SystemColors.Control;
            this.btnMenu4.Location = new System.Drawing.Point(-2, 322);
            this.btnMenu4.Name = "btnMenu4";
            this.btnMenu4.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.btnMenu4.Size = new System.Drawing.Size(236, 40);
            this.btnMenu4.TabIndex = 40;
            this.btnMenu4.Text = "Configuration divers";
            this.btnMenu4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnMenu4.UseVisualStyleBackColor = false;
            this.btnMenu4.Click += new System.EventHandler(this.btnMenu4_Click);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(49)))), ((int)(((byte)(54)))));
            this.panel2.Controls.Add(this.btnMenu7);
            this.panel2.Controls.Add(this.btnMenu6);
            this.panel2.Controls.Add(this.lblVersion);
            this.panel2.Controls.Add(this.pictureBox3);
            this.panel2.Controls.Add(this.btnMenu5);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Location = new System.Drawing.Point(-2, 31);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(235, 1042);
            this.panel2.TabIndex = 41;
            // 
            // btnMenu7
            // 
            this.btnMenu7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(63)))), ((int)(((byte)(70)))));
            this.btnMenu7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMenu7.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMenu7.ForeColor = System.Drawing.Color.OrangeRed;
            this.btnMenu7.Location = new System.Drawing.Point(-1, 369);
            this.btnMenu7.Name = "btnMenu7";
            this.btnMenu7.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.btnMenu7.Size = new System.Drawing.Size(236, 40);
            this.btnMenu7.TabIndex = 45;
            this.btnMenu7.Text = "Administration";
            this.btnMenu7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnMenu7.UseVisualStyleBackColor = false;
            this.btnMenu7.Visible = false;
            this.btnMenu7.Click += new System.EventHandler(this.btnMenu7_Click);
            // 
            // btnMenu6
            // 
            this.btnMenu6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(63)))), ((int)(((byte)(70)))));
            this.btnMenu6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMenu6.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMenu6.ForeColor = System.Drawing.SystemColors.Control;
            this.btnMenu6.Location = new System.Drawing.Point(0, 174);
            this.btnMenu6.Name = "btnMenu6";
            this.btnMenu6.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.btnMenu6.Size = new System.Drawing.Size(236, 40);
            this.btnMenu6.TabIndex = 44;
            this.btnMenu6.Text = "Check Atlanse";
            this.btnMenu6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnMenu6.UseVisualStyleBackColor = false;
            this.btnMenu6.Click += new System.EventHandler(this.btnMenu6_Click);
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox3.BackgroundImage")));
            this.pictureBox3.ErrorImage = null;
            this.pictureBox3.Location = new System.Drawing.Point(22, 14);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(100, 56);
            this.pictureBox3.TabIndex = 43;
            this.pictureBox3.TabStop = false;
            this.pictureBox3.Click += new System.EventHandler(this.pictureBox3_Click);
            // 
            // btnMenu5
            // 
            this.btnMenu5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(63)))), ((int)(((byte)(70)))));
            this.btnMenu5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMenu5.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMenu5.ForeColor = System.Drawing.SystemColors.Control;
            this.btnMenu5.Location = new System.Drawing.Point(0, 330);
            this.btnMenu5.Name = "btnMenu5";
            this.btnMenu5.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.btnMenu5.Size = new System.Drawing.Size(236, 40);
            this.btnMenu5.TabIndex = 42;
            this.btnMenu5.Text = "Logs";
            this.btnMenu5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnMenu5.UseVisualStyleBackColor = false;
            this.btnMenu5.Click += new System.EventHandler(this.btnMenu5_Click);
            // 
            // menu1
            // 
            this.menu1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.menu1.Controls.Add(this.textBox1);
            this.menu1.Controls.Add(this.button1);
            this.menu1.Controls.Add(this.lblNbrSelectionRapportsGenere);
            this.menu1.Controls.Add(this.lblNbrSelectionRapports);
            this.menu1.Controls.Add(this.btnUnselectALLrapports);
            this.menu1.Controls.Add(this.btnSelectALLrapports);
            this.menu1.Controls.Add(this.btnUnselectAllGeneratedRapports);
            this.menu1.Controls.Add(this.btnSelectAllGeneratedRapports);
            this.menu1.Controls.Add(this.lblprogressbar);
            this.menu1.Controls.Add(this.lbxDocumentsGeneres);
            this.menu1.Controls.Add(this.lbxRapports);
            this.menu1.Controls.Add(this.label503);
            this.menu1.Controls.Add(this.label7);
            this.menu1.Controls.Add(this.label4);
            this.menu1.Controls.Add(this.btnGenerer);
            this.menu1.Controls.Add(this.lbxMois);
            this.menu1.Controls.Add(this.btnSupprimer);
            this.menu1.Controls.Add(this.btnEnvoyerRapports);
            this.menu1.Controls.Add(this.btnOuvrir);
            this.menu1.Location = new System.Drawing.Point(233, 33);
            this.menu1.Name = "menu1";
            this.menu1.Size = new System.Drawing.Size(1020, 1042);
            this.menu1.TabIndex = 24;
            this.menu1.TabStop = false;
            this.menu1.Visible = false;
            // 
            // lblNbrSelectionRapportsGenere
            // 
            this.lblNbrSelectionRapportsGenere.AutoSize = true;
            this.lblNbrSelectionRapportsGenere.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNbrSelectionRapportsGenere.ForeColor = System.Drawing.SystemColors.Control;
            this.lblNbrSelectionRapportsGenere.Location = new System.Drawing.Point(209, 532);
            this.lblNbrSelectionRapportsGenere.Name = "lblNbrSelectionRapportsGenere";
            this.lblNbrSelectionRapportsGenere.Size = new System.Drawing.Size(80, 17);
            this.lblNbrSelectionRapportsGenere.TabIndex = 49;
            this.lblNbrSelectionRapportsGenere.Text = "Coché(s) : 0";
            // 
            // lblNbrSelectionRapports
            // 
            this.lblNbrSelectionRapports.AutoSize = true;
            this.lblNbrSelectionRapports.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNbrSelectionRapports.ForeColor = System.Drawing.SystemColors.Control;
            this.lblNbrSelectionRapports.Location = new System.Drawing.Point(123, 70);
            this.lblNbrSelectionRapports.Name = "lblNbrSelectionRapports";
            this.lblNbrSelectionRapports.Size = new System.Drawing.Size(80, 17);
            this.lblNbrSelectionRapports.TabIndex = 48;
            this.lblNbrSelectionRapports.Text = "Coché(s) : 0";
            // 
            // btnUnselectALLrapports
            // 
            this.btnUnselectALLrapports.BackColor = System.Drawing.Color.LightGray;
            this.btnUnselectALLrapports.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUnselectALLrapports.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.btnUnselectALLrapports.Location = new System.Drawing.Point(144, 499);
            this.btnUnselectALLrapports.Name = "btnUnselectALLrapports";
            this.btnUnselectALLrapports.Size = new System.Drawing.Size(118, 23);
            this.btnUnselectALLrapports.TabIndex = 47;
            this.btnUnselectALLrapports.Text = "Tout décocher";
            this.btnUnselectALLrapports.UseVisualStyleBackColor = false;
            this.btnUnselectALLrapports.Click += new System.EventHandler(this.btnUnselectALLrapports_Click);
            // 
            // btnSelectALLrapports
            // 
            this.btnSelectALLrapports.BackColor = System.Drawing.Color.LightGray;
            this.btnSelectALLrapports.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSelectALLrapports.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.btnSelectALLrapports.Location = new System.Drawing.Point(20, 499);
            this.btnSelectALLrapports.Name = "btnSelectALLrapports";
            this.btnSelectALLrapports.Size = new System.Drawing.Size(118, 23);
            this.btnSelectALLrapports.TabIndex = 46;
            this.btnSelectALLrapports.Text = "Tout cocher";
            this.btnSelectALLrapports.UseVisualStyleBackColor = false;
            this.btnSelectALLrapports.Click += new System.EventHandler(this.btnSelectALLrapports_Click);
            // 
            // btnUnselectAllGeneratedRapports
            // 
            this.btnUnselectAllGeneratedRapports.BackColor = System.Drawing.Color.LightGray;
            this.btnUnselectAllGeneratedRapports.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUnselectAllGeneratedRapports.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.btnUnselectAllGeneratedRapports.Location = new System.Drawing.Point(144, 961);
            this.btnUnselectAllGeneratedRapports.Name = "btnUnselectAllGeneratedRapports";
            this.btnUnselectAllGeneratedRapports.Size = new System.Drawing.Size(118, 23);
            this.btnUnselectAllGeneratedRapports.TabIndex = 45;
            this.btnUnselectAllGeneratedRapports.Text = "Tout décocher";
            this.btnUnselectAllGeneratedRapports.UseVisualStyleBackColor = false;
            this.btnUnselectAllGeneratedRapports.Click += new System.EventHandler(this.btnUnselectAllGeneratedRapports_Click);
            // 
            // btnSelectAllGeneratedRapports
            // 
            this.btnSelectAllGeneratedRapports.BackColor = System.Drawing.Color.LightGray;
            this.btnSelectAllGeneratedRapports.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSelectAllGeneratedRapports.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.btnSelectAllGeneratedRapports.Location = new System.Drawing.Point(20, 961);
            this.btnSelectAllGeneratedRapports.Name = "btnSelectAllGeneratedRapports";
            this.btnSelectAllGeneratedRapports.Size = new System.Drawing.Size(118, 23);
            this.btnSelectAllGeneratedRapports.TabIndex = 44;
            this.btnSelectAllGeneratedRapports.Text = "Tout cocher";
            this.btnSelectAllGeneratedRapports.UseVisualStyleBackColor = false;
            this.btnSelectAllGeneratedRapports.Click += new System.EventHandler(this.btnSelectAllGeneratedRapports_Click);
            // 
            // lblprogressbar
            // 
            this.lblprogressbar.AutoSize = true;
            this.lblprogressbar.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.lblprogressbar.ForeColor = System.Drawing.SystemColors.Control;
            this.lblprogressbar.Location = new System.Drawing.Point(268, 504);
            this.lblprogressbar.Name = "lblprogressbar";
            this.lblprogressbar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblprogressbar.Size = new System.Drawing.Size(11, 13);
            this.lblprogressbar.TabIndex = 43;
            this.lblprogressbar.Text = "-";
            // 
            // lbxDocumentsGeneres
            // 
            this.lbxDocumentsGeneres.Alignment = System.Windows.Forms.ListViewAlignment.Left;
            this.lbxDocumentsGeneres.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.lbxDocumentsGeneres.CheckBoxes = true;
            this.lbxDocumentsGeneres.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbxDocumentsGeneres.FullRowSelect = true;
            this.lbxDocumentsGeneres.GridLines = true;
            this.lbxDocumentsGeneres.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.lbxDocumentsGeneres.HideSelection = false;
            this.lbxDocumentsGeneres.Location = new System.Drawing.Point(20, 553);
            this.lbxDocumentsGeneres.Name = "lbxDocumentsGeneres";
            this.lbxDocumentsGeneres.Size = new System.Drawing.Size(961, 402);
            this.lbxDocumentsGeneres.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.lbxDocumentsGeneres.TabIndex = 41;
            this.lbxDocumentsGeneres.UseCompatibleStateImageBehavior = false;
            this.lbxDocumentsGeneres.View = System.Windows.Forms.View.Details;
            this.lbxDocumentsGeneres.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.lbxDocumentsGeneres_ItemChecked);
            // 
            // lbxRapports
            // 
            this.lbxRapports.Alignment = System.Windows.Forms.ListViewAlignment.Left;
            this.lbxRapports.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.lbxRapports.CheckBoxes = true;
            this.lbxRapports.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbxRapports.FullRowSelect = true;
            this.lbxRapports.GridLines = true;
            this.lbxRapports.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.lbxRapports.HideSelection = false;
            this.lbxRapports.Location = new System.Drawing.Point(20, 92);
            this.lbxRapports.Name = "lbxRapports";
            this.lbxRapports.Size = new System.Drawing.Size(961, 402);
            this.lbxRapports.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.lbxRapports.TabIndex = 40;
            this.lbxRapports.UseCompatibleStateImageBehavior = false;
            this.lbxRapports.View = System.Windows.Forms.View.Details;
            this.lbxRapports.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.lbxRapports_ItemChecked);
            // 
            // label503
            // 
            this.label503.AutoSize = true;
            this.label503.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label503.ForeColor = System.Drawing.SystemColors.Control;
            this.label503.Location = new System.Drawing.Point(18, 520);
            this.label503.Name = "label503";
            this.label503.Size = new System.Drawing.Size(185, 30);
            this.label503.TabIndex = 35;
            this.label503.Text = "Rapports générés";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Segoe UI", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.SystemColors.Control;
            this.label7.Location = new System.Drawing.Point(12, 17);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(329, 45);
            this.label7.TabIndex = 34;
            this.label7.Text = "Génération et envois";
            // 
            // menu3
            // 
            this.menu3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.menu3.Controls.Add(this.btnEnregistrerParametreEnvoi);
            this.menu3.Controls.Add(this.label22);
            this.menu3.Controls.Add(this.label21);
            this.menu3.Controls.Add(this.label20);
            this.menu3.Controls.Add(this.label19);
            this.menu3.Controls.Add(this.pictureBox2);
            this.menu3.Controls.Add(this.label18);
            this.menu3.Controls.Add(this.label17);
            this.menu3.Controls.Add(this.pictureBox1);
            this.menu3.Controls.Add(this.tbxOptionCC);
            this.menu3.Controls.Add(this.label15);
            this.menu3.Controls.Add(this.label16);
            this.menu3.Controls.Add(this.tbxOptionMessage);
            this.menu3.Controls.Add(this.label5);
            this.menu3.Controls.Add(this.tbxOptionTitre);
            this.menu3.Controls.Add(this.label9);
            this.menu3.Location = new System.Drawing.Point(1260, 33);
            this.menu3.Name = "menu3";
            this.menu3.Size = new System.Drawing.Size(1020, 1042);
            this.menu3.TabIndex = 35;
            this.menu3.TabStop = false;
            this.menu3.Visible = false;
            // 
            // btnEnregistrerParametreEnvoi
            // 
            this.btnEnregistrerParametreEnvoi.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.btnEnregistrerParametreEnvoi.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEnregistrerParametreEnvoi.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.btnEnregistrerParametreEnvoi.Location = new System.Drawing.Point(343, 434);
            this.btnEnregistrerParametreEnvoi.Name = "btnEnregistrerParametreEnvoi";
            this.btnEnregistrerParametreEnvoi.Size = new System.Drawing.Size(127, 23);
            this.btnEnregistrerParametreEnvoi.TabIndex = 36;
            this.btnEnregistrerParametreEnvoi.Text = "Enregistrer";
            this.btnEnregistrerParametreEnvoi.UseVisualStyleBackColor = false;
            this.btnEnregistrerParametreEnvoi.Click += new System.EventHandler(this.btnEnregistrerParametreEnvoi_Click);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.SystemColors.Control;
            this.label22.Location = new System.Drawing.Point(501, 330);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(155, 17);
            this.label22.TabIndex = 54;
            this.label22.Text = "{projet} = Nom du projet";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.SystemColors.Control;
            this.label21.Location = new System.Drawing.Point(501, 310);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(167, 17);
            this.label21.TabIndex = 53;
            this.label21.Text = "{nom} = Nom de l\'employé";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.SystemColors.Control;
            this.label20.Location = new System.Drawing.Point(501, 290);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(203, 17);
            this.label20.TabIndex = 52;
            this.label20.Text = "{prenom} = Prénom de l\'employé";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.SystemColors.Control;
            this.label19.Location = new System.Drawing.Point(501, 270);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(196, 17);
            this.label19.TabIndex = 51;
            this.label19.Text = "Liste des variables utilisables :";
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox2.BackgroundImage")));
            this.pictureBox2.Location = new System.Drawing.Point(476, 298);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(20, 20);
            this.pictureBox2.TabIndex = 50;
            this.pictureBox2.TabStop = false;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.SystemColors.Control;
            this.label18.Location = new System.Drawing.Point(500, 164);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(270, 17);
            this.label18.TabIndex = 49;
            this.label18.Text = "Exemple: mail@domain.ch, mail2@domain.ch";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.SystemColors.Control;
            this.label17.Location = new System.Drawing.Point(500, 147);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(203, 17);
            this.label17.TabIndex = 48;
            this.label17.Text = "Séparer les mails par une virgule.";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.BackgroundImage")));
            this.pictureBox1.Location = new System.Drawing.Point(474, 152);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(20, 20);
            this.pictureBox1.TabIndex = 47;
            this.pictureBox1.TabStop = false;
            // 
            // tbxOptionCC
            // 
            this.tbxOptionCC.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.tbxOptionCC.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxOptionCC.Location = new System.Drawing.Point(25, 152);
            this.tbxOptionCC.Name = "tbxOptionCC";
            this.tbxOptionCC.Size = new System.Drawing.Size(445, 22);
            this.tbxOptionCC.TabIndex = 46;
            this.tbxOptionCC.TextChanged += new System.EventHandler(this.tbxOptionCC_TextChanged);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.SystemColors.Control;
            this.label15.Location = new System.Drawing.Point(21, 129);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(27, 20);
            this.label15.TabIndex = 45;
            this.label15.Text = "CC";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.SystemColors.Control;
            this.label16.Location = new System.Drawing.Point(21, 186);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(70, 20);
            this.label16.TabIndex = 44;
            this.label16.Text = "Message";
            // 
            // tbxOptionMessage
            // 
            this.tbxOptionMessage.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.tbxOptionMessage.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxOptionMessage.Location = new System.Drawing.Point(25, 206);
            this.tbxOptionMessage.Multiline = true;
            this.tbxOptionMessage.Name = "tbxOptionMessage";
            this.tbxOptionMessage.Size = new System.Drawing.Size(445, 220);
            this.tbxOptionMessage.TabIndex = 43;
            this.tbxOptionMessage.TextChanged += new System.EventHandler(this.tbxOptionMessage_TextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.Control;
            this.label5.Location = new System.Drawing.Point(21, 79);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(42, 20);
            this.label5.TabIndex = 42;
            this.label5.Text = "Titre";
            // 
            // tbxOptionTitre
            // 
            this.tbxOptionTitre.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.tbxOptionTitre.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxOptionTitre.Location = new System.Drawing.Point(25, 102);
            this.tbxOptionTitre.Name = "tbxOptionTitre";
            this.tbxOptionTitre.Size = new System.Drawing.Size(445, 22);
            this.tbxOptionTitre.TabIndex = 40;
            this.tbxOptionTitre.TextChanged += new System.EventHandler(this.tbxOptionTitre_TextChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Segoe UI", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.SystemColors.Control;
            this.label9.Location = new System.Drawing.Point(12, 17);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(394, 45);
            this.label9.TabIndex = 36;
            this.label9.Text = "Configuration des envois";
            // 
            // menu4
            // 
            this.menu4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.menu4.Controls.Add(this.rdbNon);
            this.menu4.Controls.Add(this.rdbOui);
            this.menu4.Controls.Add(this.label12);
            this.menu4.Controls.Add(this.tbxOptionGeneratedFilePath);
            this.menu4.Controls.Add(this.label10);
            this.menu4.Controls.Add(this.label11);
            this.menu4.Location = new System.Drawing.Point(1259, 33);
            this.menu4.Name = "menu4";
            this.menu4.Size = new System.Drawing.Size(1020, 1042);
            this.menu4.TabIndex = 36;
            this.menu4.TabStop = false;
            this.menu4.Visible = false;
            // 
            // rdbNon
            // 
            this.rdbNon.AutoSize = true;
            this.rdbNon.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdbNon.ForeColor = System.Drawing.SystemColors.Control;
            this.rdbNon.Location = new System.Drawing.Point(76, 159);
            this.rdbNon.Name = "rdbNon";
            this.rdbNon.Size = new System.Drawing.Size(55, 24);
            this.rdbNon.TabIndex = 42;
            this.rdbNon.TabStop = true;
            this.rdbNon.Text = "Non";
            this.rdbNon.UseVisualStyleBackColor = true;
            this.rdbNon.CheckedChanged += new System.EventHandler(this.rdbNon_CheckedChanged);
            // 
            // rdbOui
            // 
            this.rdbOui.AutoSize = true;
            this.rdbOui.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdbOui.ForeColor = System.Drawing.SystemColors.Control;
            this.rdbOui.Location = new System.Drawing.Point(20, 159);
            this.rdbOui.Name = "rdbOui";
            this.rdbOui.Size = new System.Drawing.Size(50, 24);
            this.rdbOui.TabIndex = 41;
            this.rdbOui.TabStop = true;
            this.rdbOui.Text = "Oui";
            this.rdbOui.UseVisualStyleBackColor = true;
            this.rdbOui.CheckedChanged += new System.EventHandler(this.rdbOui_CheckedChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.SystemColors.Control;
            this.label12.Location = new System.Drawing.Point(16, 136);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(363, 20);
            this.label12.TabIndex = 40;
            this.label12.Text = "Ouvrir les rapports une fois la génération terminée";
            // 
            // tbxOptionGeneratedFilePath
            // 
            this.tbxOptionGeneratedFilePath.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.tbxOptionGeneratedFilePath.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxOptionGeneratedFilePath.Location = new System.Drawing.Point(20, 107);
            this.tbxOptionGeneratedFilePath.Name = "tbxOptionGeneratedFilePath";
            this.tbxOptionGeneratedFilePath.ReadOnly = true;
            this.tbxOptionGeneratedFilePath.Size = new System.Drawing.Size(697, 26);
            this.tbxOptionGeneratedFilePath.TabIndex = 39;
            this.tbxOptionGeneratedFilePath.MouseClick += new System.Windows.Forms.MouseEventHandler(this.tbxOptionGeneratedFilePath_MouseClick);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.SystemColors.Control;
            this.label10.Location = new System.Drawing.Point(16, 84);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(310, 20);
            this.label10.TabIndex = 38;
            this.label10.Text = "Dossier de réceptions des rapports générés";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Segoe UI", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.SystemColors.Control;
            this.label11.Location = new System.Drawing.Point(12, 17);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(328, 45);
            this.label11.TabIndex = 37;
            this.label11.Text = "Configuration divers";
            // 
            // menu5
            // 
            this.menu5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.menu5.Controls.Add(this.btnClearLogs);
            this.menu5.Controls.Add(this.lbxLogs);
            this.menu5.Controls.Add(this.label24);
            this.menu5.Location = new System.Drawing.Point(1259, 34);
            this.menu5.Name = "menu5";
            this.menu5.Size = new System.Drawing.Size(1020, 1042);
            this.menu5.TabIndex = 55;
            this.menu5.TabStop = false;
            this.menu5.Visible = false;
            // 
            // btnClearLogs
            // 
            this.btnClearLogs.BackColor = System.Drawing.Color.LightGray;
            this.btnClearLogs.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClearLogs.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.btnClearLogs.Location = new System.Drawing.Point(880, 39);
            this.btnClearLogs.Name = "btnClearLogs";
            this.btnClearLogs.Size = new System.Drawing.Size(127, 23);
            this.btnClearLogs.TabIndex = 43;
            this.btnClearLogs.Text = "Clear logs";
            this.btnClearLogs.UseVisualStyleBackColor = false;
            this.btnClearLogs.Click += new System.EventHandler(this.btnClearLogs_Click);
            // 
            // lbxLogs
            // 
            this.lbxLogs.Alignment = System.Windows.Forms.ListViewAlignment.Left;
            this.lbxLogs.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.lbxLogs.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbxLogs.FullRowSelect = true;
            this.lbxLogs.GridLines = true;
            this.lbxLogs.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.lbxLogs.HideSelection = false;
            this.lbxLogs.Location = new System.Drawing.Point(12, 65);
            this.lbxLogs.Name = "lbxLogs";
            this.lbxLogs.Size = new System.Drawing.Size(995, 906);
            this.lbxLogs.TabIndex = 42;
            this.lbxLogs.UseCompatibleStateImageBehavior = false;
            this.lbxLogs.View = System.Windows.Forms.View.Details;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Segoe UI", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.SystemColors.Control;
            this.label24.Location = new System.Drawing.Point(12, 17);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(90, 45);
            this.label24.TabIndex = 38;
            this.label24.Text = "Logs";
            // 
            // cbxEmploye1
            // 
            this.cbxEmploye1.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.cbxEmploye1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbxEmploye1.FormattingEnabled = true;
            this.cbxEmploye1.Location = new System.Drawing.Point(92, 139);
            this.cbxEmploye1.Name = "cbxEmploye1";
            this.cbxEmploye1.Size = new System.Drawing.Size(460, 28);
            this.cbxEmploye1.Sorted = true;
            this.cbxEmploye1.TabIndex = 25;
            this.cbxEmploye1.SelectedIndexChanged += new System.EventHandler(this.cbxEmploye1_SelectedIndexChanged);
            // 
            // cbxEmploye2
            // 
            this.cbxEmploye2.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.cbxEmploye2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbxEmploye2.FormattingEnabled = true;
            this.cbxEmploye2.Location = new System.Drawing.Point(92, 166);
            this.cbxEmploye2.Name = "cbxEmploye2";
            this.cbxEmploye2.Size = new System.Drawing.Size(460, 28);
            this.cbxEmploye2.Sorted = true;
            this.cbxEmploye2.TabIndex = 26;
            this.cbxEmploye2.SelectedIndexChanged += new System.EventHandler(this.cbxEmploye2_SelectedIndexChanged);
            // 
            // cbxEmploye3
            // 
            this.cbxEmploye3.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.cbxEmploye3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbxEmploye3.FormattingEnabled = true;
            this.cbxEmploye3.Location = new System.Drawing.Point(92, 193);
            this.cbxEmploye3.Name = "cbxEmploye3";
            this.cbxEmploye3.Size = new System.Drawing.Size(460, 28);
            this.cbxEmploye3.Sorted = true;
            this.cbxEmploye3.TabIndex = 27;
            this.cbxEmploye3.SelectedIndexChanged += new System.EventHandler(this.cbxEmploye3_SelectedIndexChanged);
            // 
            // cbxEmploye4
            // 
            this.cbxEmploye4.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.cbxEmploye4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbxEmploye4.FormattingEnabled = true;
            this.cbxEmploye4.Location = new System.Drawing.Point(92, 220);
            this.cbxEmploye4.Name = "cbxEmploye4";
            this.cbxEmploye4.Size = new System.Drawing.Size(460, 28);
            this.cbxEmploye4.Sorted = true;
            this.cbxEmploye4.TabIndex = 28;
            this.cbxEmploye4.SelectedIndexChanged += new System.EventHandler(this.cbxEmploye4_SelectedIndexChanged);
            // 
            // cbxEmploye5
            // 
            this.cbxEmploye5.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.cbxEmploye5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbxEmploye5.FormattingEnabled = true;
            this.cbxEmploye5.Location = new System.Drawing.Point(92, 247);
            this.cbxEmploye5.Name = "cbxEmploye5";
            this.cbxEmploye5.Size = new System.Drawing.Size(460, 28);
            this.cbxEmploye5.Sorted = true;
            this.cbxEmploye5.TabIndex = 29;
            this.cbxEmploye5.SelectedIndexChanged += new System.EventHandler(this.cbxEmploye5_SelectedIndexChanged);
            // 
            // cbxEmploye6
            // 
            this.cbxEmploye6.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.cbxEmploye6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbxEmploye6.FormattingEnabled = true;
            this.cbxEmploye6.Location = new System.Drawing.Point(92, 274);
            this.cbxEmploye6.Name = "cbxEmploye6";
            this.cbxEmploye6.Size = new System.Drawing.Size(460, 28);
            this.cbxEmploye6.Sorted = true;
            this.cbxEmploye6.TabIndex = 30;
            this.cbxEmploye6.SelectedIndexChanged += new System.EventHandler(this.cbxEmploye6_SelectedIndexChanged);
            // 
            // cbxEmploye7
            // 
            this.cbxEmploye7.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.cbxEmploye7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbxEmploye7.FormattingEnabled = true;
            this.cbxEmploye7.Location = new System.Drawing.Point(92, 301);
            this.cbxEmploye7.Name = "cbxEmploye7";
            this.cbxEmploye7.Size = new System.Drawing.Size(460, 28);
            this.cbxEmploye7.Sorted = true;
            this.cbxEmploye7.TabIndex = 31;
            this.cbxEmploye7.SelectedIndexChanged += new System.EventHandler(this.cbxEmploye7_SelectedIndexChanged);
            // 
            // cbxEmploye8
            // 
            this.cbxEmploye8.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.cbxEmploye8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbxEmploye8.FormattingEnabled = true;
            this.cbxEmploye8.Location = new System.Drawing.Point(92, 328);
            this.cbxEmploye8.Name = "cbxEmploye8";
            this.cbxEmploye8.Size = new System.Drawing.Size(460, 28);
            this.cbxEmploye8.Sorted = true;
            this.cbxEmploye8.TabIndex = 32;
            this.cbxEmploye8.SelectedIndexChanged += new System.EventHandler(this.cbxEmploye8_SelectedIndexChanged);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Segoe UI", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.SystemColors.Control;
            this.label25.Location = new System.Drawing.Point(12, 17);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(228, 45);
            this.label25.TabIndex = 41;
            this.label25.Text = "Check Atlanse";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.SystemColors.Control;
            this.label26.Location = new System.Drawing.Point(172, 116);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(77, 21);
            this.label26.TabIndex = 44;
            this.label26.Text = "Employé";
            // 
            // tbxPrixJour1
            // 
            this.tbxPrixJour1.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.tbxPrixJour1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.tbxPrixJour1.ForeColor = System.Drawing.Color.Black;
            this.tbxPrixJour1.Location = new System.Drawing.Point(552, 140);
            this.tbxPrixJour1.Name = "tbxPrixJour1";
            this.tbxPrixJour1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbxPrixJour1.Size = new System.Drawing.Size(119, 26);
            this.tbxPrixJour1.TabIndex = 46;
            this.tbxPrixJour1.Text = "172";
            this.tbxPrixJour1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.tbxPrixJour1.TextChanged += new System.EventHandler(this.tbxPrixJour1_TextChanged);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.SystemColors.Control;
            this.label28.Location = new System.Drawing.Point(590, 106);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(50, 15);
            this.label28.TabIndex = 47;
            this.label28.Text = "Prix par";
            // 
            // tbxPrixJour2
            // 
            this.tbxPrixJour2.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.tbxPrixJour2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.tbxPrixJour2.ForeColor = System.Drawing.Color.Black;
            this.tbxPrixJour2.Location = new System.Drawing.Point(552, 167);
            this.tbxPrixJour2.Name = "tbxPrixJour2";
            this.tbxPrixJour2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbxPrixJour2.Size = new System.Drawing.Size(119, 26);
            this.tbxPrixJour2.TabIndex = 48;
            this.tbxPrixJour2.Text = "172";
            this.tbxPrixJour2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.tbxPrixJour2.TextChanged += new System.EventHandler(this.tbxPrixJour2_TextChanged);
            // 
            // tbxPrixJour3
            // 
            this.tbxPrixJour3.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.tbxPrixJour3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.tbxPrixJour3.ForeColor = System.Drawing.Color.Black;
            this.tbxPrixJour3.Location = new System.Drawing.Point(552, 194);
            this.tbxPrixJour3.Name = "tbxPrixJour3";
            this.tbxPrixJour3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbxPrixJour3.Size = new System.Drawing.Size(119, 26);
            this.tbxPrixJour3.TabIndex = 49;
            this.tbxPrixJour3.Text = "172";
            this.tbxPrixJour3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.tbxPrixJour3.TextChanged += new System.EventHandler(this.tbxPrixJour3_TextChanged);
            // 
            // tbxPrixJour4
            // 
            this.tbxPrixJour4.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.tbxPrixJour4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.tbxPrixJour4.ForeColor = System.Drawing.Color.Black;
            this.tbxPrixJour4.Location = new System.Drawing.Point(552, 221);
            this.tbxPrixJour4.Name = "tbxPrixJour4";
            this.tbxPrixJour4.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbxPrixJour4.Size = new System.Drawing.Size(119, 26);
            this.tbxPrixJour4.TabIndex = 50;
            this.tbxPrixJour4.Text = "172";
            this.tbxPrixJour4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.tbxPrixJour4.TextChanged += new System.EventHandler(this.tbxPrixJour4_TextChanged);
            // 
            // tbxPrixJour5
            // 
            this.tbxPrixJour5.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.tbxPrixJour5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.tbxPrixJour5.ForeColor = System.Drawing.Color.Black;
            this.tbxPrixJour5.Location = new System.Drawing.Point(552, 248);
            this.tbxPrixJour5.Name = "tbxPrixJour5";
            this.tbxPrixJour5.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbxPrixJour5.Size = new System.Drawing.Size(119, 26);
            this.tbxPrixJour5.TabIndex = 51;
            this.tbxPrixJour5.Text = "172";
            this.tbxPrixJour5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.tbxPrixJour5.TextChanged += new System.EventHandler(this.tbxPrixJour5_TextChanged);
            // 
            // tbxPrixJour6
            // 
            this.tbxPrixJour6.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.tbxPrixJour6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.tbxPrixJour6.ForeColor = System.Drawing.Color.Black;
            this.tbxPrixJour6.Location = new System.Drawing.Point(552, 275);
            this.tbxPrixJour6.Name = "tbxPrixJour6";
            this.tbxPrixJour6.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbxPrixJour6.Size = new System.Drawing.Size(119, 26);
            this.tbxPrixJour6.TabIndex = 52;
            this.tbxPrixJour6.Text = "172";
            this.tbxPrixJour6.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.tbxPrixJour6.TextChanged += new System.EventHandler(this.tbxPrixJour6_TextChanged);
            // 
            // tbxPrixJour7
            // 
            this.tbxPrixJour7.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.tbxPrixJour7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.tbxPrixJour7.ForeColor = System.Drawing.Color.Black;
            this.tbxPrixJour7.Location = new System.Drawing.Point(552, 302);
            this.tbxPrixJour7.Name = "tbxPrixJour7";
            this.tbxPrixJour7.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbxPrixJour7.Size = new System.Drawing.Size(119, 26);
            this.tbxPrixJour7.TabIndex = 53;
            this.tbxPrixJour7.Text = "172";
            this.tbxPrixJour7.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.tbxPrixJour7.TextChanged += new System.EventHandler(this.tbxPrixJour7_TextChanged);
            // 
            // tbxPrixJour8
            // 
            this.tbxPrixJour8.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.tbxPrixJour8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.tbxPrixJour8.ForeColor = System.Drawing.Color.Black;
            this.tbxPrixJour8.Location = new System.Drawing.Point(552, 329);
            this.tbxPrixJour8.Name = "tbxPrixJour8";
            this.tbxPrixJour8.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbxPrixJour8.Size = new System.Drawing.Size(119, 26);
            this.tbxPrixJour8.TabIndex = 54;
            this.tbxPrixJour8.Text = "172";
            this.tbxPrixJour8.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.tbxPrixJour8.TextChanged += new System.EventHandler(this.tbxPrixJour8_TextChanged);
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.SystemColors.Control;
            this.label29.Location = new System.Drawing.Point(694, 116);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(50, 21);
            this.label29.TabIndex = 56;
            this.label29.Text = "Jours";
            // 
            // tbxNbrJours1
            // 
            this.tbxNbrJours1.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.tbxNbrJours1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxNbrJours1.ForeColor = System.Drawing.Color.DarkGreen;
            this.tbxNbrJours1.Location = new System.Drawing.Point(671, 140);
            this.tbxNbrJours1.Name = "tbxNbrJours1";
            this.tbxNbrJours1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbxNbrJours1.Size = new System.Drawing.Size(104, 26);
            this.tbxNbrJours1.TabIndex = 59;
            this.tbxNbrJours1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tbxNbrJours2
            // 
            this.tbxNbrJours2.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.tbxNbrJours2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxNbrJours2.ForeColor = System.Drawing.Color.DarkGreen;
            this.tbxNbrJours2.Location = new System.Drawing.Point(671, 167);
            this.tbxNbrJours2.Name = "tbxNbrJours2";
            this.tbxNbrJours2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbxNbrJours2.Size = new System.Drawing.Size(104, 26);
            this.tbxNbrJours2.TabIndex = 60;
            this.tbxNbrJours2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tbxNbrJours4
            // 
            this.tbxNbrJours4.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.tbxNbrJours4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxNbrJours4.ForeColor = System.Drawing.Color.DarkGreen;
            this.tbxNbrJours4.Location = new System.Drawing.Point(671, 221);
            this.tbxNbrJours4.Name = "tbxNbrJours4";
            this.tbxNbrJours4.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbxNbrJours4.Size = new System.Drawing.Size(104, 26);
            this.tbxNbrJours4.TabIndex = 61;
            this.tbxNbrJours4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tbxNbrJours3
            // 
            this.tbxNbrJours3.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.tbxNbrJours3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxNbrJours3.ForeColor = System.Drawing.Color.DarkGreen;
            this.tbxNbrJours3.Location = new System.Drawing.Point(671, 194);
            this.tbxNbrJours3.Name = "tbxNbrJours3";
            this.tbxNbrJours3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbxNbrJours3.Size = new System.Drawing.Size(104, 26);
            this.tbxNbrJours3.TabIndex = 61;
            this.tbxNbrJours3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tbxNbrJours5
            // 
            this.tbxNbrJours5.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.tbxNbrJours5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxNbrJours5.ForeColor = System.Drawing.Color.DarkGreen;
            this.tbxNbrJours5.Location = new System.Drawing.Point(671, 248);
            this.tbxNbrJours5.Name = "tbxNbrJours5";
            this.tbxNbrJours5.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbxNbrJours5.Size = new System.Drawing.Size(104, 26);
            this.tbxNbrJours5.TabIndex = 62;
            this.tbxNbrJours5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tbxNbrJours6
            // 
            this.tbxNbrJours6.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.tbxNbrJours6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxNbrJours6.ForeColor = System.Drawing.Color.DarkGreen;
            this.tbxNbrJours6.Location = new System.Drawing.Point(671, 275);
            this.tbxNbrJours6.Name = "tbxNbrJours6";
            this.tbxNbrJours6.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbxNbrJours6.Size = new System.Drawing.Size(104, 26);
            this.tbxNbrJours6.TabIndex = 63;
            this.tbxNbrJours6.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tbxNbrJours7
            // 
            this.tbxNbrJours7.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.tbxNbrJours7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxNbrJours7.ForeColor = System.Drawing.Color.DarkGreen;
            this.tbxNbrJours7.Location = new System.Drawing.Point(671, 302);
            this.tbxNbrJours7.Name = "tbxNbrJours7";
            this.tbxNbrJours7.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbxNbrJours7.Size = new System.Drawing.Size(104, 26);
            this.tbxNbrJours7.TabIndex = 64;
            this.tbxNbrJours7.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tbxNbrJours8
            // 
            this.tbxNbrJours8.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.tbxNbrJours8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxNbrJours8.ForeColor = System.Drawing.Color.DarkGreen;
            this.tbxNbrJours8.Location = new System.Drawing.Point(671, 329);
            this.tbxNbrJours8.Name = "tbxNbrJours8";
            this.tbxNbrJours8.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbxNbrJours8.Size = new System.Drawing.Size(104, 26);
            this.tbxNbrJours8.TabIndex = 65;
            this.tbxNbrJours8.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.ForeColor = System.Drawing.SystemColors.Control;
            this.label30.Location = new System.Drawing.Point(785, 115);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(101, 21);
            this.label30.TabIndex = 67;
            this.label30.Text = "Revenue (€)";
            // 
            // tbxRevenue3
            // 
            this.tbxRevenue3.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.tbxRevenue3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxRevenue3.ForeColor = System.Drawing.Color.DarkGreen;
            this.tbxRevenue3.Location = new System.Drawing.Point(775, 194);
            this.tbxRevenue3.Name = "tbxRevenue3";
            this.tbxRevenue3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbxRevenue3.Size = new System.Drawing.Size(137, 26);
            this.tbxRevenue3.TabIndex = 68;
            this.tbxRevenue3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tbxRevenue2
            // 
            this.tbxRevenue2.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.tbxRevenue2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxRevenue2.ForeColor = System.Drawing.Color.DarkGreen;
            this.tbxRevenue2.Location = new System.Drawing.Point(775, 167);
            this.tbxRevenue2.Name = "tbxRevenue2";
            this.tbxRevenue2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbxRevenue2.Size = new System.Drawing.Size(137, 26);
            this.tbxRevenue2.TabIndex = 68;
            this.tbxRevenue2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tbxRevenue4
            // 
            this.tbxRevenue4.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.tbxRevenue4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxRevenue4.ForeColor = System.Drawing.Color.DarkGreen;
            this.tbxRevenue4.Location = new System.Drawing.Point(775, 221);
            this.tbxRevenue4.Name = "tbxRevenue4";
            this.tbxRevenue4.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbxRevenue4.Size = new System.Drawing.Size(137, 26);
            this.tbxRevenue4.TabIndex = 69;
            this.tbxRevenue4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tbxRevenue5
            // 
            this.tbxRevenue5.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.tbxRevenue5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxRevenue5.ForeColor = System.Drawing.Color.DarkGreen;
            this.tbxRevenue5.Location = new System.Drawing.Point(775, 248);
            this.tbxRevenue5.Name = "tbxRevenue5";
            this.tbxRevenue5.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbxRevenue5.Size = new System.Drawing.Size(137, 26);
            this.tbxRevenue5.TabIndex = 70;
            this.tbxRevenue5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tbxRevenue6
            // 
            this.tbxRevenue6.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.tbxRevenue6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxRevenue6.ForeColor = System.Drawing.Color.DarkGreen;
            this.tbxRevenue6.Location = new System.Drawing.Point(775, 275);
            this.tbxRevenue6.Name = "tbxRevenue6";
            this.tbxRevenue6.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbxRevenue6.Size = new System.Drawing.Size(137, 26);
            this.tbxRevenue6.TabIndex = 71;
            this.tbxRevenue6.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tbxRevenue7
            // 
            this.tbxRevenue7.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.tbxRevenue7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxRevenue7.ForeColor = System.Drawing.Color.DarkGreen;
            this.tbxRevenue7.Location = new System.Drawing.Point(775, 302);
            this.tbxRevenue7.Name = "tbxRevenue7";
            this.tbxRevenue7.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbxRevenue7.Size = new System.Drawing.Size(137, 26);
            this.tbxRevenue7.TabIndex = 72;
            this.tbxRevenue7.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tbxRevenue8
            // 
            this.tbxRevenue8.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.tbxRevenue8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxRevenue8.ForeColor = System.Drawing.Color.DarkGreen;
            this.tbxRevenue8.Location = new System.Drawing.Point(775, 329);
            this.tbxRevenue8.Name = "tbxRevenue8";
            this.tbxRevenue8.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbxRevenue8.Size = new System.Drawing.Size(137, 26);
            this.tbxRevenue8.TabIndex = 73;
            this.tbxRevenue8.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblTotal
            // 
            this.lblTotal.AutoSize = true;
            this.lblTotal.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotal.ForeColor = System.Drawing.SystemColors.Control;
            this.lblTotal.Location = new System.Drawing.Point(772, 358);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(140, 21);
            this.lblTotal.TabIndex = 83;
            this.lblTotal.Text = "Total : 00 000.00€";
            // 
            // lbxMois2
            // 
            this.lbxMois2.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.lbxMois2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbxMois2.FormattingEnabled = true;
            this.lbxMois2.Location = new System.Drawing.Point(651, 63);
            this.lbxMois2.Name = "lbxMois2";
            this.lbxMois2.Size = new System.Drawing.Size(261, 28);
            this.lbxMois2.TabIndex = 44;
            // 
            // btnGenererAtlanse
            // 
            this.btnGenererAtlanse.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.btnGenererAtlanse.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGenererAtlanse.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGenererAtlanse.Location = new System.Drawing.Point(737, 386);
            this.btnGenererAtlanse.Name = "btnGenererAtlanse";
            this.btnGenererAtlanse.Size = new System.Drawing.Size(175, 35);
            this.btnGenererAtlanse.TabIndex = 44;
            this.btnGenererAtlanse.Text = "Générer";
            this.btnGenererAtlanse.UseVisualStyleBackColor = false;
            this.btnGenererAtlanse.Click += new System.EventHandler(this.btnGenererAtlanse_Click);
            // 
            // menu6
            // 
            this.menu6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.menu6.Controls.Add(this.tbxRevenue1);
            this.menu6.Controls.Add(this.lblNomPrenomATL);
            this.menu6.Controls.Add(this.label31);
            this.menu6.Controls.Add(this.lbxATLEmployeInformation);
            this.menu6.Controls.Add(this.btnATLpreview8);
            this.menu6.Controls.Add(this.btnATLpreview7);
            this.menu6.Controls.Add(this.btnATLpreview6);
            this.menu6.Controls.Add(this.btnATLpreview5);
            this.menu6.Controls.Add(this.btnATLpreview4);
            this.menu6.Controls.Add(this.btnATLpreview3);
            this.menu6.Controls.Add(this.btnATLpreview2);
            this.menu6.Controls.Add(this.btnATLpreview1);
            this.menu6.Controls.Add(this.btnGenererAtlanse);
            this.menu6.Controls.Add(this.lbxMois2);
            this.menu6.Controls.Add(this.lblTotal);
            this.menu6.Controls.Add(this.tbxRevenue8);
            this.menu6.Controls.Add(this.tbxRevenue7);
            this.menu6.Controls.Add(this.tbxRevenue6);
            this.menu6.Controls.Add(this.tbxRevenue5);
            this.menu6.Controls.Add(this.tbxRevenue4);
            this.menu6.Controls.Add(this.tbxRevenue2);
            this.menu6.Controls.Add(this.tbxRevenue3);
            this.menu6.Controls.Add(this.label30);
            this.menu6.Controls.Add(this.tbxNbrJours8);
            this.menu6.Controls.Add(this.tbxNbrJours7);
            this.menu6.Controls.Add(this.tbxNbrJours6);
            this.menu6.Controls.Add(this.tbxNbrJours5);
            this.menu6.Controls.Add(this.tbxNbrJours3);
            this.menu6.Controls.Add(this.tbxNbrJours4);
            this.menu6.Controls.Add(this.tbxNbrJours2);
            this.menu6.Controls.Add(this.tbxNbrJours1);
            this.menu6.Controls.Add(this.label29);
            this.menu6.Controls.Add(this.tbxPrixJour8);
            this.menu6.Controls.Add(this.tbxPrixJour7);
            this.menu6.Controls.Add(this.tbxPrixJour6);
            this.menu6.Controls.Add(this.tbxPrixJour5);
            this.menu6.Controls.Add(this.tbxPrixJour4);
            this.menu6.Controls.Add(this.tbxPrixJour3);
            this.menu6.Controls.Add(this.tbxPrixJour2);
            this.menu6.Controls.Add(this.label28);
            this.menu6.Controls.Add(this.tbxPrixJour1);
            this.menu6.Controls.Add(this.label26);
            this.menu6.Controls.Add(this.label25);
            this.menu6.Controls.Add(this.cbxEmploye8);
            this.menu6.Controls.Add(this.cbxEmploye7);
            this.menu6.Controls.Add(this.cbxEmploye6);
            this.menu6.Controls.Add(this.cbxEmploye5);
            this.menu6.Controls.Add(this.cbxEmploye4);
            this.menu6.Controls.Add(this.cbxEmploye3);
            this.menu6.Controls.Add(this.cbxEmploye2);
            this.menu6.Controls.Add(this.cbxEmploye1);
            this.menu6.ForeColor = System.Drawing.Color.Black;
            this.menu6.Location = new System.Drawing.Point(1259, 33);
            this.menu6.Name = "menu6";
            this.menu6.Size = new System.Drawing.Size(1020, 1042);
            this.menu6.TabIndex = 42;
            this.menu6.TabStop = false;
            this.menu6.Visible = false;
            // 
            // tbxRevenue1
            // 
            this.tbxRevenue1.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.tbxRevenue1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxRevenue1.ForeColor = System.Drawing.Color.DarkGreen;
            this.tbxRevenue1.Location = new System.Drawing.Point(775, 140);
            this.tbxRevenue1.Name = "tbxRevenue1";
            this.tbxRevenue1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbxRevenue1.Size = new System.Drawing.Size(137, 26);
            this.tbxRevenue1.TabIndex = 96;
            this.tbxRevenue1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblNomPrenomATL
            // 
            this.lblNomPrenomATL.AutoSize = true;
            this.lblNomPrenomATL.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNomPrenomATL.ForeColor = System.Drawing.SystemColors.Control;
            this.lblNomPrenomATL.Location = new System.Drawing.Point(60, 408);
            this.lblNomPrenomATL.Name = "lblNomPrenomATL";
            this.lblNomPrenomATL.Size = new System.Drawing.Size(143, 21);
            this.lblNomPrenomATL.TabIndex = 95;
            this.lblNomPrenomATL.Text = "{Prénom et Nom}";
            this.lblNomPrenomATL.Visible = false;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.ForeColor = System.Drawing.SystemColors.Control;
            this.label31.Location = new System.Drawing.Point(593, 123);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(47, 15);
            this.label31.TabIndex = 94;
            this.label31.Text = "jour (€)";
            // 
            // lbxATLEmployeInformation
            // 
            this.lbxATLEmployeInformation.Alignment = System.Windows.Forms.ListViewAlignment.Left;
            this.lbxATLEmployeInformation.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.lbxATLEmployeInformation.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colDate,
            this.colProjet,
            this.colHeure,
            this.colTypeHeure,
            this.colCommentaire});
            this.lbxATLEmployeInformation.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbxATLEmployeInformation.FullRowSelect = true;
            this.lbxATLEmployeInformation.GridLines = true;
            this.lbxATLEmployeInformation.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.lbxATLEmployeInformation.HideSelection = false;
            this.lbxATLEmployeInformation.Location = new System.Drawing.Point(66, 432);
            this.lbxATLEmployeInformation.Name = "lbxATLEmployeInformation";
            this.lbxATLEmployeInformation.Size = new System.Drawing.Size(846, 523);
            this.lbxATLEmployeInformation.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.lbxATLEmployeInformation.TabIndex = 86;
            this.lbxATLEmployeInformation.UseCompatibleStateImageBehavior = false;
            this.lbxATLEmployeInformation.View = System.Windows.Forms.View.Details;
            this.lbxATLEmployeInformation.Visible = false;
            // 
            // colDate
            // 
            this.colDate.Width = 75;
            // 
            // colProjet
            // 
            this.colProjet.Width = 300;
            // 
            // colCommentaire
            // 
            this.colCommentaire.Width = 1000;
            // 
            // btnATLpreview8
            // 
            this.btnATLpreview8.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.btnATLpreview8.Enabled = false;
            this.btnATLpreview8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnATLpreview8.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnATLpreview8.Image = ((System.Drawing.Image)(resources.GetObject("btnATLpreview8.Image")));
            this.btnATLpreview8.Location = new System.Drawing.Point(66, 328);
            this.btnATLpreview8.Name = "btnATLpreview8";
            this.btnATLpreview8.Size = new System.Drawing.Size(25, 28);
            this.btnATLpreview8.TabIndex = 93;
            this.btnATLpreview8.UseVisualStyleBackColor = false;
            this.btnATLpreview8.Click += new System.EventHandler(this.btnATLpreview8_Click);
            // 
            // btnATLpreview7
            // 
            this.btnATLpreview7.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.btnATLpreview7.Enabled = false;
            this.btnATLpreview7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnATLpreview7.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnATLpreview7.Image = ((System.Drawing.Image)(resources.GetObject("btnATLpreview7.Image")));
            this.btnATLpreview7.Location = new System.Drawing.Point(66, 301);
            this.btnATLpreview7.Name = "btnATLpreview7";
            this.btnATLpreview7.Size = new System.Drawing.Size(25, 28);
            this.btnATLpreview7.TabIndex = 92;
            this.btnATLpreview7.UseVisualStyleBackColor = false;
            this.btnATLpreview7.Click += new System.EventHandler(this.btnATLpreview7_Click);
            // 
            // btnATLpreview6
            // 
            this.btnATLpreview6.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.btnATLpreview6.Enabled = false;
            this.btnATLpreview6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnATLpreview6.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnATLpreview6.Image = ((System.Drawing.Image)(resources.GetObject("btnATLpreview6.Image")));
            this.btnATLpreview6.Location = new System.Drawing.Point(66, 274);
            this.btnATLpreview6.Name = "btnATLpreview6";
            this.btnATLpreview6.Size = new System.Drawing.Size(25, 28);
            this.btnATLpreview6.TabIndex = 91;
            this.btnATLpreview6.UseVisualStyleBackColor = false;
            this.btnATLpreview6.Click += new System.EventHandler(this.btnATLpreview6_Click);
            // 
            // btnATLpreview5
            // 
            this.btnATLpreview5.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.btnATLpreview5.Enabled = false;
            this.btnATLpreview5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnATLpreview5.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnATLpreview5.Image = ((System.Drawing.Image)(resources.GetObject("btnATLpreview5.Image")));
            this.btnATLpreview5.Location = new System.Drawing.Point(66, 247);
            this.btnATLpreview5.Name = "btnATLpreview5";
            this.btnATLpreview5.Size = new System.Drawing.Size(25, 28);
            this.btnATLpreview5.TabIndex = 90;
            this.btnATLpreview5.UseVisualStyleBackColor = false;
            this.btnATLpreview5.Click += new System.EventHandler(this.btnATLpreview5_Click);
            // 
            // btnATLpreview4
            // 
            this.btnATLpreview4.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.btnATLpreview4.Enabled = false;
            this.btnATLpreview4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnATLpreview4.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnATLpreview4.Image = ((System.Drawing.Image)(resources.GetObject("btnATLpreview4.Image")));
            this.btnATLpreview4.Location = new System.Drawing.Point(66, 220);
            this.btnATLpreview4.Name = "btnATLpreview4";
            this.btnATLpreview4.Size = new System.Drawing.Size(25, 28);
            this.btnATLpreview4.TabIndex = 89;
            this.btnATLpreview4.UseVisualStyleBackColor = false;
            this.btnATLpreview4.Click += new System.EventHandler(this.btnATLpreview4_Click);
            // 
            // btnATLpreview3
            // 
            this.btnATLpreview3.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.btnATLpreview3.Enabled = false;
            this.btnATLpreview3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnATLpreview3.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnATLpreview3.Image = ((System.Drawing.Image)(resources.GetObject("btnATLpreview3.Image")));
            this.btnATLpreview3.Location = new System.Drawing.Point(66, 193);
            this.btnATLpreview3.Name = "btnATLpreview3";
            this.btnATLpreview3.Size = new System.Drawing.Size(25, 28);
            this.btnATLpreview3.TabIndex = 88;
            this.btnATLpreview3.UseVisualStyleBackColor = false;
            this.btnATLpreview3.Click += new System.EventHandler(this.btnATLpreview3_Click);
            // 
            // btnATLpreview2
            // 
            this.btnATLpreview2.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.btnATLpreview2.Enabled = false;
            this.btnATLpreview2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnATLpreview2.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnATLpreview2.Image = ((System.Drawing.Image)(resources.GetObject("btnATLpreview2.Image")));
            this.btnATLpreview2.Location = new System.Drawing.Point(66, 166);
            this.btnATLpreview2.Name = "btnATLpreview2";
            this.btnATLpreview2.Size = new System.Drawing.Size(25, 28);
            this.btnATLpreview2.TabIndex = 87;
            this.btnATLpreview2.UseVisualStyleBackColor = false;
            this.btnATLpreview2.Click += new System.EventHandler(this.btnATLpreview2_Click);
            // 
            // btnATLpreview1
            // 
            this.btnATLpreview1.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.btnATLpreview1.Enabled = false;
            this.btnATLpreview1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnATLpreview1.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnATLpreview1.Image = ((System.Drawing.Image)(resources.GetObject("btnATLpreview1.Image")));
            this.btnATLpreview1.Location = new System.Drawing.Point(66, 139);
            this.btnATLpreview1.Name = "btnATLpreview1";
            this.btnATLpreview1.Size = new System.Drawing.Size(25, 28);
            this.btnATLpreview1.TabIndex = 84;
            this.btnATLpreview1.UseVisualStyleBackColor = false;
            this.btnATLpreview1.Click += new System.EventHandler(this.btnATLpreview1_Click);
            // 
            // menu7
            // 
            this.menu7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.menu7.Controls.Add(this.groupBox3);
            this.menu7.Controls.Add(this.groupBox4);
            this.menu7.Controls.Add(this.groupBox2);
            this.menu7.Controls.Add(this.groupBox1);
            this.menu7.Controls.Add(this.label27);
            this.menu7.Location = new System.Drawing.Point(1259, 33);
            this.menu7.Name = "menu7";
            this.menu7.Size = new System.Drawing.Size(1020, 1042);
            this.menu7.TabIndex = 56;
            this.menu7.TabStop = false;
            this.menu7.Visible = false;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label38);
            this.groupBox3.Controls.Add(this.tbxAdminMAIL_MailDenvoi);
            this.groupBox3.Controls.Add(this.tbxAdminMAIL_Port);
            this.groupBox3.Controls.Add(this.label37);
            this.groupBox3.Controls.Add(this.btnSaveMAIL);
            this.groupBox3.Controls.Add(this.tbxAdminMAIL_SMTP);
            this.groupBox3.Controls.Add(this.label36);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.groupBox3.ForeColor = System.Drawing.SystemColors.Control;
            this.groupBox3.Location = new System.Drawing.Point(20, 434);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(732, 194);
            this.groupBox3.TabIndex = 63;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Mails";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.ForeColor = System.Drawing.SystemColors.Control;
            this.label38.Location = new System.Drawing.Point(185, 80);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(153, 21);
            this.label38.TabIndex = 66;
            this.label38.Text = "Mail d\'envoi (virtuel)";
            // 
            // tbxAdminMAIL_MailDenvoi
            // 
            this.tbxAdminMAIL_MailDenvoi.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxAdminMAIL_MailDenvoi.Location = new System.Drawing.Point(187, 104);
            this.tbxAdminMAIL_MailDenvoi.Name = "tbxAdminMAIL_MailDenvoi";
            this.tbxAdminMAIL_MailDenvoi.Size = new System.Drawing.Size(312, 26);
            this.tbxAdminMAIL_MailDenvoi.TabIndex = 65;
            this.tbxAdminMAIL_MailDenvoi.TextChanged += new System.EventHandler(this.tbxAdminMAIL_MailDenvoi_TextChanged);
            // 
            // tbxAdminMAIL_Port
            // 
            this.tbxAdminMAIL_Port.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxAdminMAIL_Port.Location = new System.Drawing.Point(527, 104);
            this.tbxAdminMAIL_Port.Name = "tbxAdminMAIL_Port";
            this.tbxAdminMAIL_Port.Size = new System.Drawing.Size(63, 26);
            this.tbxAdminMAIL_Port.TabIndex = 64;
            this.tbxAdminMAIL_Port.TextChanged += new System.EventHandler(this.tbxAdminMAIL_Port_TextChanged);
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.ForeColor = System.Drawing.SystemColors.Control;
            this.label37.Location = new System.Drawing.Point(523, 80);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(38, 21);
            this.label37.TabIndex = 63;
            this.label37.Text = "Port";
            // 
            // btnSaveMAIL
            // 
            this.btnSaveMAIL.BackColor = System.Drawing.Color.LightGray;
            this.btnSaveMAIL.Enabled = false;
            this.btnSaveMAIL.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSaveMAIL.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSaveMAIL.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnSaveMAIL.Location = new System.Drawing.Point(-1, 156);
            this.btnSaveMAIL.Name = "btnSaveMAIL";
            this.btnSaveMAIL.Size = new System.Drawing.Size(734, 38);
            this.btnSaveMAIL.TabIndex = 62;
            this.btnSaveMAIL.Text = "Enregistrer";
            this.btnSaveMAIL.UseVisualStyleBackColor = false;
            this.btnSaveMAIL.Click += new System.EventHandler(this.btnSaveMAIL_Click);
            // 
            // tbxAdminMAIL_SMTP
            // 
            this.tbxAdminMAIL_SMTP.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxAdminMAIL_SMTP.Location = new System.Drawing.Point(187, 48);
            this.tbxAdminMAIL_SMTP.Name = "tbxAdminMAIL_SMTP";
            this.tbxAdminMAIL_SMTP.Size = new System.Drawing.Size(403, 26);
            this.tbxAdminMAIL_SMTP.TabIndex = 42;
            this.tbxAdminMAIL_SMTP.TextChanged += new System.EventHandler(this.tbxAdminMAIL_SMTP_TextChanged);
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.ForeColor = System.Drawing.SystemColors.Control;
            this.label36.Location = new System.Drawing.Point(183, 24);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(50, 21);
            this.label36.TabIndex = 41;
            this.label36.Text = "SMTP";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.btnVoirMDP);
            this.groupBox4.Controls.Add(this.label39);
            this.groupBox4.Controls.Add(this.tbxAdminAPI_user);
            this.groupBox4.Controls.Add(this.btnSaveAPI);
            this.groupBox4.Controls.Add(this.tbxAdminAPI_password);
            this.groupBox4.Controls.Add(this.label41);
            this.groupBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.groupBox4.ForeColor = System.Drawing.SystemColors.Control;
            this.groupBox4.Location = new System.Drawing.Point(20, 641);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(732, 194);
            this.groupBox4.TabIndex = 67;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "API";
            // 
            // btnVoirMDP
            // 
            this.btnVoirMDP.BackColor = System.Drawing.Color.LightGray;
            this.btnVoirMDP.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnVoirMDP.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnVoirMDP.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnVoirMDP.Image = ((System.Drawing.Image)(resources.GetObject("btnVoirMDP.Image")));
            this.btnVoirMDP.Location = new System.Drawing.Point(509, 107);
            this.btnVoirMDP.Name = "btnVoirMDP";
            this.btnVoirMDP.Size = new System.Drawing.Size(30, 26);
            this.btnVoirMDP.TabIndex = 67;
            this.btnVoirMDP.UseVisualStyleBackColor = false;
            this.btnVoirMDP.Click += new System.EventHandler(this.btnVoirMDP_Click);
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.ForeColor = System.Drawing.SystemColors.Control;
            this.label39.Location = new System.Drawing.Point(225, 28);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(81, 21);
            this.label39.TabIndex = 66;
            this.label39.Text = "Utilisateur";
            // 
            // tbxAdminAPI_user
            // 
            this.tbxAdminAPI_user.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxAdminAPI_user.Location = new System.Drawing.Point(227, 52);
            this.tbxAdminAPI_user.Name = "tbxAdminAPI_user";
            this.tbxAdminAPI_user.Size = new System.Drawing.Size(278, 26);
            this.tbxAdminAPI_user.TabIndex = 65;
            this.tbxAdminAPI_user.TextChanged += new System.EventHandler(this.tbxAdminAPI_user_TextChanged);
            // 
            // btnSaveAPI
            // 
            this.btnSaveAPI.BackColor = System.Drawing.Color.LightGray;
            this.btnSaveAPI.Enabled = false;
            this.btnSaveAPI.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSaveAPI.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold);
            this.btnSaveAPI.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnSaveAPI.Location = new System.Drawing.Point(-1, 157);
            this.btnSaveAPI.Name = "btnSaveAPI";
            this.btnSaveAPI.Size = new System.Drawing.Size(734, 38);
            this.btnSaveAPI.TabIndex = 62;
            this.btnSaveAPI.Text = "Enregistrer";
            this.btnSaveAPI.UseVisualStyleBackColor = false;
            this.btnSaveAPI.Click += new System.EventHandler(this.btnSaveAPI_Click);
            // 
            // tbxAdminAPI_password
            // 
            this.tbxAdminAPI_password.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxAdminAPI_password.Location = new System.Drawing.Point(227, 107);
            this.tbxAdminAPI_password.Name = "tbxAdminAPI_password";
            this.tbxAdminAPI_password.PasswordChar = '*';
            this.tbxAdminAPI_password.Size = new System.Drawing.Size(278, 26);
            this.tbxAdminAPI_password.TabIndex = 42;
            this.tbxAdminAPI_password.TextChanged += new System.EventHandler(this.tbxAdminAPI_password_TextChanged);
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.ForeColor = System.Drawing.SystemColors.Control;
            this.label41.Location = new System.Drawing.Point(223, 83);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(102, 21);
            this.label41.TabIndex = 41;
            this.label41.Text = "Mot de passe";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnSaveDIVERS);
            this.groupBox2.Controls.Add(this.tbxAdminDIVERS_backupname);
            this.groupBox2.Controls.Add(this.label35);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.groupBox2.ForeColor = System.Drawing.SystemColors.Control;
            this.groupBox2.Location = new System.Drawing.Point(20, 847);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(732, 154);
            this.groupBox2.TabIndex = 40;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Divers";
            // 
            // btnSaveDIVERS
            // 
            this.btnSaveDIVERS.BackColor = System.Drawing.Color.LightGray;
            this.btnSaveDIVERS.Enabled = false;
            this.btnSaveDIVERS.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSaveDIVERS.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold);
            this.btnSaveDIVERS.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnSaveDIVERS.Location = new System.Drawing.Point(-1, 116);
            this.btnSaveDIVERS.Name = "btnSaveDIVERS";
            this.btnSaveDIVERS.Size = new System.Drawing.Size(734, 38);
            this.btnSaveDIVERS.TabIndex = 62;
            this.btnSaveDIVERS.Text = "Enregistrer";
            this.btnSaveDIVERS.UseVisualStyleBackColor = false;
            this.btnSaveDIVERS.Click += new System.EventHandler(this.btnSaveDIVERS_Click);
            // 
            // tbxAdminDIVERS_backupname
            // 
            this.tbxAdminDIVERS_backupname.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxAdminDIVERS_backupname.Location = new System.Drawing.Point(265, 61);
            this.tbxAdminDIVERS_backupname.Name = "tbxAdminDIVERS_backupname";
            this.tbxAdminDIVERS_backupname.Size = new System.Drawing.Size(197, 26);
            this.tbxAdminDIVERS_backupname.TabIndex = 42;
            this.tbxAdminDIVERS_backupname.TextChanged += new System.EventHandler(this.tbxAdminDIVERS_backupname_TextChanged);
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.ForeColor = System.Drawing.SystemColors.Control;
            this.label35.Location = new System.Drawing.Point(261, 37);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(136, 21);
            this.label35.TabIndex = 41;
            this.label35.Text = "Nom de la backup";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnSaveHTML);
            this.groupBox1.Controls.Add(this.tbxAdminHTML_logoWidth);
            this.groupBox1.Controls.Add(this.tbxAdminHTML_colonne5largeur);
            this.groupBox1.Controls.Add(this.tbxAdminHTML_colonne5nom);
            this.groupBox1.Controls.Add(this.tbxAdminHTML_colonne4largeur);
            this.groupBox1.Controls.Add(this.tbxAdminHTML_colonne4nom);
            this.groupBox1.Controls.Add(this.tbxAdminHTML_colonne3largeur);
            this.groupBox1.Controls.Add(this.tbxAdminHTML_colonne3nom);
            this.groupBox1.Controls.Add(this.tbxAdminHTML_colonne2largeur);
            this.groupBox1.Controls.Add(this.tbxAdminHTML_colonne2nom);
            this.groupBox1.Controls.Add(this.tbxAdminHTML_colonne1largeur);
            this.groupBox1.Controls.Add(this.tbxAdminHTML_colonne1nom);
            this.groupBox1.Controls.Add(this.label34);
            this.groupBox1.Controls.Add(this.label33);
            this.groupBox1.Controls.Add(this.tbxAdminHTML_CSS);
            this.groupBox1.Controls.Add(this.tbxAdminHTML_logo);
            this.groupBox1.Controls.Add(this.label32);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.SystemColors.Control;
            this.groupBox1.Location = new System.Drawing.Point(20, 85);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(732, 336);
            this.groupBox1.TabIndex = 39;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "HTML";
            // 
            // btnSaveHTML
            // 
            this.btnSaveHTML.BackColor = System.Drawing.Color.LightGray;
            this.btnSaveHTML.Enabled = false;
            this.btnSaveHTML.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSaveHTML.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold);
            this.btnSaveHTML.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnSaveHTML.Location = new System.Drawing.Point(-1, 298);
            this.btnSaveHTML.Name = "btnSaveHTML";
            this.btnSaveHTML.Size = new System.Drawing.Size(734, 38);
            this.btnSaveHTML.TabIndex = 61;
            this.btnSaveHTML.Text = "Enregistrer";
            this.btnSaveHTML.UseVisualStyleBackColor = false;
            this.btnSaveHTML.Click += new System.EventHandler(this.btnSaveHTML_Click);
            // 
            // tbxAdminHTML_logoWidth
            // 
            this.tbxAdminHTML_logoWidth.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxAdminHTML_logoWidth.Location = new System.Drawing.Point(174, 58);
            this.tbxAdminHTML_logoWidth.Name = "tbxAdminHTML_logoWidth";
            this.tbxAdminHTML_logoWidth.Size = new System.Drawing.Size(48, 26);
            this.tbxAdminHTML_logoWidth.TabIndex = 60;
            this.tbxAdminHTML_logoWidth.TextChanged += new System.EventHandler(this.tbxAdminHTML_logoWidth_TextChanged);
            // 
            // tbxAdminHTML_colonne5largeur
            // 
            this.tbxAdminHTML_colonne5largeur.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxAdminHTML_colonne5largeur.Location = new System.Drawing.Point(174, 248);
            this.tbxAdminHTML_colonne5largeur.Name = "tbxAdminHTML_colonne5largeur";
            this.tbxAdminHTML_colonne5largeur.Size = new System.Drawing.Size(47, 26);
            this.tbxAdminHTML_colonne5largeur.TabIndex = 58;
            this.tbxAdminHTML_colonne5largeur.TextChanged += new System.EventHandler(this.tbxAdminHTML_colonne5largeur_TextChanged);
            // 
            // tbxAdminHTML_colonne5nom
            // 
            this.tbxAdminHTML_colonne5nom.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxAdminHTML_colonne5nom.Location = new System.Drawing.Point(24, 248);
            this.tbxAdminHTML_colonne5nom.Name = "tbxAdminHTML_colonne5nom";
            this.tbxAdminHTML_colonne5nom.Size = new System.Drawing.Size(144, 26);
            this.tbxAdminHTML_colonne5nom.TabIndex = 57;
            this.tbxAdminHTML_colonne5nom.TextChanged += new System.EventHandler(this.tbxAdminHTML_colonne5nom_TextChanged);
            // 
            // tbxAdminHTML_colonne4largeur
            // 
            this.tbxAdminHTML_colonne4largeur.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxAdminHTML_colonne4largeur.Location = new System.Drawing.Point(174, 215);
            this.tbxAdminHTML_colonne4largeur.Name = "tbxAdminHTML_colonne4largeur";
            this.tbxAdminHTML_colonne4largeur.Size = new System.Drawing.Size(47, 26);
            this.tbxAdminHTML_colonne4largeur.TabIndex = 55;
            this.tbxAdminHTML_colonne4largeur.TextChanged += new System.EventHandler(this.tbxAdminHTML_colonne4largeur_TextChanged);
            // 
            // tbxAdminHTML_colonne4nom
            // 
            this.tbxAdminHTML_colonne4nom.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxAdminHTML_colonne4nom.Location = new System.Drawing.Point(24, 215);
            this.tbxAdminHTML_colonne4nom.Name = "tbxAdminHTML_colonne4nom";
            this.tbxAdminHTML_colonne4nom.Size = new System.Drawing.Size(144, 26);
            this.tbxAdminHTML_colonne4nom.TabIndex = 54;
            this.tbxAdminHTML_colonne4nom.TextChanged += new System.EventHandler(this.tbxAdminHTML_colonne4nom_TextChanged);
            // 
            // tbxAdminHTML_colonne3largeur
            // 
            this.tbxAdminHTML_colonne3largeur.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxAdminHTML_colonne3largeur.Location = new System.Drawing.Point(174, 183);
            this.tbxAdminHTML_colonne3largeur.Name = "tbxAdminHTML_colonne3largeur";
            this.tbxAdminHTML_colonne3largeur.Size = new System.Drawing.Size(47, 26);
            this.tbxAdminHTML_colonne3largeur.TabIndex = 52;
            this.tbxAdminHTML_colonne3largeur.TextChanged += new System.EventHandler(this.tbxAdminHTML_colonne3largeur_TextChanged);
            // 
            // tbxAdminHTML_colonne3nom
            // 
            this.tbxAdminHTML_colonne3nom.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxAdminHTML_colonne3nom.Location = new System.Drawing.Point(24, 183);
            this.tbxAdminHTML_colonne3nom.Name = "tbxAdminHTML_colonne3nom";
            this.tbxAdminHTML_colonne3nom.Size = new System.Drawing.Size(144, 26);
            this.tbxAdminHTML_colonne3nom.TabIndex = 51;
            this.tbxAdminHTML_colonne3nom.TextChanged += new System.EventHandler(this.tbxAdminHTML_colonne3nom_TextChanged);
            // 
            // tbxAdminHTML_colonne2largeur
            // 
            this.tbxAdminHTML_colonne2largeur.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxAdminHTML_colonne2largeur.Location = new System.Drawing.Point(174, 151);
            this.tbxAdminHTML_colonne2largeur.Name = "tbxAdminHTML_colonne2largeur";
            this.tbxAdminHTML_colonne2largeur.Size = new System.Drawing.Size(47, 26);
            this.tbxAdminHTML_colonne2largeur.TabIndex = 49;
            this.tbxAdminHTML_colonne2largeur.TextChanged += new System.EventHandler(this.tbxAdminHTML_colonne2largeur_TextChanged);
            // 
            // tbxAdminHTML_colonne2nom
            // 
            this.tbxAdminHTML_colonne2nom.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxAdminHTML_colonne2nom.Location = new System.Drawing.Point(24, 151);
            this.tbxAdminHTML_colonne2nom.Name = "tbxAdminHTML_colonne2nom";
            this.tbxAdminHTML_colonne2nom.Size = new System.Drawing.Size(144, 26);
            this.tbxAdminHTML_colonne2nom.TabIndex = 48;
            this.tbxAdminHTML_colonne2nom.TextChanged += new System.EventHandler(this.tbxAdminHTML_colonne2nom_TextChanged);
            // 
            // tbxAdminHTML_colonne1largeur
            // 
            this.tbxAdminHTML_colonne1largeur.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxAdminHTML_colonne1largeur.Location = new System.Drawing.Point(174, 119);
            this.tbxAdminHTML_colonne1largeur.Name = "tbxAdminHTML_colonne1largeur";
            this.tbxAdminHTML_colonne1largeur.Size = new System.Drawing.Size(47, 26);
            this.tbxAdminHTML_colonne1largeur.TabIndex = 46;
            this.tbxAdminHTML_colonne1largeur.TextChanged += new System.EventHandler(this.tbxAdminHTML_colonne1largeur_TextChanged);
            // 
            // tbxAdminHTML_colonne1nom
            // 
            this.tbxAdminHTML_colonne1nom.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxAdminHTML_colonne1nom.Location = new System.Drawing.Point(24, 119);
            this.tbxAdminHTML_colonne1nom.Name = "tbxAdminHTML_colonne1nom";
            this.tbxAdminHTML_colonne1nom.Size = new System.Drawing.Size(144, 26);
            this.tbxAdminHTML_colonne1nom.TabIndex = 45;
            this.tbxAdminHTML_colonne1nom.TextChanged += new System.EventHandler(this.tbxAdminHTML_colonne1nom_TextChanged);
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.ForeColor = System.Drawing.SystemColors.Control;
            this.label34.Location = new System.Drawing.Point(20, 95);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(99, 21);
            this.label34.TabIndex = 44;
            this.label34.Text = "Les colonnes";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.ForeColor = System.Drawing.SystemColors.Control;
            this.label33.Location = new System.Drawing.Point(280, 37);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(38, 21);
            this.label33.TabIndex = 43;
            this.label33.Text = "CSS";
            // 
            // tbxAdminHTML_CSS
            // 
            this.tbxAdminHTML_CSS.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxAdminHTML_CSS.Location = new System.Drawing.Point(278, 58);
            this.tbxAdminHTML_CSS.Multiline = true;
            this.tbxAdminHTML_CSS.Name = "tbxAdminHTML_CSS";
            this.tbxAdminHTML_CSS.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbxAdminHTML_CSS.Size = new System.Drawing.Size(427, 216);
            this.tbxAdminHTML_CSS.TabIndex = 42;
            this.tbxAdminHTML_CSS.TextChanged += new System.EventHandler(this.tbxAdminHTML_CSS_TextChanged);
            // 
            // tbxAdminHTML_logo
            // 
            this.tbxAdminHTML_logo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxAdminHTML_logo.Location = new System.Drawing.Point(24, 58);
            this.tbxAdminHTML_logo.Name = "tbxAdminHTML_logo";
            this.tbxAdminHTML_logo.Size = new System.Drawing.Size(144, 26);
            this.tbxAdminHTML_logo.TabIndex = 41;
            this.tbxAdminHTML_logo.TextChanged += new System.EventHandler(this.tbxAdminHTML_logo_TextChanged);
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.ForeColor = System.Drawing.SystemColors.Control;
            this.label32.Location = new System.Drawing.Point(20, 34);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(45, 21);
            this.label32.TabIndex = 40;
            this.label32.Text = "Logo";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Segoe UI", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.SystemColors.Control;
            this.label27.Location = new System.Drawing.Point(12, 17);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(245, 45);
            this.label27.TabIndex = 38;
            this.label27.Text = "Administration";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(515, 39);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 50;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(409, 42);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 20);
            this.textBox1.TabIndex = 51;
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.ClientSize = new System.Drawing.Size(2166, 1100);
            this.Controls.Add(this.menu1);
            this.Controls.Add(this.btnMenu4);
            this.Controls.Add(this.btnMenu3);
            this.Controls.Add(this.btnMenu2);
            this.Controls.Add(this.btnMenu1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.tbxUsers);
            this.Controls.Add(this.lblGUID);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.menu7);
            this.Controls.Add(this.menu5);
            this.Controls.Add(this.menu6);
            this.Controls.Add(this.menu2);
            this.Controls.Add(this.menu4);
            this.Controls.Add(this.menu3);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmMain";
            this.Text = "W";
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.menu2.ResumeLayout(false);
            this.menu2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.menu1.ResumeLayout(false);
            this.menu1.PerformLayout();
            this.menu3.ResumeLayout(false);
            this.menu3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.menu4.ResumeLayout(false);
            this.menu4.PerformLayout();
            this.menu5.ResumeLayout(false);
            this.menu5.PerformLayout();
            this.menu6.ResumeLayout(false);
            this.menu6.PerformLayout();
            this.menu7.ResumeLayout(false);
            this.menu7.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblGUID;
        private System.Windows.Forms.ProgressBar progressbarProjet;
        private System.Windows.Forms.TextBox tbxUsers;
        private System.Windows.Forms.Button btnGenerer;
        private System.Windows.Forms.GroupBox menu2;
        private System.Windows.Forms.ListBox lbxConfig;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.Button btnSupprimerConfig;
        private System.Windows.Forms.ComboBox cbxEmploye;
        private System.Windows.Forms.Button btnAjouterConfig;
        private System.Windows.Forms.ComboBox cbxHeureParJour;
        private System.Windows.Forms.ComboBox cbxProjets;
        private System.Windows.Forms.ComboBox lbxMois;
        private System.Windows.Forms.Button btnSupprimer;
        private System.Windows.Forms.Button btnOuvrir;
        private System.Windows.Forms.Button btnEnvoyerRapports;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnQuitter;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnMenu1;
        private System.Windows.Forms.Button btnMenu2;
        private System.Windows.Forms.Button btnMenu3;
        private System.Windows.Forms.Button btnMenu4;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.GroupBox menu1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.GroupBox menu3;
        private System.Windows.Forms.GroupBox menu4;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox tbxOptionGeneratedFilePath;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label503;
        private System.Windows.Forms.TextBox tbxOptionTitre;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox tbxOptionMessage;
        private System.Windows.Forms.TextBox tbxOptionCC;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Button btnEnregistrerParametreEnvoi;
        private System.Windows.Forms.Label lblVersion;
        private System.Windows.Forms.ListView lbxRapports;
        private System.Windows.Forms.ListView lbxDocumentsGeneres;
        private System.Windows.Forms.Label lblprogressbar;
        private System.Windows.Forms.Button btnMenu5;
        private System.Windows.Forms.GroupBox menu5;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.ListView lbxLogs;
        private System.Windows.Forms.Button btnClearLogs;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.RadioButton rdbOui;
        private System.Windows.Forms.RadioButton rdbNon;
        private System.Windows.Forms.Button btnMenu6;
        private System.Windows.Forms.ComboBox cbxEmploye1;
        private System.Windows.Forms.ComboBox cbxEmploye2;
        private System.Windows.Forms.ComboBox cbxEmploye3;
        private System.Windows.Forms.ComboBox cbxEmploye4;
        private System.Windows.Forms.ComboBox cbxEmploye5;
        private System.Windows.Forms.ComboBox cbxEmploye6;
        private System.Windows.Forms.ComboBox cbxEmploye7;
        private System.Windows.Forms.ComboBox cbxEmploye8;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox tbxPrixJour1;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox tbxPrixJour2;
        private System.Windows.Forms.TextBox tbxPrixJour3;
        private System.Windows.Forms.TextBox tbxPrixJour4;
        private System.Windows.Forms.TextBox tbxPrixJour5;
        private System.Windows.Forms.TextBox tbxPrixJour6;
        private System.Windows.Forms.TextBox tbxPrixJour7;
        private System.Windows.Forms.TextBox tbxPrixJour8;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox tbxNbrJours1;
        private System.Windows.Forms.TextBox tbxNbrJours2;
        private System.Windows.Forms.TextBox tbxNbrJours4;
        private System.Windows.Forms.TextBox tbxNbrJours3;
        private System.Windows.Forms.TextBox tbxNbrJours5;
        private System.Windows.Forms.TextBox tbxNbrJours6;
        private System.Windows.Forms.TextBox tbxNbrJours7;
        private System.Windows.Forms.TextBox tbxNbrJours8;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox tbxRevenue3;
        private System.Windows.Forms.TextBox tbxRevenue2;
        private System.Windows.Forms.TextBox tbxRevenue4;
        private System.Windows.Forms.TextBox tbxRevenue5;
        private System.Windows.Forms.TextBox tbxRevenue6;
        private System.Windows.Forms.TextBox tbxRevenue7;
        private System.Windows.Forms.TextBox tbxRevenue8;
        private System.Windows.Forms.Label lblTotal;
        private System.Windows.Forms.ComboBox lbxMois2;
        private System.Windows.Forms.Button btnGenererAtlanse;
        private System.Windows.Forms.GroupBox menu6;
        private System.Windows.Forms.Button btnATLpreview1;
        private System.Windows.Forms.ListView lbxATLEmployeInformation;
        private System.Windows.Forms.Button btnATLpreview8;
        private System.Windows.Forms.Button btnATLpreview7;
        private System.Windows.Forms.Button btnATLpreview6;
        private System.Windows.Forms.Button btnATLpreview5;
        private System.Windows.Forms.Button btnATLpreview4;
        private System.Windows.Forms.Button btnATLpreview3;
        private System.Windows.Forms.Button btnATLpreview2;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label lblNomPrenomATL;
        private System.Windows.Forms.Button btnUnselectAllGeneratedRapports;
        private System.Windows.Forms.Button btnSelectAllGeneratedRapports;
        private System.Windows.Forms.Button btnUnselectALLrapports;
        private System.Windows.Forms.Button btnSelectALLrapports;
        private System.Windows.Forms.GroupBox menu7;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Button btnMenu7;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox tbxAdminHTML_logo;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox tbxAdminHTML_CSS;
        private System.Windows.Forms.TextBox tbxAdminHTML_colonne1largeur;
        private System.Windows.Forms.TextBox tbxAdminHTML_colonne1nom;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TextBox tbxAdminHTML_colonne5largeur;
        private System.Windows.Forms.TextBox tbxAdminHTML_colonne5nom;
        private System.Windows.Forms.TextBox tbxAdminHTML_colonne4largeur;
        private System.Windows.Forms.TextBox tbxAdminHTML_colonne4nom;
        private System.Windows.Forms.TextBox tbxAdminHTML_colonne3largeur;
        private System.Windows.Forms.TextBox tbxAdminHTML_colonne3nom;
        private System.Windows.Forms.TextBox tbxAdminHTML_colonne2largeur;
        private System.Windows.Forms.TextBox tbxAdminHTML_colonne2nom;
        private System.Windows.Forms.TextBox tbxAdminHTML_logoWidth;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox tbxAdminDIVERS_backupname;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Button btnSaveHTML;
        private System.Windows.Forms.Button btnSaveDIVERS;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btnSaveMAIL;
        private System.Windows.Forms.TextBox tbxAdminMAIL_SMTP;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.TextBox tbxAdminMAIL_Port;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.TextBox tbxAdminMAIL_MailDenvoi;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.TextBox tbxAdminAPI_user;
        private System.Windows.Forms.Button btnSaveAPI;
        private System.Windows.Forms.TextBox tbxAdminAPI_password;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Button btnVoirMDP;
        private System.Windows.Forms.Label lblNbrSelectionRapports;
        private System.Windows.Forms.Label lblNbrSelectionRapportsGenere;
        private System.Windows.Forms.ColumnHeader colDate;
        private System.Windows.Forms.ColumnHeader colProjet;
        private System.Windows.Forms.ColumnHeader colHeure;
        private System.Windows.Forms.ColumnHeader colTypeHeure;
        private System.Windows.Forms.ColumnHeader colCommentaire;
        private System.Windows.Forms.TextBox tbxRevenue1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button button1;
    }
}

