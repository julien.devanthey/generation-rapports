﻿// USING
using System;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Net;
using System.Net.Mail;
using SelectPdf;
using System.Globalization;
using System.Security.Cryptography;
using System.Linq;

namespace GenerationRapports
{
    public partial class frmMain : Form
    {
        // Variables pour le mouvement de la fenêtre
        int mov;
        int movX;
        int movY;

        // INT pour l'apparition de la fenêtre administration
        int iAdmin = 0;

        // Encryption du mot de passe
        string PasswordHash = "P@@Sw0rdDSFSURiztu47fDZFSdf47";
        string SaltKey = "S@LT&KEY";
        string VIKey = "@1B2c3D4e5F6g7H8";

        public frmMain()
        {
            InitializeComponent();
        }

        // Déplacement de la fenêtre 1
        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            mov = 1;
            movX = e.X;
            movY = e.Y;
        }

        // Déplacement de la fenêtre 2
        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            if (mov == 1)
            {
                this.SetDesktopLocation(MousePosition.X - movX, MousePosition.Y - movY);
            }
        }

        // Déplacement de la fenêtre 3
        private void panel1_MouseUp(object sender, MouseEventArgs e)
        {
            mov = 0;
        }

        // Chargement de la fenêtre
        private void frmMain_Load(object sender, EventArgs e)
        {
            try
            {

                // Separator format
                NumberFormatInfo nfi = new CultureInfo("en-US", false).NumberFormat;
                nfi.NumberGroupSeparator = ".";

                // Listviews paramètres visuel
                lbxRapports.Columns.Add("", -2);
                lbxDocumentsGeneres.Columns.Add("", -2);
                lbxLogs.Columns.Add("", -2);
                lbxATLEmployeInformation.Columns.Add("", -2);

                // Vérifier si la version du programme est égal à celle des options
                /*
                if(lblVersion.Text != "Version " + getSettings("version"))
                {
                    MessageBox.Show("ATTENTION : La version du fichier de configuration est obsolète");
                }
                */

                // Taille de la fenêtre
                this.MinimumSize = new System.Drawing.Size(1251, 1040);
                this.MaximumSize = new System.Drawing.Size(1251, 1040);

                // Set location des pages
                SetLocationOfMenus(233, 33);

                // Selection du la première page
                SelectMenu(1);

                // Chargement des paramètres
                LoadSettings();

                // Boutons ATL
                EnableOrDisableAllBTNPreviewATL();

                // Chargement des API
                GetGUIDandUSERS();

                // Afficher les trois mois avant dans les deux listbox
                ShowMonthInListbox(lbxMois);
                ShowMonthInListbox(lbxMois2);

                // Afficher les valeurs d'heures par jour dans la configuration
                cbxHeureParJour.Items.Clear();
                cbxHeureParJour.Items.Add("8.4");
                cbxHeureParJour.Items.Add("8.2");
                cbxHeureParJour.Items.Add("8.0");

                // Refresh
                RefreshListbox(3);
                RefreshListbox(4);
                RefreshListbox(5);
                RefreshListbox(6);
                RefreshListbox(7);
            } catch
            {
                MessageBox.Show("Une erreur est survenue lors du chargement de l'application.\n" + "Vérifiez votre connexion internet, la disponibilité de l'api de aceproject.com ou l'exactitude du mot de passe pour l'utilisateur \"" + getSettings("apiUserName") + "\"");
            }
        }

        private void cbxEmploye_SelectedIndexChanged(object sender, EventArgs e)
        {
            // refresh
            RefreshListbox(2);
            progressbarProjet.Value = 100; // PROGRESS
            Application.DoEvents(); // PROGRESS
        }
        private void btnAjouterConfig_Click(object sender, EventArgs e)
        {
            // Récupération valeurs
            string sEmploye = Convert.ToString(cbxEmploye.SelectedItem);
            string sProjet = Convert.ToString(cbxProjets.SelectedItem);
            string sDivisePar = "";
            sDivisePar = Convert.ToString(cbxHeureParJour.SelectedItem);

            // Check si aucun champ n'est vide
            if (sEmploye != "" && sProjet != "" && sDivisePar != "")
            {
                // Ajouter la config
                AjouterConfig(sEmploye, sProjet, sDivisePar);

                // Refresh
                RefreshListbox(4);
            } else
            {
                MessageBox.Show("Veuillez séléctionner tous les champs");
            }

            // Enlever la valeur de la listbox de Projets
            cbxProjets.Text = "";

        }
        private void btnSupprimerConfig_Click(object sender, EventArgs e)
        {
            // Récupération de la valeur à supprimer
            string sSelectedConfig = Convert.ToString(lbxConfig.SelectedItem);

            // Supprimer la config
            if (sSelectedConfig != "")
            {
                SupprimerConfig(sSelectedConfig);
            } else
            {
                MessageBox.Show("Veuillez séléctionner une configuration");
            }
        }
        private void lbxMois_SelectionChangeCommitted(object sender, EventArgs e)
        {
            // Refresh
            lblprogressbar.Text = "Chargement des rapports...";
            RefreshListbox(5);
            lblprogressbar.Text = getSettings("labelProgressBarDone");
        }
        private void btnGenerer_Click(object sender, EventArgs e)
        {
            // Check si rien n'est séléctionné
            int v = 0;
            for (int i = 0; i < lbxRapports.Items.Count; i++)
            {
                if (lbxRapports.Items[i].Checked == true)
                {
                    v++;
                }
            }
            if (v == 0)
            {
                MessageBox.Show("Veuillez séléctionner un ou plusieurs rapports.");
            } else
            {
                // Calcule du nombre de rapports séléctionné
                int x = 0;
                int y = 0;
                for (int i = 0; i < lbxRapports.Items.Count; i++)
                {
                    if (lbxRapports.Items[i].Checked == true)
                    {
                        x++;
                    }
                }

                // Pour chaque rapports séléctionné
                for (int i = 0; i < lbxRapports.Items.Count; i++)
                {
                    if (lbxRapports.Items[i].Checked == true)
                    {
                        string sSelectedItem = lbxRapports.Items[i].Text.Split(new string[] { " ||" }, StringSplitOptions.None)[0];
                        lblprogressbar.Visible = true;
                        lblprogressbar.Text = "Préparation des données..."; // PROGRESS
                        Application.DoEvents(); // PROGRESS

                        // Récupération des valeurs de l'item séléctionné
                        string sSelectedPrenom = sSelectedItem.Split('-')[0];
                        string sSelectedNom = sSelectedItem.Split('-')[1].Split('_')[0];
                        string sSelectedAnnee = sSelectedItem.Split('_')[1];
                        string sSelectedMois = sSelectedItem.Split('_')[2];
                        string sSelectedProjet = sSelectedItem.Split('_')[3].Split(new string[] { " / " }, StringSplitOptions.None)[0].Replace(".pdf", "");
                        string sSelectedDivisePar = sSelectedItem.Split(new string[] { " / " }, StringSplitOptions.None)[1];

                        // Generer le rapport
                        y++;
                        lblprogressbar.Text = "[" + y + "/" + x + "] Génération du rapport de " + sSelectedPrenom + " " + sSelectedNom + " (" + sSelectedProjet + ")....";
                        Application.DoEvents(); // PROGRESS
                        GenererRapport(sSelectedPrenom, sSelectedNom, sSelectedAnnee, sSelectedMois, sSelectedProjet, sSelectedDivisePar);
                    }
                }
                // Refresh
                lblprogressbar.Text = "Fin de la génération....";
                Application.DoEvents(); // PROGRESS
                RefreshListbox(6);
                RefreshListbox(5);

                // Loading
                lblprogressbar.Text = getSettings("labelProgressBarDone"); // PROGRESS
                Application.DoEvents(); // PROGRESS

                // Décocher toutes les cases
                for (int j = 0; j < lbxRapports.Items.Count; j++)
                {
                    lbxRapports.Items[j].Checked = false;
                }
            }
        }
        private void btnSupprimer_Click(object sender, EventArgs e)
        {

            // Pour chaque rapports séléctionné
            for (int i = 0; i < lbxDocumentsGeneres.Items.Count; i++)
            {
                if (lbxDocumentsGeneres.Items[i].Checked == true)
                {
                    // Variable
                    string sNomDuRapport = lbxDocumentsGeneres.Items[i].Text.Split(new string[] { " ||" }, StringSplitOptions.None)[0];

                    // Supprimer
                    lblprogressbar.Text = "Suppression du rapport " + lbxDocumentsGeneres.Items[i].Text.Split(new string[] { " ||" }, StringSplitOptions.None)[0];
                    SupprimerRapportGenere(sNomDuRapport);
                }
            }
            // Refresh
            RefreshListbox(6);
            lblprogressbar.Text = getSettings("labelProgressBarDone");
        }
        private void btnOuvrir_Click(object sender, EventArgs e)
        {
            // Pour chaque rapports séléctionné
            for (int i = 0; i < lbxDocumentsGeneres.Items.Count; i++)
            {
                if (lbxDocumentsGeneres.Items[i].Checked == true)
                {
                    // Variable
                    string sNomDuRapport = lbxDocumentsGeneres.Items[i].Text.Split(new string[] { " ||" }, StringSplitOptions.None)[0];

                    // Ouvrir
                    lblprogressbar.Text = "Ouverture du rapport de " + lbxDocumentsGeneres.Items[i].Text.Split(new string[] { " ||" }, StringSplitOptions.None)[0];
                    System.Diagnostics.Process.Start(getSettings("FilePathGeneration") + @"\" + sNomDuRapport);
                }
            }
            lblprogressbar.Text = getSettings("labelProgressBarDone");
        }
        private void btnQuitter_Click(object sender, EventArgs e)
        {
            // Quitter
            this.Close();
        }

        private void btnMenu1_Click(object sender, EventArgs e)
        {
            // Afficher le menu
            SelectMenu(1);
        }

        private void btnMenu2_Click(object sender, EventArgs e)
        {
            SelectMenu(2);
        }

        private void btnMenu3_Click(object sender, EventArgs e)
        {
            SelectMenu(3);
        }

        private void btnMenu4_Click(object sender, EventArgs e)
        {
            SelectMenu(4);
        }
        private void btnMenu5_Click(object sender, EventArgs e)
        {
            SelectMenu(5);
        }
        private void btnMenu6_Click(object sender, EventArgs e)
        {
            SelectMenu(6);
        }
        private void btnMenu7_Click(object sender, EventArgs e)
        {
            SelectMenu(7);
        }
        private void tbxOptionTitre_TextChanged(object sender, EventArgs e)
        {
            // Activer bouton enregistrer si il y a une différence
            MailActiverOuDesactiverBoutonEnregistrer();
        }

        private void tbxOptionCC_TextChanged(object sender, EventArgs e)
        {
            // Activer bouton enregistrer si il y a une différence
            MailActiverOuDesactiverBoutonEnregistrer();
        }

        private void tbxOptionMessage_TextChanged(object sender, EventArgs e)
        {
            // Activer bouton enregistrer si il y a une différence
            MailActiverOuDesactiverBoutonEnregistrer();
        }
        private void btnEnregistrerParametreEnvoi_Click(object sender, EventArgs e)
        {
            // Empêcher l'insertion d'un "
            if(tbxOptionTitre.Text.Contains("\"") || tbxOptionCC.Text.Contains("\"") ||tbxOptionMessage.Text.Contains("\"")) { MessageBox.Show("Caractère interdit : \""); return; }

            // Enregistrer les paramètre
            updateSettings("mailTitre", tbxOptionTitre.Text);
            updateSettings("mailCC", tbxOptionCC.Text);
            updateSettings("mailMessage", tbxOptionMessage.Text.Replace("\r\n", "{rn}"));

            // Désactiver le bouton
            MailActiverOuDesactiverBoutonEnregistrer();
        }
        private void btnEnvoyerRapports_Click(object sender, EventArgs e)
        {
            // Check si y'a plusieurs rapports pour une même personne
            string sEmployes = "";
            string sMultiRapportEmployes = "";
            for (int j = 0; j < lbxDocumentsGeneres.Items.Count; j++)
            {
                if (lbxDocumentsGeneres.Items[j].Checked == true)
                {
                    string sCurrentEmploye = lbxDocumentsGeneres.Items[j].Text.Split('_')[0];
                    if (sEmployes.Contains(sCurrentEmploye))
                    {
                        sMultiRapportEmployes += sCurrentEmploye + "\n";
                    } else
                    {
                        sEmployes += sCurrentEmploye;
                    }
                }
            }

            // Check si rien n'est séléctionné
            int v = 0;
            for (int i = 0; i < lbxDocumentsGeneres.Items.Count; i++)
            {
                if (lbxDocumentsGeneres.Items[i].Checked == true)
                {
                    v++;
                }
            }
            if (v == 0)
            {
                MessageBox.Show("Veuillez séléctionner un ou plusieurs rapports.");
            } else
            {
                // Progress
                lblprogressbar.Visible = true;
                lblprogressbar.Text = "Chargement des données"; // PROGRESS
                Application.DoEvents(); // PROGRESS

                // variable
                string sMessageInformatif = "";
                int iNbrErreurEnvoi = 0;

                // Calcule du nombre de documents séléctionné
                int x = 0;
                int y = 0;
                for (int i = 0; i < lbxDocumentsGeneres.Items.Count; i++)
                {
                    if (lbxDocumentsGeneres.Items[i].Checked == true)
                    {
                        x++;
                    }
                }


                // Pour chaque rapports séléctionné
                for (int i = 0; i < lbxDocumentsGeneres.Items.Count; i++)
                {
                    if (lbxDocumentsGeneres.Items[i].Checked == true)
                    {
                        // Variable
                        string[] AllUsers = tbxUsers.Text.Split(new string[] { "},{" }, StringSplitOptions.None);
                        string sNomDuRapport = lbxDocumentsGeneres.Items[i].Text.Split(new string[] { " ||" }, StringSplitOptions.None)[0];
                        string sPrenom = sNomDuRapport.Split('-')[0];
                        string sNom = sNomDuRapport.Split('-')[1].Split('_')[0];
                        string sAnnee = sNomDuRapport.Split('-')[1].Split('_')[1];
                        string sMois = sNomDuRapport.Split('-')[1].Split('_')[2];
                        string sProjet = sNomDuRapport.Split(new string[] { sPrenom + "-" + sNom + "_" + sAnnee + "_" + sMois + "_" }, StringSplitOptions.None)[1].Replace(".pdf", "");
                        string sMail = "MAIL_NOT_FOUND";
                        string sAttachment = getSettings("FilePathGeneration") + sNomDuRapport;

                        // Recherche du mail
                        for (int j = 0; j < AllUsers.Length; j++)
                        {
                            string sFoundFirstName = Encoding.UTF8.GetString(Encoding.Default.GetBytes(GetDataFrom("FIRST_NAME", AllUsers[j], "STRING")));
                            string sFoundLastName = Encoding.UTF8.GetString(Encoding.Default.GetBytes(GetDataFrom("LAST_NAME", AllUsers[j], "STRING")));
                            if (sFoundFirstName == sPrenom && sFoundLastName == sNom)
                            {
                                sMail = GetDataFrom("EMAIL_ALERT", AllUsers[j], "STRING");
                                break;
                            }
                        }

                        // Envoyer mail
                        y++;
                        lblprogressbar.Text = "[" + y + "/" + x + "] Envoi du rapport \"" + sNomDuRapport + "\" à " + sPrenom + " " + sNom;
                        Application.DoEvents(); // PROGRESS
                                                //MessageBox.Show("Nom Du Rapport: " + sNomDuRapport + "\nPrenom: " + sPrenom + "\nNom: " + sNom + "\nProjet: " + sProjet + "\nMail: " + sMail);

                        // Envoi du mail
                        bool isSentCorrectly = sendMail(sAttachment, sPrenom, sNom, sProjet, sMail, sMois, sMultiRapportEmployes);

                        // Si le mail a été mal envoyé ou a subis une erreur
                        if (!isSentCorrectly)
                        {
                            sMessageInformatif += "ERREUR:\n" + sPrenom + " " + sNom + " n'a pas reçu son rapport correctement !\n";
                            iNbrErreurEnvoi++;
                        } 
                        else
                        {
                            sMessageInformatif += "SUCCÈS: " + sPrenom + " " + sNom + " a bien reçu son/ses rapport.\n";
                        }
                    }
                }

                // Message informatif
                if (iNbrErreurEnvoi == 0)
                {
                    MessageBox.Show("Tous les mails ont correctement été envoyés !\n\n" + sMessageInformatif);
                }
                else
                {
                    MessageBox.Show("ATTENTION ! Il y a " + iNbrErreurEnvoi + "mail(s) qui n'a pas pu être envoyé !\n\n" + sMessageInformatif);
                }

                // Refresh listbox 5
                RefreshListbox(5);
                RefreshListbox(6);
                updateSettings("allSendedMail", "");

                // Progress
                lblprogressbar.Text = getSettings("labelProgressBarDone");// PROGRESS
                Application.DoEvents(); // PROGRESS
            }
        }
        private void tbxOptionGeneratedFilePath_MouseClick(object sender, MouseEventArgs e)
        {
            // Recherche du dossier
            string sSelectedPath = "";
            using (var fbd = new FolderBrowserDialog())
            {
                DialogResult result = fbd.ShowDialog();

                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                {
                    string[] files = Directory.GetFiles(fbd.SelectedPath);
                    sSelectedPath = fbd.SelectedPath;
                }
            }

            // Changement de la valeur
            if (sSelectedPath != "")
            {
                updateSettings("FilePathGeneration", sSelectedPath);
                tbxOptionGeneratedFilePath.Text = sSelectedPath;
            }
        }
        private void btnClearLogs_Click(object sender, EventArgs e)
        {

            // Clear logs
            ClearLogs();
        }
        private void rdbNon_CheckedChanged(object sender, EventArgs e)
        {
            // Changement de l'option sur FALSE
            updateSettings("OpenRapportWhenGenerated", "False");
        }

        private void rdbOui_CheckedChanged(object sender, EventArgs e)
        {
            // Changement de l'option sur TRUE
            updateSettings("OpenRapportWhenGenerated", "True");
        }
        private void btnGenererAtlanse_Click(object sender, EventArgs e)
        {

            // Refresh
            RefreshListbox(7);

            // Générer tous les rapports d'atlanse
            atlanseGenererALL();

            // Revenu total
            AfficherRevenuTotal();

        }
        private void cbxEmploye1_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Sauvegarder la donnée
            saveAtlanseEmployeNom(cbxEmploye1, 1);

            // Check si le bouton preview peut être activé
            EnableOrDisableAllBTNPreviewATL();

        }

        private void cbxEmploye2_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Sauvegarder la donnée
            saveAtlanseEmployeNom(cbxEmploye2, 2);

            // Check si le bouton preview peut être activé
            EnableOrDisableAllBTNPreviewATL();
        }

        private void cbxEmploye3_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Sauvegarder la donnée
            saveAtlanseEmployeNom(cbxEmploye3, 3);

            // Check si le bouton preview peut être activé
            EnableOrDisableAllBTNPreviewATL();
        }

        private void cbxEmploye4_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Sauvegarder la donnée
            saveAtlanseEmployeNom(cbxEmploye4, 4);

            // Check si le bouton preview peut être activé
            EnableOrDisableAllBTNPreviewATL();
        }

        private void cbxEmploye5_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Sauvegarder la donnée
            saveAtlanseEmployeNom(cbxEmploye5, 5);

            // Check si le bouton preview peut être activé
            EnableOrDisableAllBTNPreviewATL();
        }

        private void cbxEmploye6_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Sauvegarder la donnée
            saveAtlanseEmployeNom(cbxEmploye6, 6);

            // Check si le bouton preview peut être activé
            EnableOrDisableAllBTNPreviewATL();

        }

        private void cbxEmploye7_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Sauvegarder la donnée
            saveAtlanseEmployeNom(cbxEmploye7, 7);

            // Check si le bouton preview peut être activé
            EnableOrDisableAllBTNPreviewATL();

        }

        private void cbxEmploye8_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Sauvegarder la donnée
            saveAtlanseEmployeNom(cbxEmploye8, 8);

            // Check si le bouton preview peut être activé
            EnableOrDisableAllBTNPreviewATL();

        }

        private void tbxPrixJour1_TextChanged(object sender, EventArgs e)
        {
            // Sauvegarder la donnée
            saveAtlanseEmployePrixParJour(tbxPrixJour1, 1);


            // Check si le bouton preview peut être activé
            EnableOrDisableAllBTNPreviewATL();
        }

        private void tbxPrixJour2_TextChanged(object sender, EventArgs e)
        {
            // Sauvegarder la donnée
            saveAtlanseEmployePrixParJour(tbxPrixJour2, 2);


            // Check si le bouton preview peut être activé
            EnableOrDisableAllBTNPreviewATL();
        }

        private void tbxPrixJour3_TextChanged(object sender, EventArgs e)
        {
            // Sauvegarder la donnée
            saveAtlanseEmployePrixParJour(tbxPrixJour3, 3);


            // Check si le bouton preview peut être activé
            EnableOrDisableAllBTNPreviewATL();
        }

        private void tbxPrixJour4_TextChanged(object sender, EventArgs e)
        {
            // Sauvegarder la donnée
            saveAtlanseEmployePrixParJour(tbxPrixJour4, 4);


            // Check si le bouton preview peut être activé
            EnableOrDisableAllBTNPreviewATL();
        }

        private void tbxPrixJour5_TextChanged(object sender, EventArgs e)
        {
            // Sauvegarder la donnée
            saveAtlanseEmployePrixParJour(tbxPrixJour5, 5);


            // Check si le bouton preview peut être activé
            EnableOrDisableAllBTNPreviewATL();
        }

        private void tbxPrixJour6_TextChanged(object sender, EventArgs e)
        {
            // Sauvegarder la donnée
            saveAtlanseEmployePrixParJour(tbxPrixJour6, 6);


            // Check si le bouton preview peut être activé
            EnableOrDisableAllBTNPreviewATL();
        }

        private void tbxPrixJour7_TextChanged(object sender, EventArgs e)
        {
            // Sauvegarder la donnée
            saveAtlanseEmployePrixParJour(tbxPrixJour7, 7);


            // Check si le bouton preview peut être activé
            EnableOrDisableAllBTNPreviewATL();
        }

        private void tbxPrixJour8_TextChanged(object sender, EventArgs e)
        {
            // Sauvegarder la donnée
            saveAtlanseEmployePrixParJour(tbxPrixJour8, 8);

            // Check si le bouton preview peut être activé
            EnableOrDisableAllBTNPreviewATL();

        }
        private void btnATLpreview1_Click(object sender, EventArgs e)
        {
            ShowEmployeATLtask(cbxEmploye1.Text);
        }

        private void btnATLpreview2_Click(object sender, EventArgs e)
        {
            ShowEmployeATLtask(cbxEmploye2.Text);
        }

        private void btnATLpreview3_Click(object sender, EventArgs e)
        {
            ShowEmployeATLtask(cbxEmploye3.Text);
        }

        private void btnATLpreview4_Click(object sender, EventArgs e)
        {
            ShowEmployeATLtask(cbxEmploye4.Text);
        }

        private void btnATLpreview5_Click(object sender, EventArgs e)
        {
            ShowEmployeATLtask(cbxEmploye5.Text);
        }

        private void btnATLpreview6_Click(object sender, EventArgs e)
        {
            ShowEmployeATLtask(cbxEmploye6.Text);
        }

        private void btnATLpreview7_Click(object sender, EventArgs e)
        {
            ShowEmployeATLtask(cbxEmploye7.Text);
        }

        private void btnATLpreview8_Click(object sender, EventArgs e)
        {
            ShowEmployeATLtask(cbxEmploye8.Text);
        }

        private void btnSelectALLrapports_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem item in lbxRapports.Items)
            {
                item.Checked = true;
            }
        }

        private void btnUnselectALLrapports_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem item in lbxRapports.Items)
            {
                item.Checked = false;
            }
        }

        private void btnSelectAllGeneratedRapports_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem item in lbxDocumentsGeneres.Items)
            {
                item.Checked = true;
            }
        }

        private void btnUnselectAllGeneratedRapports_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem item in lbxDocumentsGeneres.Items)
            {
                item.Checked = false;
            }
        }
        private void btnSaveHTML_Click(object sender, EventArgs e)
        {
            SaveAdminSettings(1);
        }
        private void btnSaveDIVERS_Click(object sender, EventArgs e)
        {
            SaveAdminSettings(2);
        }
        private void btnSaveMAIL_Click(object sender, EventArgs e)
        {
            SaveAdminSettings(3);
        }
        private void btnSaveAPI_Click(object sender, EventArgs e)
        {
            SaveAdminSettings(4);
        }
        private void pictureBox3_Click(object sender, EventArgs e)
        {
            // Afficher la section administration
            if (iAdmin == 1)
            {
                // Afficher
                btnMenu7.Visible = true;

            }
            else
            {
                // Incrémenter
                iAdmin++;

            }
        }
        private void btnVoirMDP_Click(object sender, EventArgs e)
        {
            // Active ou désactive les mots de passe
            if (tbxAdminAPI_password.PasswordChar == '*')
            {
                string a = tbxAdminAPI_password.Text;
                tbxAdminAPI_password.PasswordChar = '\0';
            }
            else
            {
                // Cacher le mot de passe
                tbxAdminAPI_password.PasswordChar = '*';
            }
        }
        private void lbxRapports_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            refreshNbrSelectionne(1);
        }

        private void lbxDocumentsGeneres_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            refreshNbrSelectionne(2);
        }
        // ==================================================================================================================================================
        // =============================================================== FONCTIONS ========================================================================
        // ==================================================================================================================================================

        // ==================================================================================================================================================
        // FONCTION 1 : GetWeekNumberOfMonth(DateTime date)
        // Description : Calcule, par rapport à une date donnée, de quelle numéro de semaine elle fait partie (Semaine 1, semaine 2, semaine 3, semaine 4 ...)
        // Entrée 1 : DateTime [Une date (DateTime(iAnnee, iMonth, iDay))]
        // Sortie : int [Numero De Semaine]

        // ==================================================================================================================================================
        // FONCTION 2 : GenererRapport(string sPrenom, string sNom, string sAnnee, string sMois, string sProjet, string sDivisePar)
        // Description : Génère un rapport (.pdf) en fonction des données inscrites
        // Entrée 1 : string [Prenom]
        // Entrée 2 : string [Nom]
        // Entrée 3 : string [Annee (yyyy)]
        // Entrée 4 : string [Mois (MM)]
        // Entrée 5 : string [Projet]
        // Entrée 6 : string [Heures/jour]
        // Sortie : Aucune

        // ==================================================================================================================================================
        // FONCTION 3 : GenererPDF(string HTMLstring, string sCheminDuFichierDeDestination)
        // Description : Génère un pdf depuis un string HTML
        // Entrée 1 : string [HTML]
        // Entrée 2 : string [FichierDeDestination.pdf]
        // Sortie : Aucune (Hormis le fait qu'il génère un pdf dans le dossier demandé)

        // ==================================================================================================================================================
        // FONCTION 4 : ToHtml(string sAllTasks, string sNomDuProjet, string sDivisePar, string sNom, string sPrenom)
        // Description : Retourne un string HTML des données
        // Entrée 1 : string [Toutes les tâches de la personne]
        // Entrée 2 : string [Nom du projet]
        // Entrée 3 : string [Heures/jour]
        // Entrée 4 : string [Nom]
        // Entrée 5 : string [Prénom]
        // Sortie : string [HTML]

        // ==================================================================================================================================================
        // FONCTION 5 : CreateHtmlTableFromHours(double dHeureTotal, double dHeureParJour)
        // Description : Créer la table HTML des heures
        // Entrée 1 : double [Total heures]
        // Entrée 2 : double [Heures/jour]
        // Sortie : string [HTML]

        // ==================================================================================================================================================
        // FONCTION 6 : CreateHtmlTableFromAllTasks(string sAllTasks, string sNomDuProjet, string sPrenom, string sNom)
        // Description : Créer la table HTML des tâches de l'utilisateur
        // Entrée 1 : string [Toutes les tâches de la personne]
        // Entrée 2 : string [Nom du projet]
        // Entrée 3 : string [Prénom]
        // Entrée 4 : string [Nom]
        // Sortie : string [HTML]

        // ==================================================================================================================================================
        // FONCTION 7 : SupprimerConfig(string sSelectedConfig)
        // Description : Supprime la configuration séléctionnée
        // Entrée 1 : string [Selected Config]
        // Sortie : Aucune

        // ==================================================================================================================================================
        // FONCTION 8 : AjouterConfig(string sEmploye, string sProjet, string sDivisePar)
        // Description : Ajoute une configuration
        // Entrée 1 : string [Nom de l'employé]
        // Entrée 2 : string [Nom du projet]
        // Entrée 3 : string [Heures/jour]
        // Sortie : Aucune

        // ==================================================================================================================================================
        // FONCTION 9 : FormatDate(string sDate, int iParam)
        // Description : Formate une date avec un paramètre choisis
        // Entrée 1 : string [date]
        // Entrée 2 : int [paramètre] | 1 = yyyy-MM-ddT00:00:00 -> dd.MM.yyyy | 2 = dd.MM.yyyy -> yyyy.MM.dd | 3 = dd.MM.yyyy -> dd.MM
        // Sortie : string [date]

        // ==================================================================================================================================================
        // FONCTION 10 : GetDataFrom(string sNomAttribut, string sJson)
        // Description : Récupère la donnée d'un attribut dans un JSON (api quoi)
        // Entrée 1 : string [Nom de l'attribut]
        // Entrée 2 : string [Json]
        // Entrée 3 : string [sType] (Type de la valeur recherché)
        // Sortie : string [Donnée de l'attribut]

        // ==================================================================================================================================================
        // FONCTION 11 : RefreshListbox(int iParameter)
        // Description : Rafraîchis les listboxs / donnée de l'application
        // Entrée 1 : int [Paramètre] | 1 = ALL | 2 = Only projets (config) | 3 = Only users (Config) | 4 = Only configurations | 5 = Only rapports | 6 = Only documents générés | 7 = Only employés atlanse
        // Sortie : Aucune

        // ==================================================================================================================================================
        // FONCTION 12 : lastSend(string sNomDuRapportGenere, string[] sLogsOfUser) // Retour : NO or YES;DATE
        // Description : Recherche du dernier envoie du rapport séléctionné dans les logs (Si il a été envoyé et si oui, quelle date)
        // Entrée 1 : string [Nom du rapport généré]
        // Entrée 2 : string[] [logs]
        // Sortie : string [Yes/No];[Date]

        // ==================================================================================================================================================
        // FONCTION 13 : lastGegeneration(string sNomDuRapportGenere, string[] sLogsOfUser) // Retour : NO or YES;DATE
        // Description : Recherche de la dernière génération du rapport séléctionné dans les logs (Si il a été généré et si oui, quelle date)
        // Entrée 1 : string [Nom du rapport généré]
        // Entrée 2 : string[] [logs]
        // Sortie : string [Yes/No];[Date]

        // ==================================================================================================================================================
        // FONCTION 14 : getAllLogsFrom(string sPrenomNom)
        // Description : Récupère l'entièreté des logs concernant une personne
        // Entrée 1 : string [Prenom + " " + Nom]
        // Sortie : string[] [logs]

        // ==================================================================================================================================================
        // FONCTION 15 : GetDateFromTheMonthNumberAndYearNumber(int iAnnee, int iMonth, int iParam) 
        // Description : Récupère la date du mois (MM) et l'année (yyyy)
        // Entrée 1 : int [Annee (yyyy)]
        // Entrée 2 : int [Mois (MM)]
        // Entrée 3 : int [Paramètre] | 1 = Date de début du mois | 2 = Date de fin du mois
        // Sortie : string [Date (dd.MM.yyyy)]

        // ==================================================================================================================================================
        // FONCTION 16 : GetDateFromTheMonthIndex(int iIndex, int iParam)
        // Description : Récupère la date du mois depuis l'index (L'index est la valeur donnée au mois lors de la séléction de celui-ci)
        // Entrée 1 : int [index] | 0 = Mois actuel |1 = Mois actuel - 1 |2 = Mois actuel - 2
        // Entrée 2 : int [Paramètre] | 1 = Date de début du mois | 2 = Date de fin du mois
        // Sortie : string [date (dd.MM.yyyy)]

        // ==================================================================================================================================================
        // FONCTION 17 : GetAnneeFromTheMonth(int iIndex)
        // Description : Récupère l'année du mois depuis l'index
        // Entrée 1 : int [index] | 0 = Mois actuel |1 = Mois actuel - 1 |2 = Mois actuel - 2
        // Sortie : string [date(yyyy)]

        // ==================================================================================================================================================
        // FONCTION 18 : SupprimerRapportGenere(string sNomDuRapportGenere)
        // Description : Supprime le rapport généré
        // Entrée 1 : string [Nom du rapport généré]
        // Sortie : Aucune

        // ==================================================================================================================================================
        // FONCTION 19 : SelectMenu(int iParameter)
        // Description : Sélectionne le menu
        // Entrée 1 : int [Numéro du menu] | 1 = Accueil | 2 = Configurer les rapports | 3 = Configurer les envois | 4 = Configurer les chemins d'accès | 5 = Logs | 6 = Atlanse
        // Sortie : Aucune

        // ==================================================================================================================================================
        // FONCTION 20 : MailActiverOuDesactiverBoutonEnregistrer()
        // Description : Vérifie si oui ou non le bouton d'envoie de mail doit être activé. Si oui, il l'active, si non, il ne l'active pas
        // Entrée 1 : Aucune
        // Sortie : Aucune

        // ==================================================================================================================================================
        // FONCTION 21 : sendMail(string sAttachment, string sPrenom, string sNom, string sProjet, string sMail, string sMois, string sMultiRapportEmployes)
        // Description : Envoie un mail à l'employé
        // Entrée 1 : string [Chemin du fichier qui sera joint]
        // Entrée 2 : string [Prenom]
        // Entrée 3 : string [Nom]
        // Entrée 4 : string [Projet]
        // Entrée 5 : string [Mail du destinataire]
        // Entrée 6 : string [Mois(MM)]
        // Entrée 7 : string [Liste des employés avec plusieurs attachments]
        // Sortie : bool [Si le mail a correctement été envoyé ou non] | TRUE = Mail envoyé | FALSE = Une erreur s'est produite lors de l'envoi du mail

        // ==================================================================================================================================================
        // FONCTION 22 : addLogs(int iParam1, int iParam2, string sPrenom, string sNom, string sProjet, string sDivisePar, string sMois, string sMail, string sNomDuRapport)
        // Description : Ajouter dans les logs une nouvelle ligne
        // Entrée 1 : int [Paramètre] (Quel type de logs)| 1 = Ajouter logs génération de rapports | 2 = Ajouter logs envoi de rapports | 3 = Ajouter logs suppression de rapports générés
        // Entrée 2 : int [Paramètre] (Ajouter une info ou une erreur ?) | 0 = NOK | 1 = OK
        // Entrée 3 : string [Prenom]
        // Entrée 4 : string [Nom]
        // Entrée 5 : string [Projet]
        // Entrée 6 : string [Heures/jour]
        // Entrée 7 : string [Mois(MM)]
        // Entrée 8 : string [Mail]
        // Entrée 9 : string [Nom du rapport]
        // Sortie : Aucune

        // ==================================================================================================================================================
        // FONCTION 23 : atlanseGenerer(int iNumeroEmploye, ComboBox cbxPrenomNom, TextBox tbxPrixParJour, TextBox tbxRevenu) 
        // Description : Générer le revenu ainsi que le nombre de jours de travail de l'employé chez Atlanse
        // Entrée 1 : int [Ligne]
        // Entrée 1 : ComboBox [cbxPrenomNom]
        // Entrée 1 : TextBox [tbxPrixParJour]
        // Entrée 1 : TextBox [tbxRevenu]
        // Sortie : Aucune

        // ==================================================================================================================================================
        // FONCTION 24 : saveAtlanseEmployeNom(ComboBox cbx, int iNo)
        // Description : Enregistre le nom de l'employé en base (Atlanse)
        // Entrée 1 : ComboBox [cbxEmploye]
        // Entrée 2 : int [Ligne]
        // Sortie : Aucune

        // ==================================================================================================================================================
        // FONCTION 26 : saveAtlanseEmployePrixParJour(TextBox tbx, int iNo)
        // Description : Enregistre le prix par jour de l'employé en base (Atlanse)
        // Entrée 1 : TextBox [tbxPrixParJour]
        // Entrée 2 : int [Ligne]
        // Sortie : Aucune

        // ==================================================================================================================================================
        // FONCTION 27 : EnableOrDisableAllBTNPreviewATL()
        // Description : Fonction qui regarde si le bouton de preview des dossiers atlense peuvent être vu ou pas. (Active ou désactive le bouton)
        // Entrée : Aucune

        // ==================================================================================================================================================
        // FONCTION 28 : EnableBTN(Button btn)
        // Description : En lien avec la fonction 27 -> Active le bouton
        // Entrée 1 : Button [bouton]
        // Sortie : Aucune

        // ==================================================================================================================================================
        // FONCTION 29 : DisableBTN(Button btn)
        // Description : En lien avec la fonction 27 -> Désactive le bouton
        // Entrée 1 : Button [bouton]
        // Sortie : Aucune

        // ==================================================================================================================================================
        // FONCTION 30 : ShowEmployeATLtask(string sEmploye)
        // Description : Montre toutes les tâches de l'employés ATL pendant le mois séléctionné
        // Entrée 1 : string sPrenomEtNom
        // Sortie : Aucune

        // ==================================================================================================================================================
        // FONCTION 31 : GetGUIDandUSERS()
        // Description : Appel API pour récupérer le GUID et la liste de tous les utilisateurs
        // Entrée 1 : Aucune
        // Sortie : Aucune

        // ==================================================================================================================================================
        // FONCTION 32 : LoadSettings()
        // Description : Chargement des paramètres
        // Entrée 1 : Aucune
        // Sortie : Aucune

        // ==================================================================================================================================================
        // FONCTION 33 : ShowMonthInListbox(ComboBox lbx)
        // Description : Montre la liste des 3 derniers mois dans une ComboxBox
        // Entrée 1 : ComboBox [cbx]
        // Sortie : Aucune

        // ==================================================================================================================================================
        // FONCTION 34 : SetLocationOfMenus(int ilocX, int ilocY)
        // Description : Change la location des menus dans la page
        // Entrée 1 : int [Location X]
        // Entrée 2 : int [Location Y]
        // Sortie : Aucune

        // ==================================================================================================================================================
        // FONCTION 35 : atlanseGenererALL()
        // Description : Générer et afficher les valeurs ATL
        // Entrée 1 : Aucune
        // Sortie : Aucune

        // ==================================================================================================================================================
        // FONCTION 36 : AfficherRevenuTotal()
        // Description : Afficher le revenu total ATL
        // Entrée 1 : Aucune
        // Sortie : Aucune

        // ==================================================================================================================================================
        // FONCTION 37 : ClearLogs()
        // Description : Clear logs
        // Entrée 1 : Aucune
        // Sortie : Aucune

        // ==================================================================================================================================================
        // FONCTION 38 : MonthFromNumber(string number)
        // Description : Récupère le mois (Janvier février) par rapport au nombre donné (de 01 à 12)
        // Entrée 1 : string Numéro du mois (01 à 12)
        // Sortie : string [sMois (Janvier, Février, Mars....)]

        // ==================================================================================================================================================
        // FONCTION 38 : SaveAdminSettings(int iParam)
        // Description : Enregistre les paramètres (Administration)
        // Entrée 1 : int i | 1 = HTML settings | 2 = Divers | 3 = Mail | 4 = API
        // Sortie : Aucune

        // ==================================================================================================================================================
        // FONCTION 39 : SaveAdminSettings(int iParam) 
        // Description : Sauvegarde les paramètres de l'onglet ADMINISTRATION
        // Entrée 1 : int [param] | 1 = section html | 2 = section divers | 3 = section mail | 4 = section API
        // Sortie : Aucune

        // ==================================================================================================================================================
        // FONCTION 40 : refreshNbrSelectionne(int iParam) // iParam -> 1 = Rapports | 2 = Rapports générés
        // Description : 
        // Entrée 1 : 
        // Sortie : Aucune

        // ==================================================================================================================================================
        // FONCTION 41 : updateSettings(string sKey, string sValue)
        // Description : Met à jour les données d'un paramètre
        // Entrée 1 : string [Clé du paramètre]
        // Entrée 2 : string [Valeur à insérer]
        // Sortie : Aucune

        // ==================================================================================================================================================
        // FONCTION 42 : getSettings(string sKey)
        // Description : Recherche la donnée d'un paramètre
        // Entrée 1 : string [Clé du paramètre]
        // Sortie : string [Valeur du paramètre] 

        // ==================================================================================================================================================
        // FONCTION 43 : encrypt(string str)
        // Description : Encrypte un string
        // Entrée 1 : string [string décrypté]
        // Sortie : string [string crypté] 

        // ==================================================================================================================================================
        // FONCTION 44 : decrypt(string str)
        // Description : Decrypte un string
        // Entrée 1 : string [string crypté]
        // Sortie : string [string décrypté] 

        // FONCTION 1
        static int GetWeekNumberOfMonth(DateTime date)
        {
            // Valeurs & Traitement
            date = date.Date;
            DateTime firstMonthDay = new DateTime(date.Year, date.Month, 1);
            DateTime firstMonthMonday = firstMonthDay.AddDays((DayOfWeek.Monday + 7 - firstMonthDay.DayOfWeek) % 7);
            if (firstMonthMonday > date)
            {
                firstMonthDay = firstMonthDay.AddMonths(-1);
                firstMonthMonday = firstMonthDay.AddDays((DayOfWeek.Monday + 7 - firstMonthDay.DayOfWeek) % 7);
            }

            // Résultat
            int iResult = (date - firstMonthMonday).Days / 7 + 1;
            return iResult;
        }

        // FONCTION 2
        private void GenererRapport(string sPrenom, string sNom, string sAnnee, string sMois, string sProjet, string sDivisePar)
        {
            try
            {
                // Variables
                string[] AllUsers = tbxUsers.Text.Split(new string[] { "},{" }, StringSplitOptions.None);
                string sUserID = "";
                string sAllTasks = "";
                string DateDu = "N/A";
                string DateAu = "N/A";

                // Recherche de l'ID de l'employé
                for (int i = 0; i < AllUsers.Length; i++)
                {
                    // Variables
                    string sFoundFirstName = Encoding.UTF8.GetString(Encoding.Default.GetBytes(GetDataFrom("FIRST_NAME", AllUsers[i], "STRING")));
                    string sFoundLastName = Encoding.UTF8.GetString(Encoding.Default.GetBytes(GetDataFrom("LAST_NAME", AllUsers[i], "STRING")));

                    // ID
                    if (sFoundFirstName == sPrenom && sFoundLastName == sNom)
                    {
                        sUserID = GetDataFrom("USER_ID", AllUsers[i], "INT");
                        break;
                    } else
                    {
                        sUserID = "ID_NOT_FOUND";
                    }
                }

                // Recherche des projets
                if (sUserID != "ID_NOT_FOUND")
                {
                    using (var webClient = new WebClient())
                    {
                        // Variables
                        string dateFrom = FormatDate(GetDateFromTheMonthNumberAndYearNumber(Convert.ToInt32(sAnnee), Convert.ToInt32(sMois), 1), 2);
                        string dateTo = FormatDate(GetDateFromTheMonthNumberAndYearNumber(Convert.ToInt32(sAnnee), Convert.ToInt32(sMois), 2), 2);
                        string userID = sUserID;

                        // Appel vers l'API
                        ServicePointManager.Expect100Continue = true;
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                        //System.Diagnostics.Process.Start("https://api.aceproject.com/?fct=gettimereport&guid=" + lblGUID.Text + "&view=1&filtermyworkitems=False&FilterTimeCreatorUserId=" + userID + "&filtertimelevel=0&filterdatefrom=" + dateFrom + "&filterdateto=" + dateTo + "&countonly=False&isshowtotalsonly=False&asynccall=False&exportonscreencolumnsonly=True&exportview=0&exportremovehtmlonly=True&exportenablefilterxls=False&format=json");
                        String sAPItimereports = webClient.DownloadString("https://api.aceproject.com/?fct=gettimereport&guid=" + lblGUID.Text + "&view=1&filtermyworkitems=False&FilterTimeCreatorUserId=" + userID + "&filtertimelevel=0&filterdatefrom=" + dateFrom + "&filterdateto=" + dateTo + "&countonly=False&isshowtotalsonly=False&asynccall=False&exportonscreencolumnsonly=True&exportview=0&exportremovehtmlonly=True&exportenablefilterxls=False&format=json");

                        // Si il n'a pas de projets inscrits
                        if (sAPItimereports.Contains("\"results\":[]}"))
                        {
                            // aucun projets
                            MessageBox.Show("Aucune tâche n'a été trouvée pour " + sPrenom + " " + sNom);
                        }

                        // Récupération de toute les tâches du projet
                        string[] sAPItimereport = sAPItimereports.Split(new string[] { "},{" }, StringSplitOptions.None);
                        int oldIdOfTheWeek = 0;
                        int newIdOfTheWeek = 0;
                        string sNewTask = "";
                        for (int y = 0; y < sAPItimereport.Length; y++)
                        {
                            string sFoundProject = Encoding.UTF8.GetString(Encoding.Default.GetBytes(GetDataFrom("PROJECT_NAME", sAPItimereport[y], "STRING")));

                            if (sFoundProject == sProjet)
                            {
                                // Recherche du nombre d'heure passés sur la tâche
                                string heure = GetDataFrom("TOTAL", sAPItimereport[y], "INT");

                                // Format de la tâche
                                sNewTask = GetDataFrom("DATE_WORKED", sAPItimereport[y], "STRING") + "_" + heure + "_" + GetDataFrom("COMMENT", sAPItimereport[y], "STRING") + "*";
                                byte[] bytes = Encoding.Default.GetBytes(sNewTask);
                                sNewTask = Encoding.UTF8.GetString(bytes);

                                // Savoir si cette tâche est dans la même semaine que la précédente tâche
                                string sDateTask = GetDataFrom("DATE_WORKED", sAPItimereport[y], "STRING");
                                int iAnnee = Convert.ToInt32(sDateTask.Split('T')[0].Split('-')[0]);
                                int iMonth = Convert.ToInt32(sDateTask.Split('T')[0].Split('-')[1]);
                                int iDay = Convert.ToInt32(sDateTask.Split('T')[0].Split('-')[2]);
                                newIdOfTheWeek = GetWeekNumberOfMonth(new DateTime(iAnnee, iMonth, iDay)); //2020-08-12T
                                if (newIdOfTheWeek == oldIdOfTheWeek)
                                {
                                    // Fait partis de la même semaine que la précédente
                                    sAllTasks += sNewTask;
                                }
                                else
                                {
                                    // Ne fait pas partis de la même semaine que la précédente
                                    sAllTasks += "\n*****\n" + sNewTask + "*\n";
                                    oldIdOfTheWeek = newIdOfTheWeek;
                                }
                            }
                        }
                    }
                    // Mise en forme hebdomadaire
                    string[] sWeeks = sAllTasks.Split(new string[] { "\n*****\n" }, StringSplitOptions.None);
                    string sTaches = "";
                    for (int i = 0; i < sWeeks.Length; i++)
                    {
                        // Pour chaque semaine
                        if (sWeeks[i] != "")
                        {
                            // Rechercher la plus petite date et la plus grande
                            string[] sWeeksTasks = sWeeks[i].Split('*');
                            DateDu = sWeeks[i].Split('_')[0];
                            DateAu = sWeeksTasks[sWeeksTasks.Length - 2].Split('_')[0];
                            if (DateAu == "")
                            {
                                DateAu = DateDu;
                            }

                            // Calcul des heures totaux de la semaine & Récolte du commentaire
                            double dHourTotal = 0;
                            string comment = "";
                            for (int j = 0; j < sWeeksTasks.Length; j++)
                            {
                                if (sWeeksTasks[j] != "" && sWeeksTasks[j] != "\n")
                                {
                                    // Heures
                                    string sThisTaskHour = sWeeksTasks[j].Split('_')[1];
                                    double dThisTaskHour = Convert.ToDouble(sThisTaskHour);
                                    dHourTotal += dThisTaskHour;

                                    // Commentaire
                                    comment = sWeeksTasks[j].Split('_')[2];
                                }
                            }

                            // test
                            //debug//MessageBox.Show("Du : " + FormatDate(DateDu, 1) + "\nau : " + FormatDate(DateAu, 1) + "\n\nEt un total de " + dHourTotal + " heures");

                            // Créer un tableau avec toutes les données générées
                            // DateStartWeek;DateEndWeek;HeuresTotaux;commentaire<END>
                            // exemple : 12.08.2020;16.08.2020;3,5;ceci est un commentaire<END>
                            string row = "\n" + FormatDate(DateDu, 1) + ";;;" + FormatDate(DateAu, 1) + ";;;" + dHourTotal + ";;;" + comment + "<END>";
                            sTaches += row;
                        }
                    }
                    // Préparer l'HTML du document
                    string HTML = ToHtml(sTaches, sProjet, sDivisePar, sAnnee, sMois, sNom, sPrenom);

                    // Convertir l'HTML en PDF
                    sProjet = sProjet.Replace("-", "_").Replace(":", "_").Replace(" ", "_").Replace("____", "___").Replace("___", "__").Replace("__", "_");
                    string sNomDuFichierPdf = sPrenom + "-" + sNom + "_" + sAnnee + "_" + sMois + "_" + sProjet;
                    GenererPDF(HTML, getSettings("FilePathGeneration") + sNomDuFichierPdf + ".pdf");

                    // Ouvrir le rapport si l'option est true
                    if (getSettings("OpenRapportWhenGenerated") == "True")
                    {
                        System.Diagnostics.Process.Start(getSettings("FilePathGeneration") + sNomDuFichierPdf + ".pdf");
                    }

                    // LOGS
                    addLogs(1, 1, sPrenom, sNom, sProjet, sDivisePar, sMois, "", "");
                } else
                {
                    // Message d'erreur
                    addLogs(1, 0, sPrenom, sNom, sProjet, sDivisePar, sMois, "", "");
                    MessageBox.Show("L'identifiant du rapport n'a pas été trouvé.");
                }
            }
            catch (Exception e)
            {
                // Message d'erreur
                MessageBox.Show("Une erreur est survenue lors de la génération du rapport : \n" + e);

                // LOGS
                addLogs(1, 0, sPrenom, sNom, sProjet, sDivisePar, sMois, "", "");
            }
        }

        // FONCTION 3
        public void GenererPDF(string HTMLstring, string sCheminDuFichierDeDestination)
        {
            try
            {
                // Instantier l'objet
                HtmlToPdf converter = new HtmlToPdf();

                // Options
                converter.Options.PdfPageSize = SelectPdf.PdfPageSize.A4;
                converter.Options.PdfPageOrientation = SelectPdf.PdfPageOrientation.Landscape;
                converter.Options.MarginLeft = 10;
                converter.Options.MarginRight = 10;
                converter.Options.MarginTop = 20;
                converter.Options.MarginBottom = 20;

                // Créer le document
                PdfDocument doc = converter.ConvertHtmlString(HTMLstring);

                // Sauver
                doc.Save(sCheminDuFichierDeDestination);

                // Fermer le document
                doc.Close();
            }
            catch (Exception e)
            {
                MessageBox.Show("Une erreur est survenue lors de la génération du pdf\n" + e);
            }
        }

        // FONCTION 4
        private string ToHtml(string sAllTasks, string sNomDuProjet, string sDivisePar, string sAnnee, string sMois, string sNom, string sPrenom)
        {
            // -- Entrées exemple --
            // sAllTasks : 12.12.2020;14.12.2020;5,5;Ceci est un commentaire test;\n...
            // sNomDuProjet : VS DMBA - Valais
            // sDivisePar : 8.4
            // sNom : Thieulin
            // sPrenom : Caroline
            // sMois : 07
            // sAnnee : 2020

            // Variables
            string sLogoLink = getSettings("adminHTML_Logo");
            string sTitre = "Rapport d'activité de " + sPrenom + " " + sNom + " " + MonthFromNumber(sMois) + " " + sAnnee;

            // Créer le tableau avec les tâches
            string htmlTaches = CreateHtmlTableFromAllTasks(sAllTasks, sNomDuProjet, sPrenom, sNom);

            // Créer le tableau avec le nombre d'heure
            string[] sTaches = sAllTasks.Split(new string[] { "<END>" }, StringSplitOptions.None);
            double dHeureTotal = 0;
            double dHeureParJour = Convert.ToDouble(sDivisePar);
            for (int i = 0; i < sTaches.Length; i++)
            {
                if (sTaches[i] != "")
                {
                    string sHeure = sTaches[i].Split(new string[] { ";;;" }, StringSplitOptions.None)[2];
                    dHeureTotal += Convert.ToDouble(sHeure);
                }
            }
            string htmlHeures = CreateHtmlTableFromHours(dHeureTotal, dHeureParJour);
            string CSS = "<style>" + File.ReadAllText(getSettings("adminHTML_CSS")) + "</style>";

            // Document en HTML
            string HTML = "<!DOCTYPE html>" +

                    // Table
                    "<table class=\"tableGeneral\">" +

                        // Tête de la table
                        "<thead class=\"tableGeneralHEAD\">" +


                            // Ligne 1
                            "<tr>" +
                                // Colonne 1
                                "<th width=\"50px\"><img src=\"" + sLogoLink + "\"width=\"" + getSettings("adminHTML_WidthLogo") + "\"></th>" +

                                // Colonne 2
                                "<th><h2>" + sTitre + "</h2></th>" +

                                // Colonne 3
                                "<th><h3 class=\"signature\">Signature client:</h3></th>" +

                                // Colonne 4
                                "<th>" + htmlHeures + "</th>" +

                            "</tr>" +
                        "</thead>" +

                        // Corps de la table
                        "<tbody>" +

                            // Ligne 1
                            "<tr>" +

                                 // Colonne 1 (qui fait 4 colonne)
                                 "<td colspan = \"4\" width:\"100%\">" + htmlTaches + "</td>" +

                            "</tr>" +
                        "</tbody>" +
                    "</table>" +

                    // CSS
                    CSS
                    ;

            return HTML;
        }

        // FONCTION 5
        private string CreateHtmlTableFromHours(double dHeureTotal, double dHeureParJour)
        {
            // Variables
            string sNomLigne1 = "Total Heures";
            string sNomLigne2 = "Total Jours";
            string sHeureTotal = dHeureTotal.ToString("0.00");
            string sJours = (dHeureTotal / dHeureParJour).ToString("0.00");

            // Mise en forme du tableau
            string htmlTable =
                    "<table class=\"tableHeures\">" +
                        "<thead></thead><tbody>" +
                            "<tr><td class=\"black\" style=\"text-align: right;\">" + sNomLigne1 + "</td> <td class=\"black\" style=\"weight:10px;\">" + sHeureTotal + "</td></tr>" + // 1ère ligne
                            "<tr><td class=\"black\" style=\"text-align: right;\">" + sNomLigne2 + "</td> <td class=\"black\" style=\"weight:10px;\">" + sJours + "</td></tr>" + // 2ème ligne
                        "</tbody>" +
                    "</table>";
            return htmlTable;
        }

        // FONCTION 6
        private string CreateHtmlTableFromAllTasks(string sAllTasks, string sNomDuProjet, string sPrenom, string sNom)
        {
            // Variables
            string sNomColonne1 = getSettings("adminHTML_Colonne1nom");
            string sNomColonne2 = getSettings("adminHTML_Colonne2nom");
            string sNomColonne3 = getSettings("adminHTML_Colonne3nom");
            string sNomColonne4 = getSettings("adminHTML_Colonne4nom");
            string sNomColonne5 = getSettings("adminHTML_Colonne5nom");

            // Largeur des colonnes
            string sWidthColonne1 = getSettings("adminHTML_Colonne1largeur");
            string sWidthColonne2 = getSettings("adminHTML_Colonne2largeur");
            string sWidthColonne3 = getSettings("adminHTML_Colonne3largeur");
            string sWidthColonne4 = getSettings("adminHTML_Colonne4largeur");
            string sWidthColonne5 = getSettings("adminHTML_Colonne5largeur");

            // Données
            string[] sTaches = sAllTasks.Split(new string[] { "<END>" }, StringSplitOptions.None);
            string htmlTaches = "";
            for (int i = 0; i < sTaches.Length; i++)
            {
                if (sTaches[i] != "")
                {
                    string sDateDu = sTaches[i].Split(new string[] { ";;;" }, StringSplitOptions.None)[0];
                    string sDateAu = sTaches[i].Split(new string[] { ";;;" }, StringSplitOptions.None)[1];
                    string sProjet = sNomDuProjet;
                    string sPrenomNom = sPrenom + " " + sNom;
                    string sHeure = Convert.ToDouble(sTaches[i].Split(new string[] { ";;;" }, StringSplitOptions.None)[2]).ToString("0.00");
                    string sCommentaire = sTaches[i].Split(new string[] { ";;;" }, StringSplitOptions.None)[3];
                    htmlTaches += "<tr>" +
                        "<td class=\"black\">" + FormatDate(sDateDu, 3) + " - " + FormatDate(sDateAu, 3) + "</td>" +
                        "<td class=\"black\">" + sProjet + "</td>" +
                        "<td class=\"black\">" + sPrenomNom + "</td>" +
                        "<td class=\"black\" style=\"text-align: right;\">" + sHeure + "</td>" +
                        "<td class=\"black\">" + sCommentaire + "</td>" +
                        "</tr>";
                }
            }

            // Mise en forme du tableau
            string htmlTable =
                    "<table class=\"tableTaches\" style=\"border-collapse: collapse;\">" +
                        "<thead><tr>" +
                        "<th width=" + sWidthColonne1 + " class=\"headerTableBlue\">" + sNomColonne1 + "</th>" +
                        "<th width=" + sWidthColonne2 + " class=\"headerTableBlue\">" + sNomColonne2 + "</th>" +
                        "<th width=" + sWidthColonne3 + " class=\"headerTableBlue\">" + sNomColonne3 + "</th>" +
                        "<th width=" + sWidthColonne4 + " class=\"headerTableBlue\">" + sNomColonne4 + "</th>" +
                        "<th width=" + sWidthColonne5 + " class=\"headerTableBlue\">" + sNomColonne5 + "</th>" +
                        "</tr></thead>" +
                        "<tbody>" + htmlTaches +
                        "</tbody>" +
                    "</table>";
            htmlTable = htmlTable.Replace(@"\r\n", "<br>"); // Remplacer les retours à la ligne en format html

            return htmlTable;
        }

        // FONCTION 7
        private void SupprimerConfig(string sSelectedConfig)
        {
            // Récupération et formation des nouvelles données
            sSelectedConfig = (sSelectedConfig).Replace(" | ", ";");
            string OldUsersData = File.ReadAllText(getSettings("grConf"));
            string NewUserData = OldUsersData.Replace(sSelectedConfig, "");

            // Enregistrement des nouvelles données
            File.WriteAllText(getSettings("grConf"), NewUserData);

            // Refresh listbox 5
            RefreshListbox(5);
            RefreshListbox(4);
        }

        // FONCTION 8
        private void AjouterConfig(string sEmploye, string sProjet, string sDivisePar)
        {
            // Récupération et formation des nouvelles données
            string OldUsersData = File.ReadAllText(getSettings("grConf"));
            string NewUserData = OldUsersData + "\n" + sEmploye + ";" + sProjet + ";" + sDivisePar + ";";

            // Enregistrement des nouvelles données
            if (!OldUsersData.Contains(sEmploye + ";" + sProjet + ";" + sDivisePar + ";"))
            {
                // Ecrire
                File.WriteAllText(getSettings("grConf"), NewUserData);

                // Refresh listbox 5
                RefreshListbox(5);
            }
            else
            {
                MessageBox.Show("Doublons détecté");
            }
        }

        // FONCTION 9
        private string FormatDate(string sDate, int iParam)
        {
            try
            {
                if (iParam == 1)
                {
                    string sDateReverse = sDate.Split('T')[0];
                    string[] sDateSplitted = sDateReverse.Split('-');
                    string sNewDate = sDateSplitted[2] + "." + sDateSplitted[1] + "." + sDateSplitted[0];

                    return sNewDate;
                }
                else
                {
                    if (iParam == 2)
                    {
                        string[] sDateSplitted = sDate.Split('.');
                        string sNewDate = sDateSplitted[2] + "." + sDateSplitted[1] + "." + sDateSplitted[0];

                        return sNewDate;
                    }
                    else
                    {
                        if (iParam == 3)
                        {
                            string sNewDate = sDate.Split('.')[0] + "." + sDate.Split('.')[1];
                            return sNewDate;
                        } else
                        {
                            MessageBox.Show("La fonction \"FormatDate\" a mal été appelée");
                            return "Error";
                        }
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Une erreur est survenue lors du formatage de la date \"" + sDate + "\"\n" + e);
                return "Error";
            }
        }

        // FONCTION 10
        private string GetDataFrom(string sNomAttribut, string sJson, string sType)
        {
            try
            {
                if (sType == "STRING")
                {
                    sNomAttribut = "\"" + sNomAttribut + "\":\"";
                    string X = sJson.Split(new string[] { sNomAttribut }, StringSplitOptions.None)[1];
                    string Y = X.Split(new string[] { "\"" }, StringSplitOptions.None)[0];
                    string sResultat = Y.Replace("\"", "");
                    return sResultat;
                }
                else if (sType == "INT")
                {
                    sNomAttribut = "\"" + sNomAttribut + "\":";
                    string X = sJson.Split(new string[] { sNomAttribut }, StringSplitOptions.None)[1];
                    string Y = X.Split(new string[] { "\"" }, StringSplitOptions.None)[0];
                    string sResultat = Y.Replace("\"", "").Replace(",", "").Replace("}]}\r\n", "");
                    return sResultat;
                }
                else {
                    return "TYPE NON SPÉCIFIÉ";
                }
            }
            catch
            {
                return "N/A";
            }
        }

        // FONCTION 11
        public void RefreshListbox(int iParameter) // 1 = ALL | 2 = Only projets (config) | 3 = Only users (Config) | 4 = Only configurations | 5 = Only rapports | 6 = Only documents générés
        {
            // Variables
            string sEmploye;
            string[] AllUsers;
            string k = "";
            string sProjet = "----------------";

            // Refresh la liste des projets
            if (iParameter == 1 || iParameter == 2)
            {
                cbxProjets.Items.Clear();
                try
                {
                    AllUsers = tbxUsers.Text.Split(new string[] { "},{" }, StringSplitOptions.None);
                    if (Convert.ToString(cbxEmploye.SelectedItem) != "")
                    {
                        for (int i = 0; i < AllUsers.Length; i++)
                        {
                            progressbarProjet.Value = 10; // PROGRESS
                            Application.DoEvents(); // PROGRESS
                            // Recherche de l'ID de la personne sélectionné
                            string sUserID = GetDataFrom("USER_ID", AllUsers[i], "INT");

                            // Recherche du prénom+nom de la personne dans l'API
                            string sPrenomEtNomDeLutilisateurActuel = GetDataFrom("FIRST_NAME", AllUsers[i], "STRING") + " " + GetDataFrom("LAST_NAME", AllUsers[i], "STRING");
                            byte[] bytes = Encoding.Default.GetBytes(sPrenomEtNomDeLutilisateurActuel);
                            sPrenomEtNomDeLutilisateurActuel = Encoding.UTF8.GetString(bytes);

                            // Si le nom de l'API est identique à celui qui a été séléctionné
                            if (sPrenomEtNomDeLutilisateurActuel == Convert.ToString(cbxEmploye.SelectedItem))
                            {
                                // Afficher les projets
                                using (var webClient = new WebClient())
                                {
                                    progressbarProjet.Value = 40; // PROGRESS
                                    Application.DoEvents(); // PROGRESS
                                                            // Appel vers l'API
                                    ServicePointManager.Expect100Continue = true;
                                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                                    string dateFrom = "2000.01.01"; //dtpDateDu.Value.ToString("yyyy.MM.dd");
                                    string dateTo = DateTime.Today.ToString("yyyy.MM.dd"); // "2100.25.12"; // dtpDateAu.Value.ToString("yyyy.MM.dd");
                                    string userID = sUserID;
                                    String sAPItimereports = webClient.DownloadString("https://api.aceproject.com/?fct=gettimereport&guid=" + lblGUID.Text + "&view=1&filtermyworkitems=False&FilterTimeCreatorUserId=" + userID + "&filtertimelevel=0&filterdatefrom=" + dateFrom + "&filterdateto=" + dateTo + "&countonly=False&isshowtotalsonly=False&asynccall=False&exportonscreencolumnsonly=True&exportview=0&exportremovehtmlonly=True&exportenablefilterxls=False&format=json");

                                    // Si il n'a pas de projets inscrits
                                    if (sAPItimereports.Contains("\"results\":[]}"))
                                    {
                                        MessageBox.Show("Aucun projets n'a été trouvé pour " + sPrenomEtNomDeLutilisateurActuel + ".");
                                        progressbarProjet.Value = 100; // PROGRESS
                                        Application.DoEvents(); // PROGRESS
                                        break;
                                    }

                                    // Affichage des projets
                                    progressbarProjet.Value = 90; // PROGRESS
                                    Application.DoEvents(); // PROGRESS
                                    string[] sAPItimereport = sAPItimereports.Split(new string[] { "},{" }, StringSplitOptions.None);
                                    for (int y = 0; y < sAPItimereport.Length; y++)
                                    {
                                        if (progressbarProjet.Value >= 90)
                                        {
                                            progressbarProjet.Value = 96; // PROGRESS
                                            Application.DoEvents(); // PROGRESS
                                        } else
                                        {
                                            progressbarProjet.Value += 10; // PROGRESS
                                            Application.DoEvents(); // PROGRESS
                                        }
                                        sProjet = GetDataFrom("PROJECT_NAME", sAPItimereport[y], "STRING");

                                        if (!k.Contains(sProjet))
                                        {
                                            byte[] bytes2 = Encoding.Default.GetBytes(sProjet);
                                            string sProjetutf8 = Encoding.UTF8.GetString(bytes2);
                                            cbxProjets.Items.Add(sProjetutf8);
                                            k += sProjet;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    MessageBox.Show("Une erreur s'est produite lors du chargement de la liste des projets de l'employé\n" + e);
                }
            }
            // Refresh la liste des employés
            if (iParameter == 1 || iParameter == 3)
            {
                cbxEmploye.Items.Clear();
                try
                {
                    AllUsers = tbxUsers.Text.Split(new string[] { "},{" }, StringSplitOptions.None);
                    for (int i = 0; i < AllUsers.Length; i++)
                    {
                        // Variable à afficher + encoder en utf8
                        sEmploye = GetDataFrom("FIRST_NAME", AllUsers[i], "STRING") + " " + GetDataFrom("LAST_NAME", AllUsers[i], "STRING");
                        byte[] bytes = Encoding.Default.GetBytes(sEmploye);
                        sEmploye = Encoding.UTF8.GetString(bytes);

                        // Afficher l'employé
                        cbxEmploye.Items.Add(sEmploye);
                    }
                }
                catch (Exception e)
                {
                    MessageBox.Show("Une erreur s'est produite lors du chargement de la liste d'employé (configs)\n" + e);
                }
            }
            // Refresh la liste des configs
            if (iParameter == 1 || iParameter == 4)
            {
                lbxConfig.Items.Clear();
                try
                {
                    string[] sAllConfigs = File.ReadAllText(getSettings("grConf")).Split('\n');
                    for (int i = 0; i < sAllConfigs.Length; i++)
                    {
                        if (sAllConfigs[i] != "")
                        {
                            /*
                            ListViewItem item = new ListViewItem(sAllConfigs[i].Split(';')[0]);
                            item.SubItems.Add(sAllConfigs[i].Split(';')[1]);
                            item.SubItems.Add(sAllConfigs[i].Split(';')[2]);

                            lbxConfig.Items.Add(item);
                            */
                            lbxConfig.Items.Add(sAllConfigs[i].Replace(";", " | "));
                        }
                    }
                }
                catch (Exception e)
                {
                    MessageBox.Show("Une erreur s'est produite lors du chargement de la liste des configurations. Fichier manquant ?\n" + e);
                }
            }
            // Refresh la liste des rapports potentiels
            if (iParameter == 1 || iParameter == 5)
            {
                try
                {
                    // Clear
                    lbxRapports.Items.Clear();

                    // Recherche de données
                    string DateDu = GetDateFromTheMonthIndex(lbxMois.SelectedIndex, 1);
                    string DateAu = GetDateFromTheMonthIndex(lbxMois.SelectedIndex, 2);
                    DateDu = FormatDate(DateDu, 2);
                    DateAu = FormatDate(DateAu, 2);
                    string annee = GetAnneeFromTheMonth(lbxMois.SelectedIndex);
                    string mois = DateDu.Split('.')[1];
                    string sDivisePar = "8.4";

                    // Traitement
                    AllUsers = tbxUsers.Text.Split(new string[] { "},{" }, StringSplitOptions.None);
                    for (int i = 0; i < AllUsers.Length; i++)
                    {

                        // Recherche de l'ID de la personne sélectionné
                        string sUserID = GetDataFrom("USER_ID", AllUsers[i], "INT");

                        // Recherche du prénom+nom de la personne dans l'API
                        string sPrenomEtNomDeLutilisateurActuel = GetDataFrom("FIRST_NAME", AllUsers[i], "STRING") + " " + GetDataFrom("LAST_NAME", AllUsers[i], "STRING");
                        byte[] bytes = Encoding.Default.GetBytes(sPrenomEtNomDeLutilisateurActuel);
                        sPrenomEtNomDeLutilisateurActuel = Encoding.UTF8.GetString(bytes);

                        // Vérifier le nom dans la config
                        string[] sAllConfigs = File.ReadAllText(getSettings("grConf")).Split('\n');
                        Boolean isNameInTheConfig = false;
                        Boolean isProjetInTheConfig = false;
                        for (int j = 0; j < sAllConfigs.Length; j++)
                        {
                            if (sAllConfigs[j].Contains(sPrenomEtNomDeLutilisateurActuel))
                            {
                                isNameInTheConfig = true;
                                break;
                            }
                        }
                        // Afficher les projets
                        if (isNameInTheConfig)
                        {
                            using (var webClient = new WebClient())
                            {
                                // Appel vers l'API
                                ServicePointManager.Expect100Continue = true;
                                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                                string dateFrom = DateDu;
                                string dateTo = DateAu;
                                string userID = sUserID;
                                String sAPItimereports = webClient.DownloadString("https://api.aceproject.com/?fct=gettimereport&guid=" + lblGUID.Text + "&view=1&filtermyworkitems=False&FilterTimeCreatorUserId=" + userID + "&filtertimelevel=0&filterdatefrom=" + dateFrom + "&filterdateto=" + dateTo + "&countonly=False&isshowtotalsonly=False&asynccall=False&exportonscreencolumnsonly=True&exportview=0&exportremovehtmlonly=True&exportenablefilterxls=False&format=json");

                                // Si il y a un projet inscrit
                                if (!sAPItimereports.Contains("\"results\":[]}"))
                                {
                                    // Affichage des projets
                                    string[] sAPItimereport = sAPItimereports.Split(new string[] { "},{" }, StringSplitOptions.None);
                                    for (int y = 0; y < sAPItimereport.Length; y++)
                                    {
                                        // sProjet
                                        sProjet = GetDataFrom("PROJECT_NAME", sAPItimereport[y], "STRING");
                                        string sProjetutf8 = Encoding.UTF8.GetString(Encoding.Default.GetBytes(sProjet));

                                        // Vérif si le projet est dans les configs
                                        for (int j = 0; j < sAllConfigs.Length; j++)
                                        {
                                            if (sAllConfigs[j].Contains(sPrenomEtNomDeLutilisateurActuel) && sAllConfigs[j].Contains(sProjetutf8))
                                            {
                                                // Valeurs
                                                isProjetInTheConfig = true;
                                                sDivisePar = sAllConfigs[j].Split(';')[2];

                                                // sRapport
                                                string sRapport = GetDataFrom("FIRST_NAME", AllUsers[i], "STRING") + "-" + GetDataFrom("LAST_NAME", AllUsers[i], "STRING") + "_" + annee + "_" + mois + "_" + GetDataFrom("PROJECT_NAME", sAPItimereport[y], "STRING") + ".pdf" + " / " + sDivisePar;
                                                sRapport = Encoding.UTF8.GetString(Encoding.Default.GetBytes(sRapport));

                                                // Ajouter le projet
                                                if (isNameInTheConfig && isProjetInTheConfig)
                                                {
                                                    sProjet = Encoding.UTF8.GetString(Encoding.Default.GetBytes(sProjet));
                                                    if (!k.Contains(sProjet + sDivisePar))
                                                    {
                                                        // Toutes les logs
                                                        string[] userLogs = getAllLogsFrom(sPrenomEtNomDeLutilisateurActuel);

                                                        // Est-ce qu'il a été généré
                                                        string sLastGeneration = lastGegeneration(mois + "_" + sProjet.Replace("-", "_").Replace(":", "_").Replace(" ", "_").Replace("____", "___").Replace("___", "__").Replace("__", "_"), userLogs);

                                                        // Est-ce qu'il a été envoyé
                                                        string sLastSend = lastSend(mois + "_" + sProjet.Replace("-", "_").Replace(":", "_").Replace(" ", "_").Replace("____", "___").Replace("___", "__").Replace("__", "_"), userLogs);

                                                        // Si il a été généré ou envoyer, alors mettre en forme
                                                        if (sLastGeneration.Contains("YES") && sLastSend.Contains("NO"))
                                                        {
                                                            lbxRapports.Items.Add(sRapport + " || Généré le " + sLastGeneration.Split(';')[1]).BackColor = Color.FromArgb(255, 255, 204);
                                                        } else if (sLastSend.Contains("YES"))
                                                        {
                                                            lbxRapports.Items.Add(sRapport + " || Généré le " + sLastGeneration.Split(';')[1]).BackColor = Color.FromArgb(255, 255, 204);
                                                            //lbxRapports.Items.Add(sRapport + " || Envoyé le " + sLastSend.Split(';')[1]).BackColor = Color.FromArgb(208, 255, 205); // Changement 1.2.7
                                                        } else
                                                        {
                                                            lbxRapports.Items.Add(sRapport);
                                                        }

                                                        // Pour la condition
                                                        k += sProjet + sDivisePar;
                                                    }
                                                }
                                            } else
                                            {
                                                isProjetInTheConfig = false;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    lblprogressbar.Text = getSettings("labelProgressBarDone"); // PROGRESS
                    Application.DoEvents(); // PROGRESS

                } catch (Exception e)
                {
                    MessageBox.Show("Une erreur est survenue lors du rafraichissement des rapports potentiels\nErreur de login ?\n\n" + e);
                    this.Close();
                }
            }
            // Refresh la liste des documents générés
            if (iParameter == 1 || iParameter == 6)
            {
                // clear
                lbxDocumentsGeneres.Items.Clear();

                // Recherche de tous les fichiers dans le dossier
                string filepath = getSettings("FilePathGeneration");
                DirectoryInfo d = new DirectoryInfo(filepath);

                foreach (var file in d.GetFiles("*.pdf"))
                {
                    // Variables
                    string sPrenomEtNomDeLutilisateurActuel = file.Name.Split('-')[0] + " " + file.Name.Split('-')[1].Split('_')[0];
                    string sMois = file.Name.Split('_')[2];
                    string sProjetGenere = file.Name
                                            .Split(new string[] { file.Name.Split('_')[0] + "_" + file.Name.Split('_')[1] + "_" + file.Name.Split('_')[2] + "_" }, StringSplitOptions.None)[1]
                                            .Split(new string[] { ".pdf" }, StringSplitOptions.None)[0];

                    string[] userLogs = getAllLogsFrom(sPrenomEtNomDeLutilisateurActuel);

                    // Est-ce qu'il a été généré
                    string sLastGeneration = lastGegeneration(sMois + "_" + sProjetGenere.Replace("-", "_").Replace(":", "_").Replace(" ", "_").Replace("____", "___").Replace("___", "__").Replace("__", "_"), userLogs);

                    // Est-ce qu'il a été envoyé
                    string sLastSend = lastSend(sMois + "_" + sProjetGenere, userLogs);

                    // Si il a été généré ou envoyer, alors mettre en forme
                    if (sLastGeneration.Contains("YES") && sLastSend.Contains("NO"))
                    {
                        lbxDocumentsGeneres.Items.Add(file.Name + " || Généré le " + sLastGeneration.Split(';')[1]).BackColor = Color.FromArgb(255, 255, 204);
                    }
                    else if (sLastSend.Contains("YES"))
                    {
                        lbxDocumentsGeneres.Items.Add(file.Name + " || Généré le " + sLastGeneration.Split(';')[1]).BackColor = Color.FromArgb(255, 255, 204);
                        //lbxDocumentsGeneres.Items.Add(sRapport + " || Envoyé le " + sLastSend.Split(';')[1]).BackColor = Color.FromArgb(208, 255, 205); // Changement 1.2.7
                    }
                    else
                    {
                        lbxDocumentsGeneres.Items.Add(file.Name);
                    }
                }
            }
            // Refresh la liste des employés atlanse
            if (iParameter == 1 || iParameter == 7)
            {
                // Valeurs
                AllUsers = tbxUsers.Text.Split(new string[] { "},{" }, StringSplitOptions.None);

                // Clear
                cbxEmploye1.Items.Clear();
                cbxEmploye2.Items.Clear();
                cbxEmploye3.Items.Clear();
                cbxEmploye4.Items.Clear();
                cbxEmploye5.Items.Clear();
                cbxEmploye6.Items.Clear();
                cbxEmploye7.Items.Clear();
                cbxEmploye8.Items.Clear();
                tbxNbrJours1.Text = "";
                tbxNbrJours2.Text = "";
                tbxNbrJours3.Text = "";
                tbxNbrJours4.Text = "";
                tbxNbrJours5.Text = "";
                tbxNbrJours6.Text = "";
                tbxNbrJours7.Text = "";
                tbxNbrJours8.Text = "";
                tbxRevenue1.Text = "";
                tbxRevenue2.Text = "";
                tbxRevenue3.Text = "";
                tbxRevenue4.Text = "";
                tbxRevenue5.Text = "";
                tbxRevenue6.Text = "";
                tbxRevenue7.Text = "";
                tbxRevenue8.Text = "";

                // Ajoute tous les utilisateurs venant du groupe "Nearshore" (Donc de Atlanse)
                cbxEmploye1.Items.Add("");
                cbxEmploye2.Items.Add("");
                cbxEmploye3.Items.Add("");
                cbxEmploye4.Items.Add("");
                cbxEmploye5.Items.Add("");
                cbxEmploye6.Items.Add("");
                cbxEmploye7.Items.Add("");
                cbxEmploye8.Items.Add("");
                for (int i = 0; i < AllUsers.Length; i++)
                {
                    if (GetDataFrom("USER_GROUP_NAME", AllUsers[i], "STRING") == "Nearshore")
                    {
                        cbxEmploye1.Items.Add(GetDataFrom("FIRST_NAME", AllUsers[i], "STRING") + " " + GetDataFrom("LAST_NAME", AllUsers[i], "STRING"));
                        cbxEmploye2.Items.Add(GetDataFrom("FIRST_NAME", AllUsers[i], "STRING") + " " + GetDataFrom("LAST_NAME", AllUsers[i], "STRING"));
                        cbxEmploye3.Items.Add(GetDataFrom("FIRST_NAME", AllUsers[i], "STRING") + " " + GetDataFrom("LAST_NAME", AllUsers[i], "STRING"));
                        cbxEmploye4.Items.Add(GetDataFrom("FIRST_NAME", AllUsers[i], "STRING") + " " + GetDataFrom("LAST_NAME", AllUsers[i], "STRING"));
                        cbxEmploye5.Items.Add(GetDataFrom("FIRST_NAME", AllUsers[i], "STRING") + " " + GetDataFrom("LAST_NAME", AllUsers[i], "STRING"));
                        cbxEmploye6.Items.Add(GetDataFrom("FIRST_NAME", AllUsers[i], "STRING") + " " + GetDataFrom("LAST_NAME", AllUsers[i], "STRING"));
                        cbxEmploye7.Items.Add(GetDataFrom("FIRST_NAME", AllUsers[i], "STRING") + " " + GetDataFrom("LAST_NAME", AllUsers[i], "STRING"));
                        cbxEmploye8.Items.Add(GetDataFrom("FIRST_NAME", AllUsers[i], "STRING") + " " + GetDataFrom("LAST_NAME", AllUsers[i], "STRING"));
                    }
                }
            }
        }

        // FONCTION 12
        private string lastSend(string sNomDuRapportGenere, string[] sLogsOfUser) // Retour : NO or YES;DATE
        {
            string isSent = "NO";
            for (int i = 0; i < sLogsOfUser.Length; i++)
            {
                if (sLogsOfUser[i].Contains("Envoi"))
                {
                    if (sLogsOfUser[i].Contains(sNomDuRapportGenere))
                    {
                        string sDate = sLogsOfUser[i].Split(']')[0].Replace("[", "").Replace("-", "à");
                        isSent = "YES;" + sDate;
                    }
                }
            }
            return isSent;
        }

        // FONCTION 13
        private string lastGegeneration(string sNomDuRapportGenere, string[] sLogsOfUser) // Retour : NO or YES;DATE
        {
            string isGenerated = "NO";
            for (int i = 0; i < sLogsOfUser.Length; i++)
            {
                if (sLogsOfUser[i].Contains("Génération"))
                {
                    if (sLogsOfUser[i].Contains(sNomDuRapportGenere))
                    {
                        string sDate = sLogsOfUser[i].Split(']')[0].Replace("[", "").Replace("-", "à");
                        isGenerated = "YES;" + sDate;
                    }
                }
            }
            return isGenerated;
        }

        // FONCTION 14
        private string[] getAllLogsFrom(string sPrenomNom)
        {
            // Variables
            string sLogsFrom = "";
            string[] sLogs = File.ReadAllText(getSettings("grLogs")).Split('\n');

            // Traitement
            for (int i = 0; i < sLogs.Length; i++)
            {
                if (sLogs[i].Contains(sPrenomNom))
                {
                    sLogsFrom += sLogs[i] + "\n";
                }
            }

            // Retour
            return sLogsFrom.Split('\n');
        }

        // FONCTION 15
        private string GetDateFromTheMonthNumberAndYearNumber(int iAnnee, int iMonth, int iParam) // 1 = Date de début du mois | 2 = Date de fin du mois 
        {
            try
            {
                // Variables
                string sResult = "N/A";

                if (iParam == 1)
                {
                    // Recherche de la date de début
                    sResult = new DateTime(iAnnee, iMonth, 1).ToString("dd.MM.yyyy");
                }
                else
                {
                    // Recherche de la date de fin
                    sResult = new DateTime(iAnnee, iMonth, 1).AddMonths(1).AddDays(-1).ToString("dd.MM.yyyy");
                }
                return sResult;
            } catch (Exception e)
            {
                MessageBox.Show("Une erreur est survenue lors de la recherche du mois (Nombre)\n" + e);
                return "ERROR";
            }
        }

        // FONCTION 16
        private string GetDateFromTheMonthIndex(int iIndex, int iParam) // 1 = Date de début du mois | 2 = Date de fin du mois
        {
            try
            {
                // Variables
                string sResult = "N/A";
                DateTime thisDay = DateTime.Today;

                if (iParam == 1)
                {
                    // Recherche de la date de début
                    if (iIndex == 0)
                    {
                        // Mois actuel
                        sResult = new DateTime(thisDay.Year, thisDay.Month, 1).ToString("dd.MM.yyyy");
                    }
                    if (iIndex == 1)
                    {
                        // Mois actuel - 1
                        sResult = new DateTime(thisDay.Year, thisDay.AddMonths(-1).Month, 1).ToString("dd.MM.yyyy");
                    }
                    if (iIndex == 2)
                    {
                        // Mois actuel - 2
                        sResult = new DateTime(thisDay.Year, thisDay.AddMonths(-2).Month, 1).ToString("dd.MM.yyyy");
                    }
                }
                else
                {
                    // Recherche de la date de fin
                    if (iIndex == 0)
                    {
                        // Mois actuel
                        sResult = new DateTime(thisDay.Year, thisDay.Month, 1).AddMonths(1).AddDays(-1).ToString("dd.MM.yyyy");
                    }
                    if (iIndex == 1)
                    {
                        // Mois actuel - 1
                        sResult = new DateTime(thisDay.Year, thisDay.AddMonths(-1).Month, 1).AddMonths(1).AddDays(-1).ToString("dd.MM.yyyy");
                    }
                    if (iIndex == 2)
                    {
                        // Mois actuel - 2
                        sResult = new DateTime(thisDay.Year, thisDay.AddMonths(-2).Month, 1).AddMonths(1).AddDays(-1).ToString("dd.MM.yyyy");
                    }
                }
                // Retour
                return sResult;
            } catch (Exception e)
            {
                MessageBox.Show("Une erreur est survenue lors de la recherche du mois (Index)\n" + e);

                return "ERROR";
            }
        }

        // FONCTION 17
        private string GetAnneeFromTheMonth(int iIndex)
        {
            // Variables
            string sResult = "";
            DateTime thisDay = DateTime.Today;

            // Recherche de la date
            if (iIndex == 0)
            {
                // Mois actuel
                sResult = new DateTime(thisDay.Year, thisDay.Month, 1).ToString("yyyy");
            }
            if (iIndex == 1)
            {
                // Mois actuel - 1
                sResult = new DateTime(thisDay.Year, thisDay.AddMonths(-1).Month, 1).ToString("yyyy");
            }
            if (iIndex == 2)
            {
                // Mois actuel - 2
                sResult = new DateTime(thisDay.Year, thisDay.AddMonths(-2).Month, 1).ToString("yyyy");
            }

            // Retour
            return sResult;
        }

        // FONCTION 18
        private void SupprimerRapportGenere(string sNomDuRapportGenere)
        {
            try
            {
                // Variables
                string sFilePath = getSettings("FilePathGeneration") + @"\" + sNomDuRapportGenere;

                // Validation pop-up
                string sTitre = "Etes vous sûr ?";
                string sQuestion = "Voulez-vous vraiment supprimer \"" + sNomDuRapportGenere + "\"" + " ?";
                DialogResult dialogResult = MessageBox.Show(sQuestion, sTitre, MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    // Supprimer
                    File.Delete(sFilePath);
                    addLogs(3, 1, "", "", "", "", "", "", sNomDuRapportGenere);
                }
                else if (dialogResult == DialogResult.No)
                {
                    // Annuler
                }
            } catch (Exception e)
            {
                addLogs(3, 0, "", "", "", "", "", "", sNomDuRapportGenere);
                MessageBox.Show("Une erreur est survenue lors de la suppression du rapport\n" + e);
            }
        }

        // FONCTION 19
        public void SelectMenu(int iParameter) // 1 = Accueil | 2 = Configurer les rapports | 3 = Configurer les envois | 4 Configurer les chemins d'accès
        {
            // Variables
            Color cActivee = Color.FromArgb(63, 63, 110);
            Color cDesactivee = Color.FromArgb(63, 63, 70);
            Font fActivee = new Font(btnMenu2.Font, FontStyle.Bold);
            Font fDesactivee = new Font(btnMenu2.Font, FontStyle.Regular);

            // Tout désactiver
            btnMenu1.BackColor = cDesactivee;
            btnMenu2.BackColor = cDesactivee;
            btnMenu3.BackColor = cDesactivee;
            btnMenu4.BackColor = cDesactivee;
            btnMenu5.BackColor = cDesactivee;
            btnMenu6.BackColor = cDesactivee;
            btnMenu7.BackColor = cDesactivee;
            btnMenu1.Font = fDesactivee;
            btnMenu2.Font = fDesactivee;
            btnMenu3.Font = fDesactivee;
            btnMenu4.Font = fDesactivee;
            btnMenu5.Font = fDesactivee;
            btnMenu6.Font = fDesactivee;
            btnMenu7.Font = fDesactivee;

            // Activer le bon menu
            if (iParameter == 1)
            {
                // Menu 1
                btnMenu1.BackColor = cActivee;
                btnMenu1.Font = fActivee;

                // Afficher la page
                menu1.Visible = true;
                menu2.Visible = false;
                menu3.Visible = false;
                menu4.Visible = false;
                menu5.Visible = false;
                menu6.Visible = false;
                menu7.Visible = false;
            }
            if (iParameter == 2)
            {
                // Menu 2
                btnMenu2.BackColor = cActivee;
                btnMenu2.Font = fActivee;

                // Afficher la page
                menu1.Visible = false;
                menu2.Visible = true;
                menu3.Visible = false;
                menu4.Visible = false;
                menu5.Visible = false;
                menu6.Visible = false;
                menu7.Visible = false;
            }
            if (iParameter == 3)
            {
                // Menu 3
                btnMenu3.BackColor = cActivee;
                btnMenu3.Font = fActivee;

                // Afficher la page
                menu1.Visible = false;
                menu2.Visible = false;
                menu3.Visible = true;
                menu4.Visible = false;
                menu5.Visible = false;
                menu6.Visible = false;
                menu7.Visible = false;
            }
            if (iParameter == 4)
            {
                // Menu 4
                btnMenu4.BackColor = cActivee;
                btnMenu4.Font = fActivee;

                // Afficher la page
                menu1.Visible = false;
                menu2.Visible = false;
                menu3.Visible = false;
                menu4.Visible = true;
                menu5.Visible = false;
                menu6.Visible = false;
                menu7.Visible = false;
            }
            if (iParameter == 5)
            {
                // Menu 5
                btnMenu5.BackColor = cActivee;
                btnMenu5.Font = fActivee;

                // Afficher la page
                menu1.Visible = false;
                menu2.Visible = false;
                menu3.Visible = false;
                menu4.Visible = false;
                menu5.Visible = true;
                menu6.Visible = false;
                menu7.Visible = false;

                // Afficher logs
                Color cError = Color.FromArgb(255, 230, 230);
                Color cInfo = Color.FromArgb(233, 255, 229);
                string[] sLogs = File.ReadAllText(getSettings("grLogs")).Split('\n');
                lbxLogs.Items.Clear();
                int i = 0;
                for (i = 0; i < sLogs.Length; i++)
                {
                    if (sLogs[i] != "")
                    {
                        if (sLogs[i].Contains("[INFO]"))
                        {
                            lbxLogs.Items.Add(sLogs[i]).BackColor = cInfo;
                        }
                        else if (sLogs[i].Contains("[ERROR]"))
                        {
                            lbxLogs.Items.Add(sLogs[i]).BackColor = cError;
                        }
                    }
                }
            }
            if (iParameter == 6)
            {
                // Menu 4
                btnMenu6.BackColor = cActivee;
                btnMenu6.Font = fActivee;

                // Afficher la page
                menu1.Visible = false;
                menu2.Visible = false;
                menu3.Visible = false;
                menu4.Visible = false;
                menu5.Visible = false;
                menu6.Visible = true;
                menu7.Visible = false;
            }
            if (iParameter == 7)
            {
                // Menu 4
                btnMenu7.BackColor = cActivee;
                btnMenu7.Font = fActivee;

                // Afficher la page
                menu1.Visible = false;
                menu2.Visible = false;
                menu3.Visible = false;
                menu4.Visible = false;
                menu5.Visible = false;
                menu6.Visible = false;
                menu7.Visible = true;
            }
        }

        // FONCTION 20
        private void MailActiverOuDesactiverBoutonEnregistrer()
        {
            // Variables
            string sOldData = getSettings("mailTitre") + getSettings("mailCC") + getSettings("mailMessage").Replace("{rn}", "\r\n");
            string sNewData = tbxOptionTitre.Text + tbxOptionCC.Text + tbxOptionMessage.Text;

            // Activation du bouton si il y a une différence
            if (sOldData != sNewData)
            {
                btnEnregistrerParametreEnvoi.Enabled = true;
                btnEnregistrerParametreEnvoi.BackColor = Color.MediumSeaGreen;
            } else
            {
                btnEnregistrerParametreEnvoi.Enabled = false;
                btnEnregistrerParametreEnvoi.BackColor = Color.LightGray;
            }
        }

        // FONCTION 21
        private bool sendMail(string sAttachment, string sPrenom, string sNom, string sProjet, string sMail, string sMois, string sMultiRapportEmployes)
        {
            try
            {
                // Variables
                string sMailFrom = getSettings("mailFrom");
                string sMailTo = sMail;
                string[] CC = getSettings("mailCC").Split(new string[] { ", " }, StringSplitOptions.None);
                string sTitre = getSettings("mailTitre");
                string sMessage = getSettings("mailMessage").Replace("{prenom}", sPrenom).Replace("{nom}", sNom).Replace("{projet}", sProjet).Replace("{rn}", "\r\n");
                string sSMTP = getSettings("mailSMTP");
                int iPort = Convert.ToInt32(getSettings("mailPort"));

                // Informations
                MailMessage message = new MailMessage();
                SmtpClient smtp = new SmtpClient();
                message.From = new MailAddress(sMailFrom);
                message.To.Add(new MailAddress(sMailTo));
                message.Subject = sTitre;
                message.Body = sMessage;
                message.IsBodyHtml = false;
                smtp.Port = iPort;
                smtp.Host = sSMTP;
                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;

                // Attachments
                Attachment attachment = new Attachment(sAttachment);
                int iNumberOfAttachment = 0;
                if (sMultiRapportEmployes.Contains(sPrenom + "-" + sNom))
                {
                    // Plusieurs attachments
                    for (int j = 0; j < lbxDocumentsGeneres.Items.Count; j++)
                    {
                        if (lbxDocumentsGeneres.Items[j].Checked == true)
                        {
                            if (lbxDocumentsGeneres.Items[j].Text.Contains(sPrenom + "-" + sNom))
                            {
                                Attachment attachments = new Attachment(getSettings("FilePathGeneration") + lbxDocumentsGeneres.Items[j].Text.Split(new string[] { " ||" }, StringSplitOptions.None)[0]);
                                message.Attachments.Add(attachments);
                                iNumberOfAttachment++;
                            }
                        }
                    }
                }
                else
                {
                    // Un seul attachment
                    message.Attachments.Add(attachment);
                }

                // CC
                for (int i = 0; i < CC.Length; i++)
                {
                    if (CC[i] != "")
                    {
                        message.CC.Add(CC[i]);
                    }
                }

                // Envoi du mail
                string sAllSendedMail = getSettings("allSendedMail");
                if (!sAllSendedMail.Contains(sPrenom + "-" + sNom)) // Si un mail a déjà été envoyé a cette personne
                {
                    // Envoi du mail
                    smtp.Send(message);

                    // Ajout dans la liste des mails envoyés
                    updateSettings("allSendedMail", getSettings("allSendedMail") + sPrenom + "-" + sNom);
                }
                else
                {
                    // Ne pas envoyer
                }

                // Ajout dans les logs
                addLogs(2, 1, sPrenom, sNom, sProjet, "", sMois, sMail, "");

                // retour
                return true;
            }
            catch (Exception e)
            {
                // En cas d'erreur
                addLogs(2, 0, sPrenom, sNom, sProjet, "", "", sMail, "");
                MessageBox.Show("Une erreur est survenue lors de l'envoi du mail\n" + e);
                return false;
            }

        }

        // FONCTION 22
        private void addLogs(int iParam1, int iParam2, string sPrenom, string sNom, string sProjet, string sDivisePar, string sMois, string sMail, string sNomDuRapport)
        {
            try
            {
                // Variables
                string sLogsPath = getSettings("grLogs");
                string sOldLogs = File.ReadAllText(sLogsPath);
                string sNewLine = "";
                string sCurrentDateTime = DateTime.Now.ToString("dd.MM.yyyy - HH:mm:ss");
                string sInfo = "[" + sCurrentDateTime + "] [INFO] > ";
                string sError = "[" + sCurrentDateTime + "] [ERROR] > ";

                // Traitement sNewLIne
                if (iParam1 == 1)
                {
                    // Ajouter logs génération de rapports
                    if (iParam2 == 1)
                    {
                        // Generation faites avec succès
                        sNewLine = sInfo + "SUCCÈS : Génération du rapport de " + sPrenom + " " + sNom + " (" + sMois + "_" + sProjet + ")";
                    }
                    else
                    {
                        // Erreur lors de la génération
                        sNewLine = sError + "ERREUR : Génération du rapport de " + sPrenom + " " + sNom + " (" + sMois + "_" + sProjet + ")";
                    }
                }
                if (iParam1 == 2)
                {
                    // Ajouter logs d'envoi de rapports
                    if (iParam2 == 1)
                    {
                        // Generation faites avec succès
                        sNewLine = sInfo + "SUCCÈS : Envoi du rapport de " + sPrenom + " " + sNom + " (" + sMois + "_" + sProjet + ") mail: " + sMail;
                    }
                    else
                    {
                        // Erreur lors de la génération
                        sNewLine = sError + "ERREUR : Envoi du rapport de " + sPrenom + " " + sNom + " (" + sMois + "_" + sProjet + ") mail: " + sMail;
                    }
                }
                if (iParam1 == 3)
                {
                    // Ajouter logs d'envoi de rapports
                    if (iParam2 == 1)
                    {
                        // Generation faites avec succès
                        sNewLine = sInfo + "SUCCÈS : Suppression du rapport : " + sNomDuRapport;
                    }
                    else
                    {
                        // Erreur lors de la génération
                        sNewLine = sError + "ERREUR : Suppression du rapport : " + sNomDuRapport;
                    }
                }

                // Ajout de la nouvelle ligne aux logs
                string sNewLogs = sOldLogs + sNewLine + "\n";
                File.WriteAllText(sLogsPath, sNewLogs);
            } catch (Exception e)
            {
                MessageBox.Show("Une erreur est survenue lors de l'écriture des logs\n" + e);
            }
        }

        // FONCTION 23
        private void atlanseGenerer(int iNumeroEmploye, ComboBox cbxPrenomNom, TextBox tbxPrixParJour, TextBox tbxRevenu)
        {
            try
            {
                // Check
                if (cbxPrenomNom.Text == "") { tbxRevenu.Text = "0"; return; }
                if (tbxPrixParJour.Text == "") { MessageBox.Show("Veuillez insérer un prix par jour pour tous les employés"); return; }

                // Variables
                string[] AllUsers = tbxUsers.Text.Split(new string[] { "},{" }, StringSplitOptions.None);
                string DateDu = FormatDate(GetDateFromTheMonthIndex(lbxMois2.SelectedIndex, 1), 2);
                string DateAu = FormatDate(GetDateFromTheMonthIndex(lbxMois2.SelectedIndex, 2), 2);
                string sPrenom = cbxPrenomNom.Text.Split(' ')[0];
                string sNom = cbxPrenomNom.Text.Split(' ')[1];
                string sPrixParJour = tbxPrixParJour.Text;

                // Récupération de tous les commentaires de la personne sur le mois séléctionné
                string sUserID = "";

                // Recherche de l'ID de l'employé
                for (int i = 0; i < AllUsers.Length; i++)
                {
                    // Variables
                    string sFoundFirstName = Encoding.UTF8.GetString(Encoding.Default.GetBytes(GetDataFrom("FIRST_NAME", AllUsers[i], "STRING")));
                    string sFoundLastName = Encoding.UTF8.GetString(Encoding.Default.GetBytes(GetDataFrom("LAST_NAME", AllUsers[i], "STRING")));

                    // ID
                    if (sFoundFirstName == sPrenom && sFoundLastName == sNom)
                    {
                        sUserID = GetDataFrom("USER_ID", AllUsers[i], "INT");
                        break;
                    }
                    else
                    {
                        sUserID = "ID_NOT_FOUND";
                    }
                }

                // Recherche des projets
                if (sUserID != "ID_NOT_FOUND")
                {
                    using (var webClient = new WebClient())
                    {
                        // Appel vers l'API
                        ServicePointManager.Expect100Continue = true;
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                        String sAPItimereports = webClient.DownloadString("https://api.aceproject.com/?fct=gettimereport&guid=" + lblGUID.Text + "&view=1&filtermyworkitems=False&FilterTimeCreatorUserId=" + sUserID + "&filtertimelevel=0&filterdatefrom=" + DateDu + "&filterdateto=" + DateAu + "&countonly=False&isshowtotalsonly=False&asynccall=False&exportonscreencolumnsonly=True&exportview=0&exportremovehtmlonly=True&exportenablefilterxls=False&format=json");
                        
                        // Check si l'employé a des tâches pour le mois courant
                        if(sAPItimereports.Contains("results\":[]"))
                        {
                            MessageBox.Show(sPrenom + " " + sNom + " n'a pas encore d'informations pour ce mois-ci");
                            return;
                        }

                        string[] sAPItimereport = sAPItimereports.Split(new string[] { "},{" }, StringSplitOptions.None);
                        // DEBUG AFFICHER API // System.Diagnostics.Process.Start("https://api.aceproject.com/?fct=gettimereport&guid=" + lblGUID.Text + "&view=1&filtermyworkitems=False&FilterTimeCreatorUserId=" + sUserID + "&filtertimelevel=0&filterdatefrom=" + DateDu + "&filterdateto=" + DateAu + "&countonly=False&isshowtotalsonly=False&asynccall=False&exportonscreencolumnsonly=True&exportview=0&exportremovehtmlonly=True&exportenablefilterxls=False&format=json");

                        // Nombre de jours travaillé au total (En enlevant "PAP - Administration")
                        double dNbrHeuresTotal = 0;
                        for (int i = 0; i < sAPItimereport.Length; i++)
                        {
                            if (GetDataFrom("PROJECT_NAME", sAPItimereport[i], "STRING") != "PAP - Administration")
                            {
                                // Recherche du nombre d'heure passés sur la tâche
                                string heure = GetDataFrom("TOTAL", sAPItimereport[i], "INT");

                                // Incrémentation
                                dNbrHeuresTotal += Convert.ToDouble(heure);
                            }
                        }
                        string sNbrJours = "";

                        // Calcule du nombre de jours
                        if(Math.Round(dNbrHeuresTotal / 8, 2) < 1)
                        {
                            // Chiffre inférieur à 1
                            sNbrJours = (Math.Round(dNbrHeuresTotal / 8, 2)).ToString("N");
                        } 
                        else
                        {
                            // Chiffre supérieur à 1
                            sNbrJours = (Math.Round(dNbrHeuresTotal / 8, 2)).ToString("");
                        }
                        

                        // Calcule du revenu // ICI
                        string sRevenu = (Convert.ToDouble(sPrixParJour) * Convert.ToDouble(sNbrJours)).ToString("N");

                        // Affichage
                        if (iNumeroEmploye == 1) { tbxRevenue1.Text = sRevenu; tbxNbrJours1.Text = sNbrJours; }
                        if (iNumeroEmploye == 2) { tbxRevenue2.Text = sRevenu; tbxNbrJours2.Text = sNbrJours; }
                        if (iNumeroEmploye == 3) { tbxRevenue3.Text = sRevenu; tbxNbrJours3.Text = sNbrJours; }
                        if (iNumeroEmploye == 4) { tbxRevenue4.Text = sRevenu; tbxNbrJours4.Text = sNbrJours; }
                        if (iNumeroEmploye == 5) { tbxRevenue5.Text = sRevenu; tbxNbrJours5.Text = sNbrJours; }
                        if (iNumeroEmploye == 6) { tbxRevenue6.Text = sRevenu; tbxNbrJours6.Text = sNbrJours; }
                        if (iNumeroEmploye == 7) { tbxRevenue7.Text = sRevenu; tbxNbrJours7.Text = sNbrJours; }
                        if (iNumeroEmploye == 8) { tbxRevenue8.Text = sRevenu; tbxNbrJours8.Text = sNbrJours; }

                    }
                }
                else
                {
                    MessageBox.Show("Erreur lors de la génération des données d'atlanse. ID non trouvé");
                }
            }
            catch (Exception e)
            {
                // Message d'erreur
                MessageBox.Show("Une erreur est survenue lors de la recherche des données d'atlanse: \n" + e);
            }
        }

        // FONCTION 24
        private void saveAtlanseEmployeNom(ComboBox cbx, int iNo)
        {
            try
            {
                updateSettings("atlEmployeNom" + Convert.ToString(iNo), cbx.Text);
            } catch
            {
                MessageBox.Show("Une erreur est survenue lors de la sauvegarde des noms des employés atlanse");
            }
        }

        // FONCTION 26
        private void saveAtlanseEmployePrixParJour(TextBox tbx, int iNo)
        {
            try {
                updateSettings("atlPrixParJour" + Convert.ToString(iNo), tbx.Text);
            } catch
            {
                MessageBox.Show("Une erreur est survenue lors de la sauvegarde des prix par jour des employés atlanse");
            }
        }

        // FONCTION 27
        private void EnableOrDisableAllBTNPreviewATL()
        {
            // Bouton 1 (Activer ou désactiver)
            if (cbxEmploye1.Text != "" && tbxPrixJour1.Text != "") { EnableBTN(btnATLpreview1); } else { DisableBTN(btnATLpreview1); }
            if (cbxEmploye2.Text != "" && tbxPrixJour2.Text != "") { EnableBTN(btnATLpreview2); } else { DisableBTN(btnATLpreview2); }
            if (cbxEmploye3.Text != "" && tbxPrixJour3.Text != "") { EnableBTN(btnATLpreview3); } else { DisableBTN(btnATLpreview3); }
            if (cbxEmploye4.Text != "" && tbxPrixJour4.Text != "") { EnableBTN(btnATLpreview4); } else { DisableBTN(btnATLpreview4); }
            if (cbxEmploye5.Text != "" && tbxPrixJour5.Text != "") { EnableBTN(btnATLpreview5); } else { DisableBTN(btnATLpreview5); }
            if (cbxEmploye6.Text != "" && tbxPrixJour6.Text != "") { EnableBTN(btnATLpreview6); } else { DisableBTN(btnATLpreview6); }
            if (cbxEmploye7.Text != "" && tbxPrixJour7.Text != "") { EnableBTN(btnATLpreview7); } else { DisableBTN(btnATLpreview7); }
            if (cbxEmploye8.Text != "" && tbxPrixJour8.Text != "") { EnableBTN(btnATLpreview8); } else { DisableBTN(btnATLpreview8); }
        }

        // FONCTION 28
        private void EnableBTN(Button btn)
        {
            try
            {
                // Activer
                btn.Enabled = true;
                btn.BackColor = Color.MediumSeaGreen;
            }
            catch
            {
                MessageBox.Show("Erreur lors de l'activation d'un des boutons de preview d'atlanse");
            }
        }

        // FONCTION 29
        private void DisableBTN(Button btn)
        {
            try
            {
                // Désactiver
                btn.Enabled = false;
                btn.BackColor = Color.LightGray;
            }
            catch
            {
                MessageBox.Show("Erreur lors de la désactivation d'un des boutons de preview d'atlanse");
            }
        }

        // FONCTION 30
        private void ShowEmployeATLtask(string sEmploye)
        {
            try
            {
                // Activer la liste (+clear)
                lbxATLEmployeInformation.Visible = true;
                lblNomPrenomATL.Visible = true;
                lblNomPrenomATL.Text = sEmploye + " (" + lbxMois2.Text + ") ";
                lbxATLEmployeInformation.Items.Clear();

                // Variables
                string[] AllUsers = tbxUsers.Text.Split(new string[] { "},{" }, StringSplitOptions.None);
                string dateFrom = FormatDate(GetDateFromTheMonthIndex(lbxMois2.SelectedIndex, 1), 2);
                string dateTo = FormatDate(GetDateFromTheMonthIndex(lbxMois2.SelectedIndex, 2), 2);
                string sUserID = "";
                string sPrenom = sEmploye.Split(' ')[0];
                string sNom = sEmploye.Split(' ')[1];

                // Recherche de l'ID
                for (int i = 0; i < AllUsers.Length; i++)
                {
                    // Variables
                    string sFoundFirstName = Encoding.UTF8.GetString(Encoding.Default.GetBytes(GetDataFrom("FIRST_NAME", AllUsers[i], "STRING")));
                    string sFoundLastName = Encoding.UTF8.GetString(Encoding.Default.GetBytes(GetDataFrom("LAST_NAME", AllUsers[i], "STRING")));

                    // ID
                    if (sFoundFirstName == sPrenom && sFoundLastName == sNom)
                    {
                        sUserID = GetDataFrom("USER_ID", AllUsers[i], "INT");
                        break;
                    }
                    else
                    {
                        sUserID = "ID_NOT_FOUND";
                    }
                }

                // Recherche des projets
                if (sUserID != "ID_NOT_FOUND")
                {
                    using (var webClient = new WebClient())
                    {
                        // Appel vers l'API
                        ServicePointManager.Expect100Continue = true;
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                        String sAPItimereports = webClient.DownloadString("https://api.aceproject.com/?fct=gettimereport&guid=" + lblGUID.Text + "&view=1&filtermyworkitems=False&FilterTimeCreatorUserId=" + sUserID + "&filtertimelevel=0&filterdatefrom=" + dateFrom + "&filterdateto=" + dateTo + "&countonly=False&isshowtotalsonly=False&asynccall=False&exportonscreencolumnsonly=True&exportview=0&exportremovehtmlonly=True&exportenablefilterxls=False&format=json");

                        // Si il n'a pas de projets inscrits
                        if (sAPItimereports.Contains("\"results\":[]}"))
                        {
                            // aucun projets
                            MessageBox.Show("Aucune tâche n'a été trouvée pour " + sPrenom + " " + sNom);
                            return;
                        }

                        // Récupération de toute les tâches du projet
                        string[] sAPItimereport = sAPItimereports.Split(new string[] { "},{" }, StringSplitOptions.None);
                        foreach (var sTimeReport in sAPItimereport)
                        {
                            // Valeurs
                            string sDate = FormatDate(GetDataFrom("DATE_WORKED", sTimeReport, "STRING"), 1);
                            string sProjet = Encoding.UTF8.GetString(Encoding.Default.GetBytes(GetDataFrom("PROJECT_NAME", sTimeReport, "STRING")));
                            string sHeure = GetDataFrom("TOTAL", sTimeReport, "INT");
                            string sTypeHeure = GetDataFrom("TIME_TYPE_NAME", sTimeReport, "STRING");
                            string sCommentaire = GetDataFrom("COMMENT", sTimeReport, "STRING");
                            if(sCommentaire == "N/A") { sCommentaire = "-"; }

                            ListViewItem item = new ListViewItem(sDate);
                            item.SubItems.Add(sProjet);
                            item.SubItems.Add(sHeure);
                            item.SubItems.Add(sTypeHeure);
                            item.SubItems.Add(sCommentaire);

                            // Affichage
                            if (!sProjet.Contains("PAP - Administration"))
                            {
                                lbxATLEmployeInformation.Items.Add(item);
                            }
                            else
                            {
                                lbxATLEmployeInformation.Items.Add(item).BackColor = Color.FromArgb(255, 155, 155);
                            }
                        }
                    }
                }
            } catch
            {
                MessageBox.Show("Erreur lors de la preview de la liste des tâches d'Atlanse");
            }
        }

        // FONCTION 31
        private void GetGUIDandUSERS()
        {
            try
            {
                using (var webClient = new WebClient())
                {
                    // Appel vers l'API
                    ServicePointManager.Expect100Continue = true;
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                    // Récupération du GUID pour accéder aux APIs
                    string sUserName = getSettings("apiUserName");
                    string sPassword = decrypt(getSettings("apiPassword"));
                    string userAPI = "https://api.aceproject.com/?fct=login&accountid=proactive&username=" + sUserName + "&password=" + sPassword + "&format=json";
                    String sUserInformation = webClient.DownloadString(userAPI);
                    string sGUID = GetDataFrom("GUID", sUserInformation, "STRING");
                    lblGUID.Text = sGUID;

                    // Récupérations de tous les utilisateurs
                    // DEBUG - OUVRIR API // System.Diagnostics.Process.Start("https://api.aceproject.com/?fct=getusers&guid=" + sGUID + "&filteractive=true&sortorder=FIRST_NAME&format=json");
                    String sAPIusers = webClient.DownloadString("https://api.aceproject.com/?fct=getusers&guid=" + sGUID + "&filteractive=true&sortorder=FIRST_NAME&format=json");
                    tbxUsers.Text = sAPIusers;

                }
            }
            catch
            {
                MessageBox.Show("Une erreur est survenue lors de l'appel à l'API.\n" + "Vérifiez votre connexion internet, la disponibilité de l'api de aceproject.com ou l'exactitude du mot de passe pour l'utilisateur \"" + getSettings("apiUserName") + "\"");
            }
        }

        // FONCTION 32
        private void LoadSettings()
        {
            try
            {
                // ============== Utilisateur ==============
                tbxOptionGeneratedFilePath.Text = getSettings("FilePathGeneration");
                if (getSettings("OpenRapportWhenGenerated") == "True")
                {
                    rdbOui.Checked = true;
                    rdbNon.Checked = false;
                }
                else
                {
                    rdbOui.Checked = false;
                    rdbNon.Checked = true;
                }
                tbxOptionTitre.Text = getSettings("mailTitre");
                tbxOptionCC.Text = getSettings("mailCC");
                tbxOptionMessage.Text = getSettings("mailMessage").Replace("{rn}", "\r\n");
                cbxEmploye1.Text = getSettings("atlEmployeNom1");
                cbxEmploye2.Text = getSettings("atlEmployeNom2");
                cbxEmploye3.Text = getSettings("atlEmployeNom3");
                cbxEmploye4.Text = getSettings("atlEmployeNom4");
                cbxEmploye5.Text = getSettings("atlEmployeNom5");
                cbxEmploye6.Text = getSettings("atlEmployeNom6");
                cbxEmploye7.Text = getSettings("atlEmployeNom7");
                cbxEmploye8.Text = getSettings("atlEmployeNom8");
                tbxPrixJour1.Text = getSettings("atlPrixParJour1");
                tbxPrixJour2.Text = getSettings("atlPrixParJour2");
                tbxPrixJour3.Text = getSettings("atlPrixParJour3");
                tbxPrixJour4.Text = getSettings("atlPrixParJour4");
                tbxPrixJour5.Text = getSettings("atlPrixParJour5");
                tbxPrixJour6.Text = getSettings("atlPrixParJour6");
                tbxPrixJour7.Text = getSettings("atlPrixParJour7");
                tbxPrixJour8.Text = getSettings("atlPrixParJour8");

                // ============== Administration ==============
                // HTML
                tbxAdminHTML_colonne1nom.Text = getSettings("adminHTML_Colonne1nom");
                tbxAdminHTML_colonne2nom.Text = getSettings("adminHTML_Colonne2nom");
                tbxAdminHTML_colonne3nom.Text = getSettings("adminHTML_Colonne3nom");
                tbxAdminHTML_colonne4nom.Text = getSettings("adminHTML_Colonne4nom");
                tbxAdminHTML_colonne5nom.Text = getSettings("adminHTML_Colonne5nom");

                tbxAdminHTML_colonne1largeur.Text = getSettings("adminHTML_Colonne1largeur");
                tbxAdminHTML_colonne2largeur.Text = getSettings("adminHTML_Colonne2largeur");
                tbxAdminHTML_colonne3largeur.Text = getSettings("adminHTML_Colonne3largeur");
                tbxAdminHTML_colonne4largeur.Text = getSettings("adminHTML_Colonne4largeur");
                tbxAdminHTML_colonne5largeur.Text = getSettings("adminHTML_Colonne5largeur");

                tbxAdminHTML_logo.Text = getSettings("adminHTML_Logo");
                tbxAdminHTML_logoWidth.Text = getSettings("adminHTML_WidthLogo");
                tbxAdminHTML_CSS.Text = File.ReadAllText(getSettings("adminHTML_CSS"));

                // MAIL
                tbxAdminMAIL_MailDenvoi.Text = getSettings("mailFrom");
                tbxAdminMAIL_SMTP.Text = getSettings("mailSMTP");
                tbxAdminMAIL_Port.Text = getSettings("mailPort");

                // API
                tbxAdminAPI_user.Text = getSettings("apiUserName");
                tbxAdminAPI_password.Text = decrypt(getSettings("apiPassword"));

                // Divers
                tbxAdminDIVERS_backupname.Text = getSettings("backupNom");

                // Désactiver les boutons
                DisableBTN(btnSaveHTML);
                DisableBTN(btnSaveMAIL);
                DisableBTN(btnSaveAPI);
                DisableBTN(btnSaveDIVERS);


            }
            catch
            {
                MessageBox.Show("Une erreur est survenue lors du chargement des paramètres");
            }
        }

        // FONCTION 33
        private void ShowMonthInListbox(ComboBox lbx)
        {
            try
            {
                // Valeurs
                DateTime thisDay = DateTime.Today;
                string currentMonth = thisDay.ToString("MMMM");
                string currentMonthBefore1 = new DateTime(thisDay.Year, thisDay.AddMonths(-1).Month, 1).ToString("MMMM");
                string currentMonthBefore2 = new DateTime(thisDay.Year, thisDay.AddMonths(-2).Month, 1).ToString("MMMM");

                // Mois génération rapports
                lbx.Items.Add(currentMonth);
                lbx.Items.Add(currentMonthBefore1);
                lbx.Items.Add(currentMonthBefore2);
                lbx.SelectedIndex = 1;
            } catch
            {
                MessageBox.Show("Une erreur est survenue lors de l'affichage des mois dans une listbox");
            }
        }

        // FONCTION 34
        private void SetLocationOfMenus(int ilocX, int ilocY)
        {
            try
            {
                // Locations
                menu1.Location = new Point(ilocX, ilocY);
                menu2.Location = new Point(ilocX, ilocY);
                menu3.Location = new Point(ilocX, ilocY);
                menu4.Location = new Point(ilocX, ilocY);
                menu5.Location = new Point(ilocX, ilocY);
                menu6.Location = new Point(ilocX, ilocY);
                menu7.Location = new Point(ilocX, ilocY);

            } catch
            {
                MessageBox.Show("Une erreur est survenue lors de la SET-LOCATION des menus");
            }
        }

        // FONCTION 35
        private void atlanseGenererALL() 
        {

            // Générer
            atlanseGenerer(1, cbxEmploye1, tbxPrixJour1, tbxRevenue1);
            atlanseGenerer(2, cbxEmploye2, tbxPrixJour2, tbxRevenue2);
            atlanseGenerer(3, cbxEmploye3, tbxPrixJour3, tbxRevenue3);
            atlanseGenerer(4, cbxEmploye4, tbxPrixJour4, tbxRevenue4);
            atlanseGenerer(5, cbxEmploye5, tbxPrixJour5, tbxRevenue5);
            atlanseGenerer(6, cbxEmploye6, tbxPrixJour6, tbxRevenue6);
            atlanseGenerer(7, cbxEmploye7, tbxPrixJour7, tbxRevenue7);
            atlanseGenerer(8, cbxEmploye8, tbxPrixJour8, tbxRevenue8);

        }

        // FONCTION 36
        private void AfficherRevenuTotal()
        {
            // Traitement
            double dRevenu1 = 0; if (tbxRevenue1.Text != "") { dRevenu1 = Convert.ToDouble(tbxRevenue1.Text.Replace("'", "")); }
            double dRevenu2 = 0; if (tbxRevenue2.Text != "") { dRevenu2 = Convert.ToDouble(tbxRevenue2.Text.Replace("'", "")); }
            double dRevenu3 = 0; if (tbxRevenue3.Text != "") { dRevenu3 = Convert.ToDouble(tbxRevenue3.Text.Replace("'", "")); }
            double dRevenu4 = 0; if (tbxRevenue4.Text != "") { dRevenu4 = Convert.ToDouble(tbxRevenue4.Text.Replace("'", "")); }
            double dRevenu5 = 0; if (tbxRevenue5.Text != "") { dRevenu5 = Convert.ToDouble(tbxRevenue5.Text.Replace("'", "")); }
            double dRevenu6 = 0; if (tbxRevenue6.Text != "") { dRevenu6 = Convert.ToDouble(tbxRevenue6.Text.Replace("'", "")); }
            double dRevenu7 = 0; if (tbxRevenue7.Text != "") { dRevenu7 = Convert.ToDouble(tbxRevenue7.Text.Replace("'", "")); }
            double dRevenu8 = 0; if (tbxRevenue8.Text != "") { dRevenu8 = Convert.ToDouble(tbxRevenue8.Text.Replace("'", "")); }
            
            double dRevenuTotal = dRevenu1 + dRevenu2 + dRevenu3 + dRevenu4 + dRevenu5 + dRevenu6 + dRevenu7 + dRevenu8;

            // Affichage
            lblTotal.Text = "Total: " + (dRevenuTotal).ToString("#,##0.00") + " €";
        }

        // FONCTION 37
        private void ClearLogs()
        {
            try
            {
                // Backup
                int iNumero = Convert.ToInt32(getSettings("NbrDeBackupsActuel")) + 1;
                string sNomDeLaBackup = getSettings("backupNom") + iNumero;
                File.WriteAllText(getSettings("backupLogsFolder") + sNomDeLaBackup, File.ReadAllText(getSettings("grLogs")));

                updateSettings("NbrDeBackupsActuel", Convert.ToString(iNumero));

                // Suppression
                File.WriteAllText(getSettings("grLogs"), "");
                lbxLogs.Items.Clear();

                // Refresh
                RefreshListbox(5);
                RefreshListbox(6);
            }
            catch
            {
                MessageBox.Show("Une erreur est survenue lors de la suppression / backup des logs");
            }
        }

        // FONCTION 38
        private string MonthFromNumber(string number)
        {
            // Valeurs
            string[] sMonth = { "Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre" };

            // Traitement
            string result = sMonth[Convert.ToInt32(number) - 1];

            // Retour
            return result;
        }

        // FONCTION 39
        private void SaveAdminSettings(int iParam)
        {
            try
            {
                // Valeurs
                if (iParam == 1)
                {
                    // Enregistrer les paramètres HTML 
                    updateSettings("adminHTML_Colonne1nom", tbxAdminHTML_colonne1nom.Text);
                    updateSettings("adminHTML_Colonne2nom", tbxAdminHTML_colonne2nom.Text);
                    updateSettings("adminHTML_Colonne3nom", tbxAdminHTML_colonne3nom.Text);
                    updateSettings("adminHTML_Colonne4nom", tbxAdminHTML_colonne4nom.Text);
                    updateSettings("adminHTML_Colonne5nom", tbxAdminHTML_colonne5nom.Text);

                    updateSettings("adminHTML_Colonne1largeur", tbxAdminHTML_colonne1largeur.Text);
                    updateSettings("adminHTML_Colonne2largeur", tbxAdminHTML_colonne2largeur.Text);
                    updateSettings("adminHTML_Colonne3largeur", tbxAdminHTML_colonne3largeur.Text);
                    updateSettings("adminHTML_Colonne4largeur", tbxAdminHTML_colonne4largeur.Text);
                    updateSettings("adminHTML_Colonne5largeur", tbxAdminHTML_colonne5largeur.Text);

                    updateSettings("adminHTML_Logo", tbxAdminHTML_logo.Text);
                    updateSettings("adminHTML_WidthLogo", tbxAdminHTML_logoWidth.Text);
                    File.WriteAllText(getSettings("adminHTML_CSS"), tbxAdminHTML_CSS.Text);
                }
                if (iParam == 2)
                {
                    // Enregistrer les paramètres DIVERS
                    updateSettings("backupNom", tbxAdminDIVERS_backupname.Text);
                }
                if (iParam == 3)
                {
                    // Enregistrer les paramètres MAIL
                    updateSettings("mailFrom", tbxAdminMAIL_MailDenvoi.Text);
                    updateSettings("mailSMTP", tbxAdminMAIL_SMTP.Text);
                    updateSettings("mailPort", tbxAdminMAIL_Port.Text);
                }
                if (iParam == 4)
                {
                    // Enregistrer les paramètres API
                    updateSettings("apiUserName", tbxAdminAPI_user.Text);
                    updateSettings("apiPassword", encrypt(tbxAdminAPI_password.Text));
                }
            } catch
            {
                MessageBox.Show("Une erreur est survenue lors de l'enregistrement des paramètres d'administration");
            }
        }

        // FONCTION 40
        private void refreshNbrSelectionne(int iParam)
        {
            // Valeur
            int i = 0;
            string text = "";
            ListView lbx;
            Label lbl;
            if(iParam == 1) { lbx = lbxRapports; } else { lbx = lbxDocumentsGeneres; }
            if(iParam == 1) { lbl = lblNbrSelectionRapports; } else { lbl = lblNbrSelectionRapportsGenere; }

            // Traitement
            foreach (ListViewItem item in lbx.Items)
            {
                if(item.Checked == true) {
                    i++;
                }
            }
            text = "Cochés : " + i;
            if (i <= 1) { text = text.Replace("s", ""); }

            // Affichage
            lbl.Text = text;
        }

        // FONCTION 41
        private void updateSettings(string sKey, string sValue)
        {
            try
            {
                // Variables
                string sSettingsPath = Properties.Settings.Default.settingsPath;
                string sSettings = File.ReadAllText(sSettingsPath);
                string[] aSettings = File.ReadAllText(sSettingsPath).Split('\n');
                string sOldSetting = "";
                string sNewSetting = "";

                // Traitement
                foreach (string sSetting in aSettings)
                {
                    // Lorsque il est sur la bonne ligne
                    if (sSetting.StartsWith(sKey))
                    {
                        sOldSetting = sSetting;
                        sNewSetting = sKey + "=\"" + sValue + "\"";
                    }
                }

                // Modification dans le fichier
                File.WriteAllText(sSettingsPath, sSettings.Replace(sOldSetting, sNewSetting));
            } catch
            {
                MessageBox.Show("Une erreur est survenue lors de la mise à jour d'un paramètre");
            }
        }

        // FONCTION 42
        private string getSettings(string sKey)
        {
            try
            {
                // Variables
                string sSettingsPath = Properties.Settings.Default.settingsPath;
                string[] aSettings = File.ReadAllText(sSettingsPath).Split('\n');
                string sData = "[SETTING_NOT_FOUND]";

                // Traitement
                foreach (string sSetting in aSettings)
                {
                    // Lorsque il est sur la bonne ligne
                    if (sSetting.StartsWith(sKey))
                    {
                        sData = sSetting.Replace(sKey + "=", "").Replace("\"", "").Replace("\r", "").Replace("\n", "");
                    }
                }

                // Retour
                return sData;
            } catch
            {
                MessageBox.Show("Une erreur est survenue lors de la recherche d'un paramètre");
                return "[SETTING_NOT_FOUND]";
            }
        }

        // FONCTION 43
        private string encrypt(string str)
        {
            // Encryption
            byte[] plainTextBytes = Encoding.UTF8.GetBytes(str);
            byte[] keyBytes = new Rfc2898DeriveBytes(PasswordHash, Encoding.ASCII.GetBytes(SaltKey)).GetBytes(256 / 8);
            var symmetricKey = new RijndaelManaged() { Mode = CipherMode.CBC, Padding = PaddingMode.Zeros };
            var encryptor = symmetricKey.CreateEncryptor(keyBytes, Encoding.ASCII.GetBytes(VIKey));
            byte[] cipherTextBytes;
            using (var memoryStream = new MemoryStream())
            {
                using (var cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write))
                {
                    cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
                    cryptoStream.FlushFinalBlock();
                    cipherTextBytes = memoryStream.ToArray();
                    cryptoStream.Close();
                }
                memoryStream.Close();
            }

            // Retour
            return Convert.ToBase64String(cipherTextBytes);
        }
        // FONCTION 44
        private string decrypt(string str)
        {
            // Decryption
            byte[] cipherTextBytes = Convert.FromBase64String(str);
            byte[] keyBytes = new Rfc2898DeriveBytes(PasswordHash, Encoding.ASCII.GetBytes(SaltKey)).GetBytes(256 / 8);
            var symmetricKey = new RijndaelManaged() { Mode = CipherMode.CBC, Padding = PaddingMode.None };
            var decryptor = symmetricKey.CreateDecryptor(keyBytes, Encoding.ASCII.GetBytes(VIKey));
            var memoryStream = new MemoryStream(cipherTextBytes);
            var cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read);
            byte[] plainTextBytes = new byte[cipherTextBytes.Length];
            int decryptedByteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);
            memoryStream.Close();
            cryptoStream.Close();

            // Retour
            return Encoding.UTF8.GetString(plainTextBytes, 0, decryptedByteCount).TrimEnd("\0".ToCharArray());
        }

        // Si un changement est effectué dans les options administrateurs
        private void tbxAdminHTML_logo_TextChanged(object sender, EventArgs e){ EnableBTN(btnSaveHTML); }
        private void tbxAdminHTML_logoWidth_TextChanged(object sender, EventArgs e){ EnableBTN(btnSaveHTML); }
        private void tbxAdminHTML_colonne1nom_TextChanged(object sender, EventArgs e) { EnableBTN(btnSaveHTML); }
        private void tbxAdminHTML_colonne1largeur_TextChanged(object sender, EventArgs e) { EnableBTN(btnSaveHTML); }
        private void tbxAdminHTML_colonne2nom_TextChanged(object sender, EventArgs e) { EnableBTN(btnSaveHTML); }
        private void tbxAdminHTML_colonne2largeur_TextChanged(object sender, EventArgs e) { EnableBTN(btnSaveHTML); }
        private void tbxAdminHTML_colonne3nom_TextChanged(object sender, EventArgs e) { EnableBTN(btnSaveHTML); }
        private void tbxAdminHTML_colonne3largeur_TextChanged(object sender, EventArgs e){ EnableBTN(btnSaveHTML); }
        private void tbxAdminHTML_colonne4nom_TextChanged(object sender, EventArgs e) { EnableBTN(btnSaveHTML); }
        private void tbxAdminHTML_colonne4largeur_TextChanged(object sender, EventArgs e) { EnableBTN(btnSaveHTML); }
        private void tbxAdminHTML_colonne5nom_TextChanged(object sender, EventArgs e) { EnableBTN(btnSaveHTML); }
        private void tbxAdminHTML_colonne5largeur_TextChanged(object sender, EventArgs e) { EnableBTN(btnSaveHTML); }
        private void tbxAdminHTML_CSS_TextChanged(object sender, EventArgs e) { EnableBTN(btnSaveHTML); }
        private void tbxAdminMAIL_SMTP_TextChanged(object sender, EventArgs e) { EnableBTN(btnSaveMAIL); }
        private void tbxAdminMAIL_MailDenvoi_TextChanged(object sender, EventArgs e) { EnableBTN(btnSaveMAIL); }
        private void tbxAdminMAIL_Port_TextChanged(object sender, EventArgs e) { EnableBTN(btnSaveMAIL); }
        private void tbxAdminAPI_user_TextChanged(object sender, EventArgs e) { EnableBTN(btnSaveAPI); }
        private void tbxAdminAPI_password_TextChanged(object sender, EventArgs e) { EnableBTN(btnSaveAPI); }
        private void tbxAdminDIVERS_backupname_TextChanged(object sender, EventArgs e) { EnableBTN(btnSaveDIVERS); }

        private void button1_Click(object sender, EventArgs e)
        {
            MessageBox.Show((Convert.ToDouble(textBox1.Text) * 60 * 24 * 30).ToString("N"));
        }
    }
}
